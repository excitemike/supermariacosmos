# Setup (Windows)

1. Install VS Code
2. Install C/C++ extension <https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools>
3. Install mingw-64 from <https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download>
  **INSTALL TO A DIRECTORY WITHOUT SPACES IN THE PATH**
4. Add MinGW bin folder to path
