// implementations for SuperMariaCosmosGame class

#include "SuperMariaCosmosGame.h"
#include "SuperMariaCosmos/GameStates/GameStates.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "SDL/include/SDL.h"

#include <stdlib.h>
#include <time.h>

SuperMariaCosmosGame::SuperMariaCosmosGame() : Framework::BaseGame(), mStylusGraphic(NULL)
{
    // we have to seed this sometime
    srand( (unsigned)time(NULL) );

    // register subsystems with manager
    Framework::BaseGame::SetSubsystemManager(&mSubsystemManager);
    {
        using namespace SuperMariaCosmos;
        mSubsystemManager.RegisterSubsystem(&GameWindow);
        mSubsystemManager.RegisterSubsystem(&Renderer);
        mSubsystemManager.RegisterSubsystem(&TopScreenScene);
        mSubsystemManager.RegisterSubsystem(&BottomScreenScene);
        mSubsystemManager.RegisterSubsystem(&mStylusScene);
        mSubsystemManager.RegisterSubsystem(&InputManager);
        mSubsystemManager.RegisterSubsystem(&WidgetManager);
        mSubsystemManager.RegisterSubsystem(&Sound);
        mSubsystemManager.RegisterSubsystem(&LevelManager);
    }
}

void SuperMariaCosmosGame::Init()
{
    // pre-init configuration
    SuperMariaCosmos::GameWindow.SetWindowSize((unsigned)SuperMariaCosmos::WindowWidth, (unsigned)SuperMariaCosmos::WindowHeight);
    SuperMariaCosmos::GameWindow.SetCaption("Super Maria Cosmos DS");

    Framework::BaseGame::Init();

    // post-init configuration
    SuperMariaCosmos::Renderer.SetBackgroundColor(0.1f, 0.1f, 0.1f);
    SuperMariaCosmos::WidgetManager.SetSceneManager(&SuperMariaCosmos::BottomScreenScene);

    // init all the gamestates
    SuperMariaCosmos::GameStates::InitAllGameStates(this);

    // no mouse cursor for this game, we'll be drawing a stylus
    SDL_ShowCursor(SDL_DISABLE);

    ADD_OBJECT( mStylusGraphic, SuperMariaCosmos::SceneObjects::Stylus, (), SuperMariaCosmos::LAYER_PLAYER, mStylusScene);

    GoToPressStart();
}


void SuperMariaCosmosGame::Shutdown()
{
    Framework::BaseGame::Shutdown();

    // shutdown all the gamestates
    SuperMariaCosmos::GameStates::ShutdownAllGameStates();

    Utilities::TextureManager::FreeAllTextures();
}

void SuperMariaCosmosGame::Update()
{
    // standard updating
    Framework::BaseGame::Update();

    // HACK!
    mStylusGraphic->Update(0.f);

    // rendering
    {
        using namespace SuperMariaCosmos;

        Renderer.BeginScene();

        // top
        Renderer.UpdateViewport(0, (int)(WindowHeight-ScreenHeight), (int)ScreenWidth, (int)ScreenHeight );
        TopScreenScene.RenderScene(&Renderer);

        // bottom
        Renderer.UpdateViewport(0, 0,(int) ScreenWidth, (int)ScreenHeight );
        BottomScreenScene.RenderScene(&Renderer);

        // stylus scene
        Renderer.UpdateViewport(0 ,0, (int)WindowWidth, (int)WindowHeight);
        mStylusScene.RenderScene(&Renderer);

        Renderer.EndScene();
    }
}

void SuperMariaCosmosGame::GoToPressStart()
{
    // todo: change game state here
    ChangeGameState(&SuperMariaCosmos::GameStates::PressStartGameState);
}

void SuperMariaCosmosGame::GoToGame()
{
}


