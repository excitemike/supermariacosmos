/*
    Base subsystem manager
    create a game-specific subclass to customize this if necessary
*/

#include "BaseSubsystemManager.h"
#include <vector>
#include <algorithm>

namespace Framework
{
    BaseSubsystemManager::~BaseSubsystemManager()
    {
        mSubsystems.clear();
    }

    // register a subsystem with the manager, so that it gets included in the
    // startup-, update-, and shutdown-all functions
    void BaseSubsystemManager::RegisterSubsystem(BaseSubsystem* pSubsystem)
    {
        mSubsystems.push_back(pSubsystem);
    }

    // unregister a subsystem with the manager
    void BaseSubsystemManager::UnregisterSubsystem(BaseSubsystem* pSubsystem)
    {
        // first, see if we can find it in the vector
        std::vector<BaseSubsystem*>::iterator findResult;
        findResult = std::find(mSubsystems.begin(), mSubsystems.end(), pSubsystem);

        // then remove it if it was even in there
        if (mSubsystems.end() != findResult)
        {
            mSubsystems.erase(findResult);
        }
    }


    void BaseSubsystemManager::RemoveAllSubsystems()
    {
        mSubsystems.clear();
    }

    void BaseSubsystemManager::InitAllSubsystems()
    {
        // call Init on each subsystem registered
        std::vector<BaseSubsystem*>::iterator it;
        for (it=mSubsystems.begin(); it!=mSubsystems.end(); ++it)
        {
            (*it)->Init();
        }
    }

    void BaseSubsystemManager::ShutdownAllSubsystems()
    {
        // call Init on each subsystem registered
        std::vector<BaseSubsystem*>::iterator it;
        for (it=mSubsystems.begin(); it!=mSubsystems.end(); ++it)
        {
            (*it)->Shutdown();
        }
    }

    void BaseSubsystemManager::UpdateAllSubsystems(float secondsSinceLastFrame)
    {
        // call Init on each subsystem registered
        std::vector<BaseSubsystem*>::iterator it;
        for (it=mSubsystems.begin(); it!=mSubsystems.end(); ++it)
        {
            (*it)->Update(secondsSinceLastFrame);
        }
    }
}
