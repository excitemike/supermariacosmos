#include "Framework/math.h"

#include <stdlib.h>

// return how far counter-clockwise to rotate in order to get from 
// angle_start to angle_end, going around no more than once
// returns a value in the range [0, 2*PI]
float AngleDiff(float angle_start, float angle_end)
{
    float anglediff = (angle_end - angle_start);
    anglediff = fmodf(anglediff, 2.f*PI);
    if (anglediff < 0.f) { anglediff += (2.f*PI); }
    return anglediff;
}

// return how far rotate in order to get from 
// angle_start to angle_end, going around no more than once
// returns a value in the range [-PI, PI]
float GetShortestAngleTo(float angle_start, float angle_end)
{
    float anglediff = (angle_end - angle_start);
    anglediff = fmodf(anglediff, 2.f*PI);
    if (anglediff < -PI) { anglediff += (2.f*PI); }
    if (anglediff >  PI) { anglediff -= (2.f*PI); }
    return anglediff;
}

float Distance(float p1x, float p1y, float p2x, float p2y)
{
    float xdiff = p2x - p1x;
    float ydiff = p2y - p1y;
    float distance_squared = (xdiff*xdiff)+(ydiff*ydiff);
    return sqrtf(distance_squared);
}

float Lerp(float start, float end, float t)
{
    return start + (t * (end-start));
}

// atan(0,0) is undefined
float SafeArcTan(float y, float x)
{
    // look out... atan(0,0) is undefined
    if (   (x > -LENGTH_EPSILON)
        && (x <  LENGTH_EPSILON) 
        && (y > -LENGTH_EPSILON)
        && (y <  LENGTH_EPSILON) )
    {
        return 0.f;
    }
    else
    {
        return atan2f(y, x);
    }
}


// rotate a point based on you providing the sine and cosine of the angle
void RotatePoint(
    float sine_theta, float cosine_theta,
    float x, float y,
    float &out_x, float &out_y)
{
    out_x = (x * cosine_theta) + (y * -sine_theta);
    out_y = (x * sine_theta)   + (y * cosine_theta);
}

// return a random number in [0, 1].  assumes you already did srand() somewhere
float RandFloat()
{
    return (float)rand() / (float)RAND_MAX;
}


// move toward a point within acceptable_distance_from_target of 
// the given x, y, but with a limit on how far one call of this 
// function can take it
void MoveToward(
    float start_x, float start_y, 
    float target_x, float target_y, 
    float &out_x, float &out_y,
    float acceptable_distance_from_target, 
    float max_distance)
{
    float xdiff = target_x - start_x;
    float ydiff = target_y - start_y;
    float distance_to_target = Distance(0, 0, xdiff, ydiff);

    if (distance_to_target > acceptable_distance_from_target) 
    {
        float movement_length = distance_to_target - acceptable_distance_from_target;
        if (movement_length > max_distance) {movement_length = max_distance;}

        float dividable_distance = (distance_to_target < LENGTH_EPSILON) ? LENGTH_EPSILON : distance_to_target;
        float scale = movement_length / dividable_distance;
        out_x = start_x + (xdiff * scale );
        out_y = start_y + (ydiff * scale );
    }
}

float ReMap(float value, float source_range_start, float source_range_end, float destination_range_start, float destination_range_end)
{
    float t = value - source_range_start;
    t /= (source_range_end - source_range_start);
    return Lerp(destination_range_start, destination_range_end, t);
}

