/*
    Base GameState class
    - handles stuff common to most gamestates
*/

#ifndef BASEGAMESTATE_H
#define BASEGAMESTATE_H

#include "BaseGame.h"
#include "BaseSubsystemManager.h"
#include "GameStateInterface.h"

namespace Framework
{
    class BaseGameState : public GameStateInterface
    {
    public:
        BaseGameState() : mpGame(NULL) {}
        virtual ~BaseGameState(){}

        virtual void Init(Framework::BaseGame *pGame) {mpGame = pGame;}
        virtual void Shutdown() {}

        virtual void Enter() {}
        virtual void Update(float secondsSinceLastFrame) {}
        virtual void Exit() {}
    protected:
        Framework::BaseGame* GetGame(){return mpGame;}
    private:
        Framework::BaseGame *mpGame;
    };
}

#endif // BASEGAMESTATE_H
