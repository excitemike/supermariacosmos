/*
    base class for games -- make your own subclass to get started making your
    own game
*/
#ifndef BASEGAME_H
#define BASEGAME_H

#include "BaseSubsystemManager.h"
#include "GameStateInterface.h"

namespace Framework
{
    class BaseGame
    {
    public:
        BaseGame();
        virtual ~BaseGame();

        virtual void Init();
        virtual void Update();
        virtual void Shutdown();

        // tell the game to stop running
        virtual void QuitGame(){mQuitting=true;}
        virtual bool HasQuit(){return mQuitting;}

        virtual void RequestRestart() {mRestarting=true;mQuitting=true;}
        virtual bool RestartRequested() {return mRestarting;}

        // change to a new gamestate
        virtual void ChangeGameState(GameStateInterface* pNewGameState);
    protected:

        virtual void SetSubsystemManager(BaseSubsystemManager*);
        virtual BaseSubsystemManager* GetSubsystemManager(){return mpSubsystemManager;}
        GameStateInterface* GetGameState(){return mpGameState;}

    private:
        void LeaveGameState();

        BaseSubsystemManager* mpSubsystemManager;
        GameStateInterface *mpGameState;

        void BeginDelayedStateChangeSection();
        void EndDelayedStateChangeSection();
        bool mGameStateChangesNeedToBeDelayed;
        bool mDelayingGameStateChange;
        GameStateInterface *mDelayedGameState;

        unsigned mLastUpdateTimeInMilliseconds;
        bool mQuitting;
        bool mRestarting;
    };
}

#endif //BASEGAME_H
