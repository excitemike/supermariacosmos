#ifndef FRAMEWORK_MATH_H
#define FRAMEWORK_MATH_H

#include <math.h>

static const float PI = 3.1415926535897932384626433832795f;
static const float DEGREES_PER_RADIAN = 180.f / PI;

static const float EPSILON = 0.001f;
static const float LENGTH_EPSILON = EPSILON;
static const float ANGLE_EPSILON = EPSILON;

// return how far counter-clockwise to rotate in order to get from 
// angle_start to angle_end, going around no more than once
// returns a value in the range [0, 2*PI]
float AngleDiff(float angle_start, float angle_end);

// return how far rotate in order to get from 
// angle_start to angle_end, going around no more than once
// returns a value in the range [-PI, PI]
float GetShortestAngleTo(float angle_start, float angle_end);

float Distance(float p1x, float p1y, float p2x, float p2y);

float Lerp(float start, float end, float t);

float ReMap(float value, float source_range_start, float source_range_end, float destination_range_start, float destination_range_end);

// atan(0,0) is undefined
float SafeArcTan(float y, float x);

// rotate a point based on you providing the sine and cosine of the angle
void RotatePoint(
    float sine_theta, float cosine_theta,
    float x, float y, 
    float &out_x, float &out_y);

// return a random number in [0, 1].  assumes you already did srand() somewhere
float RandFloat();

// move toward a point within acceptable_distance_from_target of 
// the given x, y, but with a limit on how far one call of this 
// function can take it
void MoveToward(
    float start_x, float start_y, 
    float target_x, float target_y, 
    float &out_x, float &out_y,
    float acceptable_distance_from_target, 
    float max_distance);

// if the value is outside the range, force it into the range
template <typename T>
T Clamp(const T& x, const T& min, const T& max)
{
    if (x < min) return min;
    if (x > max) return max;
    return x;
}

#endif // FRAMEWORK_MATH_H


