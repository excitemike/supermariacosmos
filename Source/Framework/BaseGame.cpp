/*
    Implementation of BaseGame
    -- to make your own game, create your own child class of BaseGame
*/
#include "BaseGame.h"
#include "SDL/include/SDL.h"

namespace Framework
{
    BaseGame::BaseGame() :
        mpSubsystemManager(0),
        mpGameState(0),
        mGameStateChangesNeedToBeDelayed(false),
        mDelayingGameStateChange(false),
        mDelayedGameState(NULL),
        mQuitting(false),
        mRestarting(false)
    {
    }

    // last chance to clean up
    BaseGame::~BaseGame()
    {
    }

    // setup the game
    void BaseGame::Init()
    {
        mQuitting = false;
        mRestarting = false;

        mLastUpdateTimeInMilliseconds = (unsigned)SDL_GetTicks();

        if (0 != mpSubsystemManager)
        {
            mpSubsystemManager->InitAllSubsystems();
        }
    }

    // process the next frame of the game
    void BaseGame::Update()
    {
        // find out how much time passed and update mLastUpdateTimeInMilliseconds
        unsigned previousTime = mLastUpdateTimeInMilliseconds;
        mLastUpdateTimeInMilliseconds = (unsigned)SDL_GetTicks();
        float timeBetweenUpdatesInSeconds = (float)(mLastUpdateTimeInMilliseconds - previousTime) * 0.001f;

        // cap time to prevent weirdness when dragging the window or something
        if (timeBetweenUpdatesInSeconds > 0.05f) {timeBetweenUpdatesInSeconds = 0.05f;}

        // update all the subsystems with that time
        if (0 != mpSubsystemManager)
        {
            mpSubsystemManager->UpdateAllSubsystems(timeBetweenUpdatesInSeconds);
        }

        // update gamestate
        if (0 != mpGameState)
        {
            BeginDelayedStateChangeSection();
            mpGameState->Update(timeBetweenUpdatesInSeconds);
            EndDelayedStateChangeSection();
        }
    }

    // clean up and prepare to be deleted
    void BaseGame::Shutdown()
    {
        LeaveGameState();

        if (0 != mpSubsystemManager)
        {
            mpSubsystemManager->ShutdownAllSubsystems();
        }
    }

    // exit up the current gamestate. we're either going to a different one or
    // shutting down
    void BaseGame::LeaveGameState()
    {
        if (NULL != mpGameState)
        {
            mpGameState->Exit();
            mpGameState = NULL;
        }
    }

    // set the subsytem manager to use
    void BaseGame::SetSubsystemManager(BaseSubsystemManager* pSubsystemManager)
    {
        mpSubsystemManager = pSubsystemManager;
    }


    void BaseGame::BeginDelayedStateChangeSection()
    {
        mGameStateChangesNeedToBeDelayed = true;
        mDelayingGameStateChange = false;
    }

    void BaseGame::EndDelayedStateChangeSection()
    {
        mGameStateChangesNeedToBeDelayed = false;

        if (mDelayingGameStateChange)
        {
            ChangeGameState(mDelayedGameState);
            mDelayedGameState = NULL;
            mDelayingGameStateChange = false;
        }
    }

    // change to another gamestate
    void BaseGame::ChangeGameState(GameStateInterface* pNewGameState)
    {
        // might not be able to do it quite yet
        if (mGameStateChangesNeedToBeDelayed)
        {
            mDelayingGameStateChange = true;
            mDelayedGameState = pNewGameState;
        }
        else
        {
            // shutdown the old gamestate (if any)
            LeaveGameState();

            //  switch to the new one
            mpGameState = pNewGameState;

            // initialize the gamestate (if any)
            if (0 != mpGameState)
            {
                mpGameState->Enter();
            }
        }
    }
}
