/*
    Base subsystem manager
    create a game-specific subclass to customize this if necessary
*/
#ifndef BASESUBSYSTEMMANAGER_H
#define BASESUBSYSTEMMANAGER_H

#include "BaseSubsystem.h"
#include <vector>

namespace Framework
{
    class BaseSubsystemManager
    {
    public:
        virtual ~BaseSubsystemManager();

        virtual void RegisterSubsystem(BaseSubsystem*);
        virtual void UnregisterSubsystem(BaseSubsystem*);
        virtual void RemoveAllSubsystems();

        virtual void InitAllSubsystems();
        virtual void ShutdownAllSubsystems();
        virtual void UpdateAllSubsystems(float secondsSinceLastFrame);
    private:
        std::vector<BaseSubsystem*> mSubsystems;
    };
}

#endif // BASESUBSYSTEMMANAGER_H
