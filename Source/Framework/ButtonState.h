/*
    enumerations related to input
*/
#ifndef FRAMEWORK_ENUM_INPUT_H
#define FRAMEWORK_ENUM_INPUT_H

namespace Framework
{
    class ButtonState
    {
    public:
        // default constructor
        ButtonState() : mButtonState(BUTTONSTATE_UP) {}

        // assignment operators
        ButtonState& operator=(const ButtonState& rhs) {mButtonState=rhs.mButtonState; return *this;}

        // equality operator
        bool operator==(const ButtonState& rhs) const {return mButtonState==rhs.mButtonState;}

        // update state based on whether the button is up or down know and the
        // current value of this state
        void Update(bool buttonIsDown)
        {
            switch (mButtonState)
            {
            case BUTTONSTATE_JUSTRELEASED:
            case BUTTONSTATE_UP:
                mButtonState = buttonIsDown ? BUTTONSTATE_JUSTPRESSED : BUTTONSTATE_UP;
                break;
            case BUTTONSTATE_JUSTPRESSED:
            case BUTTONSTATE_HELD:
                mButtonState = buttonIsDown ? BUTTONSTATE_HELD : BUTTONSTATE_JUSTRELEASED;
                break;
            }
        }

        // allow checking of buttonstate
        bool IsUp() const { return ((BUTTONSTATE_JUSTRELEASED==mButtonState)||(BUTTONSTATE_UP==mButtonState));}
        bool IsDown() const { return !IsUp();}
        bool IsHeld() const { return (BUTTONSTATE_HELD==mButtonState);}
        bool WasJustReleased() const { return (BUTTONSTATE_JUSTRELEASED==mButtonState);}
        bool WasJustPressed() const { return (BUTTONSTATE_JUSTPRESSED==mButtonState);}

    private:
        enum eButtonState
        {
	        BUTTONSTATE_JUSTRELEASED,
	        BUTTONSTATE_UP,
	        BUTTONSTATE_JUSTPRESSED,
	        BUTTONSTATE_HELD
        };

        eButtonState mButtonState;
    };
}

#endif // FRAMEWORK_ENUM_INPUT_H
