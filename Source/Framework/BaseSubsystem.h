/*
    Base subsystem class
    not too useful in itself, exists jsut to be a place to put any stuff that
    is found to be common in subsystem code
*/
#ifndef BASESUBSYSTEM_H
#define BASESUBSYSTEM_H

namespace Framework
{
    class BaseSubsystem
    {
    public:
        virtual ~BaseSubsystem(){}

        virtual void Init(){}
        virtual void Shutdown(){}

        virtual void Update(float secondsSinceLastFrame){}
    private:
    };
}

#endif // BASESUBSYSTEM_H
