/*
    Base GameState interface class
    - interface used by BaseGame when dealing with GameStates
*/

#ifndef GAMESTATEINTERFACE_H
#define GAMESTATEINTERFACE_H

namespace Framework
{
    class GameStateInterface
    {
    public:
        virtual ~GameStateInterface(){}

        virtual void Enter() = 0;
        virtual void Update(float secondsSinceLastFrame) = 0;
        virtual void Exit()  = 0;
    };
}

#endif // GAMESTATEINTERFACE_H
