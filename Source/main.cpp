#include "SDL/include/SDL.h"
#include "SuperMariaCosmosGame.h"

// these are implemented below
void Shutdown(Framework::BaseGame*);
void SetupSdl();
void GameLoop(Framework::BaseGame*);
bool HandleKeyDown(SDLKey key);
bool ProcessEvents(Framework::BaseGame *pGame);
bool ProcessEvent(SDL_Event& event, Framework::BaseGame *pGame);


// since this is an SDL app, we use main instead of WinMain.  WinMain is
// actually in the SDL stuff somewhere.
int main ( int argc, char* argv [] )
{
    SuperMariaCosmosGame game;

    SetupSdl();

    do
    {
        game.Init();

        // now the game can just go
        GameLoop(&game);

        // game loop has ended, time to shutdown the game
        game.Shutdown();

    } while (game.RestartRequested());

    SDL_Quit();

    return 0;
}


// initial setup or Simple DirectMedia Layer
void SetupSdl()
{
    SDL_Init( SDL_INIT_AUDIO | SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_JOYSTICK );
}

// main loop of the game
void GameLoop(Framework::BaseGame *pGame)
{
    // message pump - just keep handling the events until we get the quit event
    while(ProcessEvents(pGame) && !pGame->HasQuit())
    {
        pGame->Update();
    }
}

// handle a message from the SDL message loop. return false when we get the
// QUIT event, so that we can then exit the program.
bool ProcessEvents(Framework::BaseGame *pGame)
{
    // default to continue the game
    bool continueProcessing = true;

    // get the event to handle
    SDL_Event event;
    bool eventFound = (1 == SDL_PollEvent(&event));

    // if there is one, handle it
    if (eventFound)
    {
        continueProcessing = ProcessEvent(event, pGame);
    }

    return continueProcessing;
}

// process an individual event.  Rturns whether the game should continue
// processing.
bool ProcessEvent(SDL_Event& event, Framework::BaseGame *pGame)
{
    // default to continue the game
    bool continueProcessing = true;

    switch (event.type)
    {
    case SDL_QUIT:
        // stop processing events -- we're shutting the app down
        continueProcessing = false;
        break;
    case SDL_KEYDOWN:
        continueProcessing = HandleKeyDown(event.key.keysym.sym);
        break;
    default:
        // ignoring other events
        break;
    }

    return continueProcessing;
}

// handle key presses -- returns whether the game should continue running.
bool HandleKeyDown(SDLKey key)
{
    // default to continue the game
    bool continueProcessing = true;

    switch (key)
    {
    case SDLK_ESCAPE:
        continueProcessing = false;
        break;
    default:
        // ignoring other keys
        break;
    }

    return continueProcessing;
}
