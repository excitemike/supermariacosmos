/*
    Custom game class for SuperMariaCosmos
*/
#ifndef SUPERMARIACOSMOSGAME_H
#define SUPERMARIACOSMOSGAME_H

#include "Framework/BaseGame.h"
#include "Framework/BaseSubsystemManager.h"
#include "Subsystems/GameWindow.h"
#include "Subsystems/RenderManager.h"
#include "Subsystems/SceneManager.h"
#include "Subsystems/InputManager.h"
#include "SuperMariaCosmos/SceneObjects/Stylus.h"

class SuperMariaCosmosGame : public Framework::BaseGame
{
public:
    SuperMariaCosmosGame();
    virtual void Init();
    virtual void Shutdown();
    virtual void Update();

    void GoToGame();
    void Quit();
private:

    void GoToPressStart();

    Framework::BaseSubsystemManager mSubsystemManager;
    Subsystems::SceneManager mStylusScene;
    SuperMariaCosmos::SceneObjects::Stylus *mStylusGraphic;
};


#endif // SUPERMARIACOSMOSGAME_H
