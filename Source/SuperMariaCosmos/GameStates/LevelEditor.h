#ifndef SUPERMARIACOSMOS_GAMESTATES_LEVELEDITOR_H
#define SUPERMARIACOSMOS_GAMESTATES_LEVELEDITOR_H

#include "Framework/BaseGameState.h"
#include "SuperMariaCosmos/SceneObjects/StarrySky.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "SuperMariaCosmos/SceneObjects/BaseButton.h"
#include "SceneObjects/BaseWidget.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObjectObserver.h"
#include "SuperMariaCosmos/LevelEditorMenu.h"
#include "SuperMariaCosmos/Level.h"
#include <vector>
#include <string>

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        class LevelEditor : public Framework::BaseGameState, public SceneObjects::LevelObjects::LevelObjectObserver
        {
            friend class ::SuperMariaCosmos::LevelEditorMenu;
        public:
            LevelEditor();

            virtual void Enter();
            virtual void Update(float secondsSinceLastFrame);
            virtual void Exit();

            virtual void DeleteButtonPressed(SuperMariaCosmos::SceneObjects::LevelObject* pObject);

            // tell the level editor what files to load/save.  either maybe
            // left NULL to it as it is
            virtual void Configure(const char* file_to_load=NULL, const char* file_to_save_as=NULL );
        protected:
            // add scroll buttons to the scene/widget manager
            virtual void AddScrollButtons();

            // remove scroll buttons from the scene/widget manager
            virtual void RemoveScrollButtons();

            // watch for scroll button presses
            virtual void HandleScrollButtons(float secondsSinceLastFrame);

            // take a look at input and see if it should do anything about it.
            virtual void HandleInput();

            virtual void ProcessUI(float secondsSinceLastFrame);

            virtual void SpawnNewObject(float mousex, float mousey);

            virtual void LoadLevel();
            virtual void SaveAs(const char* filename);
            virtual void SaveAndExit();
            virtual void ExitWithoutSave();
            virtual void PlayLevel();
            virtual void SetCurrentLevelObjectType(SuperMariaCosmos::SceneObjects::LevelObjectType type)
            {mCurrentLevelObjectType = type;}

            virtual void UpdateCamera();

        private:
            SceneObjects::StarrySky *mBottomScreenBackground;
            SceneObjects::StarrySky *mTopScreenBackground;

            SceneObjects::BaseButton *mScrollRightButton;
            SceneObjects::BaseButton *mScrollLeftButton;
            SceneObjects::BaseButton *mScrollUpButton;
            SceneObjects::BaseButton *mScrollDownButton;

            Level mLevel;

            LevelEditorMenu mMenu;

            std::string mFileToLoad;
            std::string mFileToSaveAs;

            float mCameraX;
            float mCameraY;

            SuperMariaCosmos::SceneObjects::LevelObjectType mCurrentLevelObjectType;
        };
    }
}

#endif // SUPERMARIACOSMOS_GAMESTATES_LEVELEDITOR_H
