// Game state for editing levels in
#include "LevelEditor.h"
#include "MainGame.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/GameStates/GameStates.h"
#include "SupermariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/Shared.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

static const char * TEMP_FILE_NAME = "temp.lvl";
static const float SCROLL_RATE = 500.f; // in pixels per second

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        LevelEditor::LevelEditor() :
            Framework::BaseGameState(),
            mBottomScreenBackground(NULL),
            mTopScreenBackground(NULL),
            mScrollRightButton(NULL),
            mScrollLeftButton(NULL),
            mScrollUpButton(NULL),
            mScrollDownButton(NULL),
            mLevel(),
            mMenu(),
            mFileToLoad(""),
            mFileToSaveAs(""),
            mCameraX(ScreenWidth * 0.5f),
            mCameraY(ScreenHeight * 0.5f)
        {
            mMenu.SetLevelEditor(this);
        }

        // beginning this game mode
        void LevelEditor::Enter()
        {
            Framework::BaseGameState::Enter();

            ADD_OBJECT_BOTTOM(mBottomScreenBackground, SceneObjects::StarrySky, (false), LAYER_BACKGROUND);
            ADD_OBJECT_TOP(mTopScreenBackground, SceneObjects::StarrySky, (true), LAYER_BACKGROUND);

            LoadLevel();

            AddScrollButtons();

            mMenu.Init();

            mCurrentLevelObjectType = SuperMariaCosmos::SceneObjects::LEVEL_OBJECT_CIRCLE;

            // hide tray for now
            mMenu.Hide();
        }

        void LevelEditor::Update(float secondsSinceLastFrame)
        {
            Framework::BaseGameState::Update(secondsSinceLastFrame);

            HandleInput();

            ProcessUI(secondsSinceLastFrame);
        }

        void LevelEditor::SpawnNewObject(float mousex, float mousey)
        {
            SuperMariaCosmos::SceneObjects::LevelObject *pLevelObject =
                mLevel.AddNewObject(mCurrentLevelObjectType);

            // map coordinates into the level's space
            mLevel.ApplyLevelTransformInverse(mousex, mousey, mousex, mousey);

            if (pLevelObject)
            {
                pLevelObject->SetPosition(mousex, mousey);
                pLevelObject->SetObserver(this);
                SuperMariaCosmos::WidgetManager.Select(pLevelObject);
            }
        }

        void LevelEditor::Exit()
        {
            // fix up the potentially dangling pointers before we free stuff
            WidgetManager.Deselect();

            REMOVE_OBJECT_BOTTOM(mBottomScreenBackground, LAYER_BACKGROUND);
            REMOVE_OBJECT_TOP(mTopScreenBackground, LAYER_BACKGROUND);

            mLevel.Unload();

            mMenu.Shutdown();

            RemoveScrollButtons();

            Framework::BaseGameState::Exit();
        }


        // take a look at input and see if it should do anything about it.
        void LevelEditor::HandleInput()
        {
            float mousex = SuperMariaCosmos::InputManager.GetMouseX();
            float mousey = SuperMariaCosmos::InputManager.GetMouseY();

            SuperMariaCosmos::WidgetManager.OnMouseMove(mousex, mousey);

            if (SuperMariaCosmos::InputManager.GetLeftMouseButton().WasJustPressed() )
            {
                if (!SuperMariaCosmos::WidgetManager.OnMouseDown(mousex, mousey))
                {
                    if (IsOnTouchScreen(mousex, mousey))
                    {
                        // deselect first, before we let a tap spawn anything
                        if (NULL != WidgetManager.GetSelected())
                        {
                            WidgetManager.Deselect();
                        }
                        else
                        // if nothing else handled the mouse down, let's make a new object
                        {
                            SpawnNewObject(mousex, mousey);
                        }
                    }
                }
            }
            else if (SuperMariaCosmos::InputManager.GetLeftMouseButton().WasJustReleased() )
            {
                SuperMariaCosmos::WidgetManager.OnMouseUp(mousex, mousey);
            }
        }

        void LevelEditor::ProcessUI(float secondsSinceLastFrame)
        {
            mMenu.ProcessInput();

            HandleScrollButtons(secondsSinceLastFrame);
        }

        void LevelEditor::ExitWithoutSave()
        {
            GetGame()->ChangeGameState(&SuperMariaCosmos::GameStates::PressStartGameState);
        }

        void LevelEditor::PlayLevel()
        {
            SaveAs(TEMP_FILE_NAME);
            MainGameGameState.Configure(MainGame::USE_SPECIFIED_FILE, TEMP_FILE_NAME);
            GetGame()->ChangeGameState(&SuperMariaCosmos::GameStates::MainGameGameState);
        }

        void LevelEditor::SaveAs(const char* filename)
        {
            mLevel.Save(filename);
        }

        void LevelEditor::SaveAndExit()
        {
            SaveAs(mFileToSaveAs.c_str());
            ExitWithoutSave();
        }


        void LevelEditor::LoadLevel()
        {
            mLevel.Load(mFileToLoad.c_str(), this);
            UpdateCamera();
        }

        void LevelEditor::DeleteButtonPressed(SuperMariaCosmos::SceneObjects::LevelObject* pObject)
        {
            mLevel.DeleteObject(pObject);
        }

        // tell the level editor what files to load/save.  either maybe
        // left NULL to it as it is
        void LevelEditor::Configure(const char* file_to_load, const char* file_to_save_as)
        {
            if (NULL != file_to_load)
            {
                mFileToLoad = file_to_load;
            }
            if (NULL != file_to_save_as)
            {
                mFileToSaveAs = file_to_save_as;
            }
        }


        // add scroll buttons to the scene/widget manager
        void LevelEditor::AddScrollButtons()
        {
            ADD_WIDGET(
                mScrollRightButton,
                SceneObjects::BaseButton,
                ("Content/Textures/scroll_right_up.png", "Content/Textures/scroll_right_down.png", 12.f, 192.f));
            ADD_WIDGET(
                mScrollLeftButton,
                SceneObjects::BaseButton,
                ("Content/Textures/scroll_left_up.png", "Content/Textures/scroll_left_down.png", 12.f, 192.f));
            ADD_WIDGET(
                mScrollUpButton,
                SceneObjects::BaseButton,
                ("Content/Textures/scroll_up_up.png", "Content/Textures/scroll_up_down.png", 256.f, 12.f));
            ADD_WIDGET(
                mScrollDownButton,
                SceneObjects::BaseButton,
                ("Content/Textures/scroll_down_up.png", "Content/Textures/scroll_down_down.png", 256.f, 12.f));

            mScrollRightButton->SetX( ScreenWidth - ( mScrollRightButton->GetWidth() * 0.5f));
            mScrollRightButton->SetY( ScreenHeight * 0.5f );

            mScrollLeftButton->SetX( mScrollLeftButton->GetWidth() * 0.5f );
            mScrollLeftButton->SetY( ScreenHeight * 0.5f );

            mScrollUpButton->SetX( ScreenWidth * 0.5f );
            mScrollUpButton->SetY( ScreenHeight - (mScrollUpButton->GetHeight() * 0.5f) );

            mScrollDownButton->SetX( ScreenWidth * 0.5f );
            mScrollDownButton->SetY( mScrollDownButton->GetHeight() * 0.5f );
        }

        // remove scroll buttons from the scene/widget manager
        void LevelEditor::RemoveScrollButtons()
        {
            REMOVE_WIDGET(mScrollRightButton);
            REMOVE_WIDGET(mScrollLeftButton);
            REMOVE_WIDGET(mScrollUpButton);
            REMOVE_WIDGET(mScrollDownButton);
        }

        // watch for scroll button presses
        void LevelEditor::HandleScrollButtons(float secondsSinceLastFrame)
        {
            // update the camera position if the hit one of those buttons on the edges
            if (mScrollRightButton->IsHeld())
            {
                mCameraX += SCROLL_RATE * secondsSinceLastFrame;
            }
            else if (mScrollLeftButton->IsHeld())
            {
                mCameraX -= SCROLL_RATE * secondsSinceLastFrame;
            }
            else if (mScrollUpButton->IsHeld())
            {
                mCameraY += SCROLL_RATE * secondsSinceLastFrame;
            }
            else if (mScrollDownButton->IsHeld())
            {
                mCameraY -= SCROLL_RATE * secondsSinceLastFrame;
            }

            // update regardless of whether we moved, it's not exactly
            // expensive
            UpdateCamera();
        }

        //
        void LevelEditor::UpdateCamera()
        {
            // clamp camera movement
            if (mCameraX < Level::CAMERA_MIN_X) {mCameraX = Level::CAMERA_MIN_X;}
            if (mCameraX > Level::CAMERA_MAX_X) {mCameraX = Level::CAMERA_MAX_X;}
            if (mCameraY < Level::CAMERA_MIN_Y) {mCameraY = Level::CAMERA_MIN_Y;}
            if (mCameraY > Level::CAMERA_MAX_Y) {mCameraY = Level::CAMERA_MAX_Y;}

            mLevel.SetCameraPosition(mCameraX, mCameraY);
        }
    }
}

