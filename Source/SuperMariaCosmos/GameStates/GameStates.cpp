#include "SuperMariaCosmos\GameStates\GameStates.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        PressStart PressStartGameState;
        MainGame MainGameGameState;
        LevelEditor LevelEditorGameState;

        void InitAllGameStates(Framework::BaseGame *pGame)
        {
            PressStartGameState.Init(pGame);
            MainGameGameState.Init(pGame);
            LevelEditorGameState.Init(pGame);
        }

        void ShutdownAllGameStates()
        {
            PressStartGameState.Shutdown();
            MainGameGameState.Shutdown();
            LevelEditorGameState.Shutdown();
        }
    }
}
