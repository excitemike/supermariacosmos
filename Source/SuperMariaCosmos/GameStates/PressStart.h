/*
    GameState for the press start screen
*/
#ifndef PRESSSTART_H
#define PRESSSTART_H

#include "Framework/BaseGameState.h"
#include "SceneObjects/Sprite.h"
#include "SuperMariaCosmos/SceneObjects/TitleScreenButton.h"
#include "SuperMariaCosmos/SceneObjects/TinyButton.h"
#include "SuperMariaCosmos/SceneObjects/StarrySky.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        class PressStart : public Framework::BaseGameState
        {
        public:
            PressStart();

            virtual void Enter();
            virtual void Update(float secondsSinceLastFrame);
            virtual void Exit();
        private:
            // take a look at input and see if it should do anything about it.
            virtual void HandleInput();

            void ProcessUI();

            ::SceneObjects::Sprite *mTitleImage;
            ::SceneObjects::Sprite *mScreenBorder;
            ::SceneObjects::Sprite *mBottomScreenImage;
            SceneObjects::StarrySky *mSky;

            SuperMariaCosmos::SceneObjects::TitleScreenButton *mPlayButton;
            SuperMariaCosmos::SceneObjects::TitleScreenButton *mLevelEditButton;

            static const float TITLE_OSCILLATE_SPEED; // radians / second.  (Frequency * 2pi)
            static const float TITLE_OSCILLATE_AMOUNT; // how far up AND down the oscillation goes (pixels)
            float mTitleOscillationTheta;
        };
    }
}

#endif // PRESSSTART_H
