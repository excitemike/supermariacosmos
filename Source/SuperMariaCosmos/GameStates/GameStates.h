// declarations for global gamestates
#ifndef GAMESTATES_H
#define GAMESTATES_H

#include "SuperMariaCosmos\GameStates\PressStart.h"
#include "SuperMariaCosmos\GameStates\MainGame.h"
#include "SuperMariaCosmos\GameStates\LevelEditor.h"
#include "Framework\BaseGame.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        extern PressStart PressStartGameState;
        extern MainGame MainGameGameState;
        extern LevelEditor LevelEditorGameState;

        void InitAllGameStates(Framework::BaseGame *pGame);
        void ShutdownAllGameStates();
    }
}

#endif // GAMESTATES_H
