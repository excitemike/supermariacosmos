#include "SuperMariaCosmos/GameStates/MainGameStates/LevelFailed.h"
#include "SuperMariaCosmos/GameStates/MainGameStates/States.h"
#include "SuperMariaCosmos/Shared.h"

static const float SHOWMESSAGE_TIME = 0.5f;
static const float EXIT_TIME = SHOWMESSAGE_TIME + 1.f;

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            LevelFailed::LevelFailed() :
                MainGameState(),
                mMessage(NULL),
                mTimeInState(0.f)
            {
            }

            void LevelFailed::Enter(MainGame* pMainGame)
            {
                MainGameState::Enter(pMainGame);

                ADD_OBJECT_BOTTOM(
                    mMessage,
                    ::SceneObjects::Sprite,
                    ("Content/Textures/failure.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight),
                    SuperMariaCosmos::LAYER_FOREGROUND);

                mMessage->Hide();

                mTimeInState = 0.f;
            }

            void LevelFailed::Exit()
            {
                REMOVE_OBJECT_BOTTOM(mMessage, SuperMariaCosmos::LAYER_FOREGROUND);
            }

            void LevelFailed::Update(float secondsSinceLastFrame)
            {
                mTimeInState += secondsSinceLastFrame;

                if (mTimeInState >= SHOWMESSAGE_TIME)
                {
                    mMessage->Show();
                }

                // restart
                if (mTimeInState >= EXIT_TIME)
                {
                    LoadLevel();
                    ChangeState(&PlayingState);
                }
            }
        }
    }
}
