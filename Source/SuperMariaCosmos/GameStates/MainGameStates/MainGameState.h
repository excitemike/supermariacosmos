// substates for the main game state
#ifndef SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_MAINGAMESTATE_H
#define SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_MAINGAMESTATE_H

#include "SuperMariaCosmos/GameStates/MainGame.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            class MainGameState
            {
            public:
                virtual void Enter(MainGame* pMainGame);
                virtual void Exit() = 0;
                virtual void Update(float secondsSinceLastFrame) = 0;
            protected:
                typedef Level::LevelObjectList LevelObjectList;

                virtual const LevelObjectList *GetLevelObjects();
                virtual void SetCameraPosition(float x, float y);

                // map coordinates from the level's space
                virtual void ApplyLevelTransform(float in_x, float in_y, float &out_x, float &out_y);
                // map coordinates into the level's space
                virtual void ApplyLevelTransformInverse(float in_x, float in_y, float &out_x, float &out_y);

                virtual void ChangeState(MainGameState *pNewState);

                virtual void OnLevelComplete();
                virtual void OnLevelFailed();

                virtual void LoadLevel();
                // go back to the main menu
                virtual void QuitGame();

            private:
                MainGame *mMainGameState;
            };
        }
    }
}

#endif // SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_MAINGAMESTATE_H
