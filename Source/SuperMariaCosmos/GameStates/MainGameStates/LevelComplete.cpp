#include "SuperMariaCosmos/GameStates/MainGameStates/LevelComplete.h"
#include "SuperMariaCosmos/GameStates/MainGameStates/States.h"
#include "SuperMariaCosmos/Shared.h"

static const float ANIMATION_DELAY = 0.75f;
static const float CONGLATURATION_DELAY = 1.f;
static const float GAME_COMPLETE_DELAY = 2.5f;

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            LevelComplete::LevelComplete() :
                MainGameState(),
                mConglaturation(NULL),
                mGameCompleteTop(NULL),
                mGameCompleteBottom(NULL),
                mRemainingDelay(0.f),
                mCurrentStep(NOTHING)
            {
            }

            void LevelComplete::Enter(MainGame* pMainGame)
            {
                MainGameState::Enter(pMainGame);

                ADD_OBJECT_BOTTOM(
                    mConglaturation,
                    ::SceneObjects::Sprite,
                    ("Content/Textures/conglaturation.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight),
                    SuperMariaCosmos::LAYER_FOREGROUND);

                ADD_OBJECT_TOP(
                    mGameCompleteTop,
                    ::SceneObjects::Sprite,
                    ("Content/Textures/gamecompletetop.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight),
                    SuperMariaCosmos::LAYER_FOREGROUND);

                ADD_OBJECT_BOTTOM(
                    mGameCompleteBottom,
                    ::SceneObjects::Sprite,
                    ("Content/Textures/gamecompletebottom.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight),
                    SuperMariaCosmos::LAYER_FOREGROUND);

                mGameCompleteTop->Hide();
                mGameCompleteBottom->Hide();
                mConglaturation->Hide();

                mCurrentStep = NOTHING;
                mRemainingDelay = ANIMATION_DELAY;
            }

            void LevelComplete::Exit()
            {
                REMOVE_OBJECT_TOP(mGameCompleteTop, SuperMariaCosmos::LAYER_FOREGROUND);
                REMOVE_OBJECT_BOTTOM(mGameCompleteBottom, SuperMariaCosmos::LAYER_FOREGROUND);
                REMOVE_OBJECT_BOTTOM(mConglaturation, SuperMariaCosmos::LAYER_FOREGROUND);
            }

            void LevelComplete::Update(float secondsSinceLastFrame)
            {
                mRemainingDelay -= secondsSinceLastFrame;

                if (mRemainingDelay <= 0.f)
                {
                    switch (mCurrentStep)
                    {
                    case NOTHING:
                        mConglaturation->Show();
                        mRemainingDelay = CONGLATURATION_DELAY;
                        mCurrentStep = CONGLATURATION;
                        break;
                    case CONGLATURATION:
                        // try to go to the next level now.
                        if (LevelManager.AdvanceToNextLevel())
                        {
                            LoadLevel();
                            ChangeState(&PlayingState);
                        }
                        else
                        {
                            // if there isn't a next level, this must be the
                            // end of the game
                            mConglaturation->Hide();
                            mGameCompleteTop->Show();
                            mGameCompleteBottom->Show();
                            mRemainingDelay = GAME_COMPLETE_DELAY;
                            mCurrentStep = GAMECOMPLETE;
                        }
                        break;
                    case GAMECOMPLETE:
                        // all out of level end screens
                        QuitGame();
                        break;
                    }
                }
            }
        }
    }
}
