#ifndef SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_PLAYING_H
#define SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_PLAYING_H

#include "SuperMariaCosmos/GameStates/MainGameStates/MainGameState.h"
#include "SuperMariaCosmos/CollisionSurface.h"

struct CollisionData;

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            class Playing : public MainGameState
            {
            public:
            	Playing();

                virtual void Enter(MainGame* pMainGame);
                virtual void Exit();
                virtual void Update(float secondsSinceLastFrame);
            protected:
                // switch current surfaces and see if we are touchign a death surface
                virtual void ChangeSurface(SuperMariaCosmos::CollisionSurface* pSurface);

                virtual void ProcessInput(float secondsSinceLastFrame);
                virtual void Physics(float secondsSinceLastFrame);

                // keep the camera on Maria
                virtual void UpdateCamera(float secondsSinceLastFrame);

                virtual void FreeFallPhysics(float secondsSinceLastFrame);
                virtual void Jump();

                virtual float GetDistanceBetweenObjects(::SceneObjects::BaseSceneItem *pObj1, ::SceneObjects::BaseSceneItem *pObj2 );

                // move Maria along whatever surfaces hold her
                virtual void WalkSurfaces( float mouseX, float mouseY, float secondsSinceLastFrame );

                // handles detecting collisions with other surfaces when attached
                // to a surface.  returns whether a collision means we'd better stop.
                // transformed_mouse(x/y) should already be in the level space
                virtual bool WalkCollisions(float future_x, float future_y, float transformed_mouse_x, float transformed_mouse_y);

                // look for what collisions would happen at the future
                // position and push the dat into the given vector
                virtual void FindCollisions(std::vector<CollisionData>& collisionDataCollection, float future_x, float future_y, float transformed_mouse_x, float transformed_mouse_y);


                // given a list of collisions, look through them to find out
                // whether we need to stop and/or switch to a different surface
                virtual bool ProcessCollisions(
                    const std::vector<CollisionData>& collisionDataCollection,
                    float future_x, float future_y,
                    float transformed_mouse_x, float transformed_mouse_y);

                virtual SuperMariaCosmos::CollisionSurface* FindNearestSurface(float x, float y, float *out_distance=NULL);

                // check for whether we finished the level and kick us out to
                // another gamestate if we are done
                virtual void CheckForLevelComplete();

                // called once we know the mouse is over the touchscreen
                virtual void OnTouch( float mouseX, float mouseY, float secondsSinceLastFrame );

                // called each frame in which Maria the player is not touching the touchscreen
                virtual void OnRelease();

                // watch for when the key gets picked up, then make it follow Maria
                virtual void HandleKey(float secondsSinceLastFrame);

                // see if they goofed up and we need to restart the level
                virtual void CheckForLevelFailure();
            private:
                float mCameraX;
                float mCameraY;

                SceneObjects::Key *mKey;

                SuperMariaCosmos::CollisionSurface *mCurrentSurface;
                float mMariaTargetRotation;

                // keep a reference to Maria
                SuperMariaCosmos::SceneObjects::SuperMaria *mMaria;
            };
        }
    }
}

#endif // SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_PLAYING_H
