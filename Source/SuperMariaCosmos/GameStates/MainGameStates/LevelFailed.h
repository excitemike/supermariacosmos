#ifndef SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_LEVELFAILED_H
#define SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_LEVELFAILED_H

#include "SuperMariaCosmos/GameStates/MainGameStates/MainGameState.h"
#include "SceneObjects/Sprite.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            class LevelFailed : public MainGameState
            {
            public:
                LevelFailed();

                virtual void Enter(MainGame* pMainGame);
                virtual void Exit();
                virtual void Update(float secondsSinceLastFrame);
            private:
                ::SceneObjects::Sprite * mMessage;
                float mTimeInState;
            };
        }
    }
}

#endif // SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_LEVELFAILED_H
