// static maingame substate instances

#include "SuperMariaCosmos/GameStates/MainGameStates/States.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            Playing PlayingState;
            LevelComplete LevelCompleteState;
            LevelFailed LevelFailedState;
        }
    }
}
