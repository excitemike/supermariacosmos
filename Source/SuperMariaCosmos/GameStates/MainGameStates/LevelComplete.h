#ifndef SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_LEVELCOMPLETE_H
#define SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_LEVELCOMPLETE_H

#include "SuperMariaCosmos/GameStates/MainGameStates/MainGameState.h"
#include "SceneObjects/Sprite.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            class LevelComplete : public MainGameState
            {
            public:
                LevelComplete();

                virtual void Update(float secondsSinceLastFrame);
                virtual void Enter(MainGame* pMainGame);
                virtual void Exit();
            private:
                ::SceneObjects::Sprite * mConglaturation;
                ::SceneObjects::Sprite * mGameCompleteTop;
                ::SceneObjects::Sprite * mGameCompleteBottom;
                float mRemainingDelay;
                enum LevelCompleteStep
                {
                    NOTHING,
                    CONGLATURATION,
                    GAMECOMPLETE
                } mCurrentStep;
            };
        }
    }
}

#endif // SUPERMARIACOSMOS_GAMESTATES_MAINGAMESTATES_LEVELCOMPLETE_H
