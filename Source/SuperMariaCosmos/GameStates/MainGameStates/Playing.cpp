#include "SuperMariaCosmos/GameStates/MainGameStates/Playing.h"
#include "SuperMariaCosmos/GameStates/MainGameStates/States.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/Goal.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/Key.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"
#include <stdio.h>
#include <float.h> // for FLT_MAX
#include <assert.h>


struct CollisionData
{
    SuperMariaCosmos::CollisionSurface *pSurface;
    float normal_x;
    float normal_y;
    float movement_x;
    float movement_y;
};

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            const float GRAVITY_ACCEL = 800.f; // in pixels per second per second
            const float WALK_SPEED = 125.f; // in pixels per second
            const float KEY_SPEED = 500.f; // in pixels per second
            const float CAMERA_FOLLOW_DISTANCE = 32.f; // in pixels
            const float CAMERA_SPEED = 300.f; // in pixels per second
            const float JUMP_VELOCITY = 200.f; // in pixels per second
            const float ROTATION_RATE = 16.f; // in radians per second

            Playing::Playing() :
                MainGameState(),
                mCameraX(0.f),
                mCameraY(0.f),
                mKey(NULL),
                mCurrentSurface(NULL),
                mMariaTargetRotation(0.f),
                mMaria(NULL)
            {
            }

            void Playing::Enter(MainGame* pMainGame)
            {
                MainGameState::Enter(pMainGame);

                bool foundKey = false;
                bool foundMaria = false;
                bool foundDoor = false;

                SceneObjects::Goal *pDoor = NULL;

                // get a reference to the key and save it off
                LevelObjectList::const_iterator it;
                const LevelObjectList *pLevelObjects = GetLevelObjects();
                for (it = pLevelObjects->begin(); it != pLevelObjects->end(); ++it)
                {
                    if ( !foundKey )
                    {
                        foundKey = (*it)->CastMe(&mKey);
                    }

                    if ( !foundMaria )
                    {
                        foundMaria = (*it)->CastMe(&mMaria);
                    }

                    if ( !foundDoor )
                    {
                        foundDoor = (*it)->CastMe(&pDoor);
                    }
                }

                // start the camera at the door!
                if (foundDoor)
                {
                    mCameraX = pDoor->GetX();
                    mCameraY = pDoor->GetY();
                }
            }

            void Playing::Exit()
            {
                mKey = NULL;
                mMaria = NULL;
                mCurrentSurface = NULL;
            }

            void Playing::Update(float secondsSinceLastFrame)
            {
                if (NULL != mMaria)
                {
                    Physics(secondsSinceLastFrame);

                    // after physics so that jumping won't result in a collision right away with what you jumped off of
                    ProcessInput(secondsSinceLastFrame);

                    HandleKey(secondsSinceLastFrame);

                    CheckForLevelComplete();
                    CheckForLevelFailure();

                    UpdateCamera(secondsSinceLastFrame);
                }
            }


            void Playing::ProcessInput(float secondsSinceLastFrame)
            {
                if ( SuperMariaCosmos::InputManager.GetLeftMouseButton().IsDown() )
                {
                    float mouseX = SuperMariaCosmos::InputManager.GetMouseX();
                    float mouseY = SuperMariaCosmos::InputManager.GetMouseY();

                    if (IsOnTouchScreen(mouseX, mouseY))
                    {
                        OnTouch(mouseX, mouseY, secondsSinceLastFrame);
                    }
                    else
                    {
                        OnRelease();
                    }
                }
                else
                {
                    OnRelease();
                }

                // jump when they press the UP key
                if ( SuperMariaCosmos::InputManager.GetKeyState(SDLK_SPACE).WasJustPressed() )
                {
                    Jump();
                }

            }

            void Playing::Physics(float secondsSinceLastFrame)
            {
                switch (mMaria->GetState())
                {
                case SuperMariaCosmos::SceneObjects::SuperMaria::FREEFALL:
                    FreeFallPhysics(secondsSinceLastFrame);
                    break;
                case SuperMariaCosmos::SceneObjects::SuperMaria::STAND:
                case SuperMariaCosmos::SceneObjects::SuperMaria::WALK:
                    // we don't really do anything I would call physics while walking
                    break;
                default:
                    // unhandled case
                    assert(false);
                    break;
                }

                mMaria->RotateToward(mMariaTargetRotation, ROTATION_RATE * secondsSinceLastFrame);
            }

            // handles gravitational attraction and detecting collisions when
            // Maria is in freefall (not attached to a surface)
            void Playing::FreeFallPhysics(float secondsSinceLastFrame)
            {
                float x = mMaria->GetX();
                float y = mMaria->GetY();

                // find the nearest piece of the nearest planet
                float distance = FLT_MAX;
                SuperMariaCosmos::CollisionSurface* pSurface = FindNearestSurface(x, y, &distance);

                if (pSurface)
                {
                    // if we ran into it, stop falling...
                    float collision_radius = mMaria->GetCollisionRadius();
                    if ( distance < collision_radius)
                    {
                        mMaria->SetVelocity(0.f,0.f);

                        ChangeSurface( pSurface );

                        // get her to the right place and angle
                        WalkSurfaces(mMaria->GetX(), mMaria->GetY(), 0.f);
                    }
                    else
                    {
                        float surface_x;
                        float surface_y;
                        pSurface->GetNearestPointOnSurface(x, y, &surface_x, &surface_y);

                        // ...otherwise, fall toward it
                        mMaria->AccelToward(surface_x, surface_y, GRAVITY_ACCEL * secondsSinceLastFrame);

                        mMariaTargetRotation = SafeArcTan(y-surface_y ,x-surface_x) - (0.5f*PI);
                    }
                }
            }

            void Playing::Jump()
            {
                // can only jump when in contact with a surface
                switch(mMaria->GetState())
                {
                case SuperMariaCosmos::SceneObjects::SuperMaria::WALK:
                case SuperMariaCosmos::SceneObjects::SuperMaria::STAND:
                    float surface_x;
                    float surface_y;
                    mCurrentSurface->GetNearestPointOnSurface(mMaria->GetX(), mMaria->GetY(), &surface_x, &surface_y);

                    mMaria->AccelToward(surface_x, surface_y, JUMP_VELOCITY * -1.f);

                    mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::FREEFALL);

                    Sound.PlaySound(Subsystems::Sound::SOUND_A);
                    break;
                default:
                    break;
                }
            }

            // find the surface closest to the given point. optionally putting
            // the actual distance to it in out_distance.  If no surface is
            // found, it returns null and puts FLT_MAX in out_distance
            SuperMariaCosmos::CollisionSurface* Playing::FindNearestSurface(float x, float y, float *out_distance)
            {
                float bestdistance = FLT_MAX;
                SuperMariaCosmos::CollisionSurface* pBestSurface = NULL;

                // look at each surface of each level object
                LevelObjectList::const_iterator it;
                const LevelObjectList *pLevelObjects = GetLevelObjects();
                for (it = pLevelObjects->begin(); it != pLevelObjects->end(); ++it)
                {
                    SuperMariaCosmos::SceneObjects::LevelObject* pLevelObject = *it;
                    for (unsigned i=0; i < pLevelObject->GetNumSurfaces(); ++i)
                    {
                        SuperMariaCosmos::CollisionSurface* pSurface = pLevelObject->GetSurface(i);
                        float distance = pSurface->GetDistance(x, y);

                        // if it is nearer, update our idea of the current best
                        if ( (distance < bestdistance) || (NULL == pBestSurface) )
                        {
                            bestdistance = distance;
                            pBestSurface = pSurface;
                        }
                    }
                }

                if (NULL!=out_distance) {*out_distance = bestdistance;}

                return pBestSurface;
            }

            // called each frame in which Maria the player is not touching the touchscreen
            void Playing::OnRelease()
            {
                if (SuperMariaCosmos::SceneObjects::SuperMaria::WALK == mMaria->GetState())
                {
                    mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::STAND);
                }
            }

            // called once we know the click was on the touchscreen
            void Playing::OnTouch( float mouseX, float mouseY, float secondsSinceLastFrame )
            {
                switch (mMaria->GetState())
                {
                case SuperMariaCosmos::SceneObjects::SuperMaria::STAND:
                case SuperMariaCosmos::SceneObjects::SuperMaria::WALK:
                    WalkSurfaces(mouseX, mouseY, secondsSinceLastFrame);
                    break;
                case SuperMariaCosmos::SceneObjects::SuperMaria::FREEFALL:
                    // touching doesn't do anything in freefall
                    break;
                default:
                    // unhandled case
                    assert(false);
                    break;
                }
            }

            // move Maria along whatever surfaces hold her
            void Playing::WalkSurfaces( float mouseX, float mouseY, float secondsSinceLastFrame )
            {
                // surfaces expect that x and y in the same coordinate space they are in
                ApplyLevelTransformInverse(mouseX, mouseY, mouseX, mouseY);

                float updated_x;
                float updated_y;
                float normal_angle;
                float distance_travelled;
                CollisionSurface::Direction direction;

                CollisionSurface *pHandOffSurface =
                    mCurrentSurface->MoveAlongSurface(
                        mMaria->GetX(),
                        mMaria->GetY(),
                        mouseX, mouseY,
                        WALK_SPEED * secondsSinceLastFrame,
                        mMaria->GetCollisionRadius(),
                        &updated_x,
                        &updated_y,
                        &distance_travelled,
                        &normal_angle,
                        &direction);

                bool stopForCollision = WalkCollisions(updated_x, updated_y, mouseX, mouseY);

                if (stopForCollision)
                {
                    // bumped into something.  probably shouldn't be rubbing
                    // ourselves all over it
                    mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::STAND);

                    // and in case processing collisions gave us a different surface, put her on it
                    pHandOffSurface =
                        mCurrentSurface->MoveAlongSurface(
                            mMaria->GetX(),
                            mMaria->GetY(),
                            mouseX, mouseY,
                            0.f,
                            mMaria->GetCollisionRadius(),
                            &updated_x,
                            &updated_y,
                            NULL,
                            &normal_angle,
                            &direction);
                }
                else
                {
                    // if we're actually moving somewhere, let's play the walk
                    // animation
                    if (distance_travelled > EPSILON)
                    {
                        mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::WALK);
                    }
                    else
                    {
                        mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::STAND);
                    }
                }

                mMaria->SetPosition(updated_x, updated_y);
                mMariaTargetRotation = normal_angle - (0.5f*PI);

                if (CollisionSurface::COUNTERCLOCKWISE == direction)
                {
                    mMaria->SetHFlip(true);
                }
                else if (CollisionSurface::CLOCKWISE == direction)
                {
                    mMaria->SetHFlip(false);
                }

                // if we go off the end of a surface, keep going on the next one
                if (NULL != pHandOffSurface)
                {
                    // but before we actually do the handoff, let's first check
                    // that it won't just hand her right back immediately
                    CollisionSurface * pHandOff2 =
                        pHandOffSurface->MoveAlongSurface(
                            mMaria->GetX(),
                            mMaria->GetY(),
                            mouseX, mouseY,
                            WALK_SPEED * secondsSinceLastFrame,
                            mMaria->GetCollisionRadius());
                    if (pHandOffSurface != pHandOff2)
                    {
                        ChangeSurface( pHandOffSurface );
                    }
                }
            }

            // handles detecting collisions with other surfaces when attached
            // to a surface.  returns whether a collision means we'd better stop.
            // transformed_mouse(x/y) should already be in the level space
            bool Playing::WalkCollisions(float future_x, float future_y, float transformed_mouse_x, float transformed_mouse_y)
            {
                static std::vector<CollisionData> collisions(8);
                collisions.clear();

                FindCollisions(collisions, future_x, future_y, transformed_mouse_x, transformed_mouse_y);

                return ProcessCollisions(collisions, future_x, future_y, transformed_mouse_x, transformed_mouse_y);
            }

            // given a list of collisions, look through them to find out
            // whether we need to stop and/or switch to a different surface
            bool Playing::ProcessCollisions(
                const std::vector<CollisionData>& collisionDataCollection,
                float future_x, float future_y,
                float transformed_mouse_x, float transformed_mouse_y)
            {
                // whether we'll need to stop the movement because of a collision
                bool stopMovement = false;

                if (collisionDataCollection.size() == 0)
                {
                    // Nothing to walk on, I guess.  Quite possibly because
                    // this is a handoff situation
                    stopMovement = false;
                }
                else if (collisionDataCollection.size() == 1)
                {
                    // easy case!  there's only one we're hitting, so switch to it
                    if (mCurrentSurface == collisionDataCollection[0].pSurface)
                    {
                        // we don't even have to stop for a switch or anything
                        stopMovement = false;
                    }
                    else
                    {
                        ChangeSurface( collisionDataCollection[0].pSurface );
                        stopMovement = true;
                    }
                }
                else
                {
                    // >1 collision. let's see if it left us any valid paths
                    CollisionSurface * pBestSurface = NULL;
                    unsigned usablePaths = 0;
                    float best_distance = FLT_MAX;
                    std::vector<CollisionData>::const_iterator path_iter;
                    std::vector<CollisionData>::const_iterator normal_iter;
                    float distance_to_feet = mMaria->GetCollisionRadius() - 1.f;

                    // only consider paths that don't have a wall stopping us
                    for (path_iter = collisionDataCollection.begin(); path_iter != collisionDataCollection.end(); ++path_iter)
                    {
                        bool pathOk = true;

                        for (normal_iter = collisionDataCollection.begin(); normal_iter != collisionDataCollection.end(); ++normal_iter)
                        {
                            // don't compare against yourself
                            if (path_iter != normal_iter)
                            {
                                float dotprod =
                                    (path_iter->movement_x * normal_iter->normal_x)
                                    +(path_iter->movement_y * normal_iter->normal_y);
                                if (dotprod < 0.f)
                                {
                                    // well, we can't use this one
                                    pathOk = false;
                                    break;
                                }
                            }
                        }

                        if (pathOk)
                        {
                            // this one must be okay!
                            // see if it is the best one so far
                            float offset_to_feet_x = -path_iter->normal_x * distance_to_feet;
                            float offset_to_feet_y = -path_iter->normal_y * distance_to_feet;
                            float present_x = mMaria->GetX();
                            float present_y = mMaria->GetY();
                            float updated_x = present_x + path_iter->movement_x;
                            float updated_y = present_y + path_iter->movement_y;
                            float feet_x = updated_x + offset_to_feet_x;
                            float feet_y = updated_y + offset_to_feet_y;
                            float distance_to_target = Distance(feet_x, feet_y, transformed_mouse_x, transformed_mouse_y);

                            if ( (distance_to_target < best_distance) || (NULL == pBestSurface) )
                            {
                                best_distance = distance_to_target;
                                pBestSurface = path_iter->pSurface;
                            }

                            ++usablePaths;
                        }
                    }

                    if (0 == usablePaths)
                    {
                        // guess we aren't going anywhere
                        stopMovement = true;
                    }
                    else if (1 == usablePaths)
                    {
                        // easy case!  there's only one we're hitting, so switch to it
                        if (mCurrentSurface == pBestSurface)
                        {
                            // we don't even have to stop for a switch or anything
                            stopMovement = false;
                        }
                        else
                        {
                            // left our surface.  Make the switch and stop
                            ChangeSurface( pBestSurface );
                            stopMovement = true;
                        }
                    }
                    else
                    {
                        ChangeSurface(pBestSurface);
                        stopMovement = ( (collisionDataCollection.size() != usablePaths)||(mCurrentSurface != pBestSurface) );
                    }
                }

                return stopMovement;
            }

            // look for what collisions would happen at the future
            // position and push the dat into the given vector
            void Playing::FindCollisions(std::vector<CollisionData>& collisionDataCollection, float future_x, float future_y, float transformed_mouse_x, float transformed_mouse_y)
            {
                // look at each surface of each level object
                LevelObjectList::const_reverse_iterator it;
                const LevelObjectList *pLevelObjects = GetLevelObjects();
                for (it = pLevelObjects->rbegin(); it != pLevelObjects->rend(); ++it)
                {
                    SuperMariaCosmos::SceneObjects::LevelObject* pLevelObject = *it;
                    for (unsigned i=0; i < pLevelObject->GetNumSurfaces(); ++i)
                    {
                        SuperMariaCosmos::CollisionSurface* pSurface = pLevelObject->GetSurface(i);

                        float distance_to_surface = pSurface->GetDistance(future_x, future_y);

                        // if we're in contact with it
                        if (distance_to_surface <= mMaria->GetCollisionRadius() )
                        {
                            // see where that surface would put us
                            float updated_x;
                            float updated_y;
                            float surface_normal_angle;
                            CollisionSurface *pHandOffSurface =
                                pSurface->MoveAlongSurface(
                                    future_x,
                                    future_y,
                                    transformed_mouse_x, transformed_mouse_y,
                                    LENGTH_EPSILON,
                                    mMaria->GetCollisionRadius(),
                                    &updated_x,
                                    &updated_y,
                                    NULL,
                                    &surface_normal_angle,
                                    NULL);

                            // hmm, if there's a handoff for such a small movement,
                            // we can probably ignore that surface
                            if (NULL == pHandOffSurface)
                            {

                                float present_x = mMaria->GetX();
                                float present_y = mMaria->GetY();
                                float delta_x = updated_x - present_x;
                                float delta_y = updated_y - present_y;

                                // push it into the collision data
                                CollisionData cd;
                                cd.pSurface = pSurface;
                                cd.normal_x = cosf(surface_normal_angle);
                                cd.normal_y = sinf(surface_normal_angle);
                                cd.movement_x = delta_x;
                                cd.movement_y = delta_y;
                                collisionDataCollection.push_back(cd);
                            }
                        }
                    }
                }
            }

            // check for whether we finished the level and kick us out to
            // another gamestate if we are done
            void Playing::CheckForLevelComplete()
            {
                // can't open the door anyway if you don't have the key
                if ( (mKey==NULL) || (mKey->IsFound()) )
                {
                    // look for goals in the level
                    LevelObjectList::const_iterator it;
                    const LevelObjectList *pLevelObjects = GetLevelObjects();
                    for (it = pLevelObjects->begin(); it != pLevelObjects->end(); ++it)
                    {
                        SuperMariaCosmos::SceneObjects::LevelObject* pLevelObject = *it;

                        SceneObjects::Goal *pGoal;
                        if (pLevelObject->CastMe(&pGoal))
                        {
                            float r1 = pGoal->GetRadius();
                            float r2 = mMaria->GetCollisionRadius();
                            float distance = GetDistanceBetweenObjects(pGoal, mMaria);
                            if (distance <= (r1+r2))
                            {
                                OnLevelComplete();
                                Sound.PlaySound(Subsystems::Sound::SOUND_C);
                                mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::VICTORY);
                                mMaria->SetVelocity(0.f,0.f);
                            }
                        }
                    }
                }
            }

            // watch for when the key gets picked up, then make it follow Maria
            void Playing::HandleKey(float secondsSinceLastFrame)
            {
                if (NULL != mKey)
                {
                    float r1 = mKey->GetRadius();
                    float r2 = mMaria->GetCollisionRadius();

                    // key just sits there until you touch it
                    if (!mKey->IsFound())
                    {
                        float distance = GetDistanceBetweenObjects(mKey, mMaria);
                        if (distance <= (r1+r2))
                        {
                            mKey->SetFound(true);
                            Sound.PlaySound(Subsystems::Sound::SOUND_B);
                        }
                    }
                    else // then it starts following you around
                    {
                        float x1 = mKey->GetX();
                        float y1 = mKey->GetY();
                        float x2 = mMaria->GetX();
                        float y2 = mMaria->GetY();

                        // since both sprites lose a lot of space to alpha, we
                        // probably want a smaller follow radius on that key
                        // (I used to use the collision radius)
                        float min_distance = 8.f;

                        if (Distance(x1, y1, x2, y2) > min_distance)
                        {
                            mKey->MoveToward(x2, y2, min_distance, KEY_SPEED * secondsSinceLastFrame );
                        }
                    }
                }
            }

            float Playing::GetDistanceBetweenObjects(
                ::SceneObjects::BaseSceneItem *pObj1,
                ::SceneObjects::BaseSceneItem *pObj2 )
            {
                float x1 = pObj1->GetX();
                float y1 = pObj1->GetY();
                float x2 = pObj2->GetX();
                float y2 = pObj2->GetY();
                return Distance(x1, y1, x2, y2);
            }

            // keep the camera on Maria
            void Playing::UpdateCamera(float secondsSinceLastFrame)
            {
                MoveToward(
                    mCameraX, mCameraY,
                    mMaria->GetX(), mMaria->GetY(),
                    mCameraX, mCameraY,
                    CAMERA_FOLLOW_DISTANCE,
                    CAMERA_SPEED * secondsSinceLastFrame);

                // clamp camera movement
                if (mCameraX < Level::CAMERA_MIN_X) {mCameraX = Level::CAMERA_MIN_X;}
                if (mCameraX > Level::CAMERA_MAX_X) {mCameraX = Level::CAMERA_MAX_X;}
                if (mCameraY < Level::CAMERA_MIN_Y) {mCameraY = Level::CAMERA_MIN_Y;}
                if (mCameraY > Level::CAMERA_MAX_Y) {mCameraY = Level::CAMERA_MAX_Y;}

                SetCameraPosition(mCameraX, mCameraY);
            }

            // switch current surfaces and see if we are touchign a death surface
            void Playing::ChangeSurface(SuperMariaCosmos::CollisionSurface* pSurface)
            {
                mCurrentSurface = pSurface;
            }

            // see if they goofed up and we need to restart the level
            void Playing::CheckForLevelFailure()
            {
                if (NULL != mCurrentSurface)
                {
                    if (CollisionSurface::DEATHZONE == mCurrentSurface->GetSurfaceType())
                    {
                        OnLevelFailed();
                        Sound.PlaySound(Subsystems::Sound::SOUND_D);
                        mMaria->SetState(SuperMariaCosmos::SceneObjects::SuperMaria::BURN);
                        mMaria->SetVelocity(0.f,0.f);
                    }
                }
            }
        }
    }
}

