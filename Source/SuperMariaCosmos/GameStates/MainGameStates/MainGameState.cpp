#include "SuperMariaCosmos/GameStates/MainGameStates/MainGameState.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
			void MainGameState::Enter(MainGame* pMainGame)
			{
				mMainGameState=pMainGame;
			}

			const Level::LevelObjectList * MainGameState::GetLevelObjects()
			{
				return mMainGameState->mLevel.GetLevelObjectList();
			}

			void MainGameState::SetCameraPosition(float x, float y)
			{
				mMainGameState->mLevel.SetCameraPosition(x, y);
			}

			// map coordinates from the level's space
			void MainGameState::ApplyLevelTransform(float in_x, float in_y, float &out_x, float &out_y)
			{
				mMainGameState->mLevel.ApplyLevelTransform(in_x, in_y, out_x, out_y);
			}

			// map coordinates into the level's space
			void MainGameState::ApplyLevelTransformInverse(float in_x, float in_y, float &out_x, float &out_y)
			{
				mMainGameState->mLevel.ApplyLevelTransformInverse(in_x, in_y, out_x, out_y);
			}

			void MainGameState::ChangeState(MainGameState *pNewState)
			{
				mMainGameState->ChangeState(pNewState);
			}

			void MainGameState::OnLevelComplete()
			{
				mMainGameState->OnLevelComplete();
			}

			void MainGameState::OnLevelFailed()
			{
				mMainGameState->OnLevelFailed();
			}

			void MainGameState::LoadLevel()
			{
				mMainGameState->LoadLevel();
			}

			// go back to the main menu
			void MainGameState::QuitGame()
			{
				mMainGameState->QuitGame();
			}
        }
    }
}
