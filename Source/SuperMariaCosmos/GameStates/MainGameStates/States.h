// static maingame substate instances

#include "SuperMariaCosmos/GameStates/MainGameStates/Playing.h"
#include "SuperMariaCosmos/GameStates/MainGameStates/LevelComplete.h"
#include "SuperMariaCosmos/GameStates/MainGameStates/LevelFailed.h"

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            extern Playing PlayingState;
            extern LevelComplete LevelCompleteState;
            extern LevelFailed LevelFailedState;
        }
    }
}
