// GameState for the main game

#include "MainGame.h"

#include "SupermariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/Shared.h"
#include "SuperMariaCosmos/GameStates/MainGameStates/States.h"
#include "SuperMariaCosmos/GameStates/GameStates.h"


namespace SuperMariaCosmos
{
    namespace GameStates
    {
        MainGame::MainGame() :
            Framework::BaseGameState(),
            mLevelFile(""),
            mLevel(),
            mLevelProgression(USE_MAIN_GAME_PROGRESSION),
            mBottomScreenBackground(NULL),
            mTopScreenBackground(NULL),
            mDelayedStateChange(NULL),
            mSubState(NULL),
            mIsInStateChangeDelaySection(false),
            mDelayedStateChangeRequested(false)
        {
        }


        void MainGame::Init(Framework::BaseGame *pGame)
        {
            Framework::BaseGameState::Init(pGame);
        }


        void MainGame::Enter()
        {
            Framework::BaseGameState::Enter();

            LoadSounds();

            ADD_OBJECT_BOTTOM(mBottomScreenBackground, SceneObjects::StarrySky, (false), LAYER_BACKGROUND);
            ADD_OBJECT_TOP(mTopScreenBackground, SceneObjects::StarrySky, (true), LAYER_BACKGROUND);

            LoadLevel();

            ChangeState(&MainGameStates::PlayingState);
        }


        void MainGame::Update(float secondsSinceLastFrame)
        {
            Framework::BaseGameState::Update(secondsSinceLastFrame);

            BeginStateChangeDelaySection();
            mSubState->Update(secondsSinceLastFrame);
            EndStateChangeDelaySection();
        }

        void MainGame::Exit()
        {
            ChangeState(NULL);

            Framework::BaseGameState::Exit();

            REMOVE_OBJECT_TOP(mTopScreenBackground, LAYER_BACKGROUND);
            REMOVE_OBJECT_BOTTOM(mBottomScreenBackground, LAYER_BACKGROUND);

            UnloadLevel();

            UnloadSounds();
        }

        void MainGame::UnloadLevel()
        {
            mLevel.Unload();
        }


        void MainGame::Shutdown()
        {
            Framework::BaseGameState::Shutdown();
        }


        void MainGame::LoadLevel()
        {
            // clean up the old one before we start again
            UnloadLevel();

            const char * levelfile;
            if (USE_MAIN_GAME_PROGRESSION == mLevelProgression)
            {
                levelfile = SuperMariaCosmos::LevelManager.GetLevelFilename();
            }
            else
            {
                levelfile = mLevelFile.c_str();
            }

            mLevel.Load(levelfile);
        }

        // tells us that from now until EndStateChangeDelaySection we are not
        // allowed to actuallly do a state change.
        void MainGame::BeginStateChangeDelaySection()
        {
            mIsInStateChangeDelaySection = true;
        }

        // tells us that we are allowed to do state changes again.
        // if one was queued up, we do it now
        void MainGame::EndStateChangeDelaySection()
        {
            mIsInStateChangeDelaySection = false;

            if (mDelayedStateChangeRequested)
            {
                mDelayedStateChangeRequested = false;
                ChangeState(mDelayedStateChange);
            }
        }


        void MainGame::ChangeState(MainGameStates::MainGameState *pNextState)
        {
            // we may have to delay it
            if (mIsInStateChangeDelaySection)
            {
                mDelayedStateChange = pNextState;
                mDelayedStateChangeRequested = true;
            }
            else
            {
                // good to execute the state change

                if (NULL != mSubState)
                {
                    mSubState->Exit();
                }

                mSubState = pNextState;

                if (NULL != mSubState)
                {
                    mSubState->Enter(this);
                }
            }
        }

        // go back to the main menu
        void MainGame::QuitGame()
        {
            GetGame()->ChangeGameState(&SuperMariaCosmos::GameStates::PressStartGameState);
        }

        // tell the game where to get it's levels from and how the progression should work
        void MainGame::Configure(LevelProgression levelProgression, const char* file)
        {
            mLevelProgression = levelProgression;

            if (NULL != file)
            {
                mLevelFile = file;
            }
        }

        void MainGame::OnLevelComplete()
        {
            if (USE_MAIN_GAME_PROGRESSION == mLevelProgression)
            {
                ChangeState(&MainGameStates::LevelCompleteState);
            }
            else
            {
                LevelEditorGameState.Configure(mLevelFile.c_str());
                GetGame()->ChangeGameState(&LevelEditorGameState);
            }
        }

        void MainGame::OnLevelFailed()
        {
            if (USE_MAIN_GAME_PROGRESSION == mLevelProgression)
            {
                ChangeState(&MainGameStates::LevelFailedState);
            }
            else
            {
                LevelEditorGameState.Configure(mLevelFile.c_str());
                GetGame()->ChangeGameState(&LevelEditorGameState);
            }
        }

        void MainGame::LoadSounds()
        {
            SuperMariaCosmos::Sound.LoadSound("Content/Sound/jump.wav", Subsystems::Sound::SOUND_A);
            SuperMariaCosmos::Sound.LoadSound("Content/Sound/keypickup.wav", Subsystems::Sound::SOUND_B);
            SuperMariaCosmos::Sound.LoadSound("Content/Sound/win.wav", Subsystems::Sound::SOUND_C);
            SuperMariaCosmos::Sound.LoadSound("Content/Sound/burn.wav", Subsystems::Sound::SOUND_D);
        }

        void MainGame::UnloadSounds()
        {
            SuperMariaCosmos::Sound.UnloadSound(Subsystems::Sound::SOUND_A);
            SuperMariaCosmos::Sound.UnloadSound(Subsystems::Sound::SOUND_B);
            SuperMariaCosmos::Sound.UnloadSound(Subsystems::Sound::SOUND_C);
            SuperMariaCosmos::Sound.UnloadSound(Subsystems::Sound::SOUND_D);
        }
    }
}

