/*
GameState for the press start screen
*/
#include "PressStart.h"
#include "MainGame.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/GameStates/GameStates.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

#include <windows.h>

static const char * CUSTOM_LEVEL_FILE_NAME = "custom.lvl";

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        const float PressStart::TITLE_OSCILLATE_SPEED = 0.25f * 2.f * PI; // radians / second.  (Frequency * 2pi)
        const float PressStart::TITLE_OSCILLATE_AMOUNT = 3.f; // how far up AND down the oscillation goes (pixels)

        PressStart::PressStart()
            : Framework::BaseGameState(),
              mTitleImage(NULL),
              mScreenBorder(NULL),
              mBottomScreenImage(NULL),
              mSky(NULL),
              mPlayButton(NULL),
              mLevelEditButton(NULL),
              mTitleOscillationTheta(0.f)
        {
        }

        // beginning this game mode
        void PressStart::Enter()
        {
            // create objects
            ADD_OBJECT_TOP(mSky, SceneObjects::StarrySky, (false), LAYER_BACKGROUND);
            ADD_OBJECT_TOP(mTitleImage,       ::SceneObjects::Sprite, ("Content/Textures/Title.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight),          SuperMariaCosmos::LAYER_BACKGROUND);
            ADD_OBJECT_TOP(mScreenBorder,         ::SceneObjects::Sprite, ("Content/Textures/screenborder.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight),          SuperMariaCosmos::LAYER_BACKGROUND);
            ADD_OBJECT_BOTTOM(mBottomScreenImage, ::SceneObjects::Sprite, ("Content/Textures/MenuBackground.png", SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight), SuperMariaCosmos::LAYER_BACKGROUND);
            ADD_WIDGET(
                mPlayButton,
                SuperMariaCosmos::SceneObjects::TitleScreenButton,
                ("Content/Textures/play_button_up.png", "Content/Textures/play_button_down.png"));
            ADD_WIDGET(
                mLevelEditButton,
                SuperMariaCosmos::SceneObjects::TitleScreenButton,
                ("Content/Textures/leveledit_button_up.png", "Content/Textures/leveledit_button_down.png"));

            // tell em where to go
            mLevelEditButton->SetPosition(128, 60 );
            mPlayButton->SetPosition(     128, 132 );
        }

        void PressStart::Update(float secondsSinceLastFrame)
        {
            HandleInput();

            ProcessUI();

            // title oscillation
            mTitleOscillationTheta += TITLE_OSCILLATE_SPEED * secondsSinceLastFrame;
            if (mTitleOscillationTheta > 2.f*PI) {mTitleOscillationTheta -= 2.f*PI;}
            mTitleImage->SetY( TITLE_OSCILLATE_AMOUNT * cosf(mTitleOscillationTheta) );
        }

        void PressStart::ProcessUI()
        {
            // big buttons
            if (mPlayButton->IsHeld())
            {
                Sleep(100); // give it time to see that the button is down
                MainGameGameState.Configure(MainGame::USE_MAIN_GAME_PROGRESSION);
                LevelManager.Restart();
                GetGame()->ChangeGameState(&SuperMariaCosmos::GameStates::MainGameGameState);
            }
            else if (mLevelEditButton->IsHeld())
            {
                Sleep(100); // give it time to see that the button is down
                GameStates::LevelEditorGameState.Configure(CUSTOM_LEVEL_FILE_NAME, CUSTOM_LEVEL_FILE_NAME);
                GetGame()->ChangeGameState(&GameStates::LevelEditorGameState);
            }
        }


        void PressStart::Exit()
        {
            // don't leak
            REMOVE_OBJECT_TOP(mSky, SuperMariaCosmos::LAYER_BACKGROUND);
            REMOVE_OBJECT_TOP(mTitleImage,    SuperMariaCosmos::LAYER_BACKGROUND);
            REMOVE_OBJECT_TOP(mScreenBorder,  SuperMariaCosmos::LAYER_BACKGROUND);
            REMOVE_OBJECT_BOTTOM(mBottomScreenImage, SuperMariaCosmos::LAYER_BACKGROUND);
            REMOVE_WIDGET(mPlayButton);
            REMOVE_WIDGET(mLevelEditButton);
        }


        // take a look at input and see if it should do anything about it.
        void PressStart::HandleInput()
        {
            float mousex = SuperMariaCosmos::InputManager.GetMouseX();
            float mousey = SuperMariaCosmos::InputManager.GetMouseY();

            SuperMariaCosmos::WidgetManager.OnMouseMove(mousex, mousey);

            if (SuperMariaCosmos::InputManager.GetLeftMouseButton().WasJustPressed() )
            {
                SuperMariaCosmos::WidgetManager.OnMouseDown(mousex, mousey);
            }
            else if (SuperMariaCosmos::InputManager.GetLeftMouseButton().WasJustReleased() )
            {
                SuperMariaCosmos::WidgetManager.OnMouseUp(mousex, mousey);
            }
        }
    }
}

