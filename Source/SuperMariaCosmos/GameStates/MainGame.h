// GameState for the main game
#ifndef MAINGAME_H
#define MAINGAME_H

#include "Framework/BaseGameState.h"
#include "SuperMariaCosmos/SceneObjects/SuperMaria.h"
#include "SuperMariaCosmos/SceneObjects/StarrySky.h"
#include "SuperMariaCosmos/CollisionSurface.h"
#include "SuperMariaCosmos/Level.h"
#include <string>

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        namespace MainGameStates
        {
            class MainGameState;
        }

        class MainGame : public Framework::BaseGameState
        {
            // letting the base substate class see my privates
            friend class MainGameStates::MainGameState;
        public:
            MainGame();
            virtual void Init(Framework::BaseGame *pGame);
            virtual void Shutdown();

            virtual void Enter();
            virtual void Update(float secondsSinceLastFrame);
            virtual void Exit();

            enum LevelProgression
            {
                USE_MAIN_GAME_PROGRESSION,
                USE_SPECIFIED_FILE,
            };

            // tell the game where to get it's levels from and how the progression should work
            virtual void Configure(LevelProgression levelProgression, const char* file = NULL);
        protected:
            virtual void LoadSounds();
            virtual void UnloadSounds();
        private:
            // go back to the main menu
            void QuitGame();

            void LoadLevel();
            void UnloadLevel();
            void OnLevelComplete();
            void OnLevelFailed();
            void ChangeState(MainGameStates::MainGameState *pNextState);

            std::string mLevelFile;
            Level mLevel;
            LevelProgression mLevelProgression;

            SceneObjects::StarrySky *mBottomScreenBackground;
            SceneObjects::StarrySky *mTopScreenBackground;

            // there are times when we need to put off executing the substate change,
            // because we may be in the process of using the current state
            void BeginStateChangeDelaySection();
            void EndStateChangeDelaySection();
            MainGameStates::MainGameState *mDelayedStateChange;

            MainGameStates::MainGameState *mSubState;

            bool mIsInStateChangeDelaySection;
            bool mDelayedStateChangeRequested;
        };
    }
}

#endif // MAINGAME_H

