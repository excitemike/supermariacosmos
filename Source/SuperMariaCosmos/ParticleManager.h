// "lighter" particle manager for instantiating as you need it instead of as
// a whole subsystem to itself
#ifndef SUPERMARIACOSMOS_PARTICLEMANAGER_H
#define SUPERMARIACOSMOS_PARTICLEMANAGER_H

#include "Subsystems/RenderManager.h"
#include "Utilities/TextureManager.h"
#include <vector>

namespace SuperMariaCosmos
{
    class ParticleManager
    {
    public:
        ParticleManager();
        virtual ~ParticleManager(){}

        virtual void Init(unsigned maxParticles);
        virtual void Render(Subsystems::RenderManager* pRenderer);
        virtual void Update(float secondsSinceLastFrame);
        virtual void Shutdown();

        struct ParticleData
        {
            const Utilities::Texture* texture;
            float x;
            float y;
            float velocity_x;
            float velocity_y;
            float remaining_lifetime;
        };

        virtual void SpawnParticle(const ParticleData& particle);
    protected:
        class Particle : public ParticleData
        {
        public:
            Particle();
            Particle(const ParticleData& data);
            Particle& operator=(const ParticleData& data);
            void Update(float secondsSinceLastFrame);
            void Render(Subsystems::RenderManager* pRenderer);
            inline bool IsAlive() const;
        private:
            float rotation;
        };

        // helper function for Shutdown
        static void DeleteParticle(Particle*);

		// helper function for when rendering a collection of particles
		static void RenderParticle(ParticleManager::Particle *pParticle, Subsystems::RenderManager* pRenderer);

		// helper function for when updating a collection of particles
		static void UpdateParticle(ParticleManager::Particle *pParticle, float secondsSinceLastFrame);

    private:
        typedef std::vector<Particle*> ParticleList;
        ParticleList mParticles;
    };
}


#endif // SUPERMARIACOSMOS_PARTICLEMANAGER_H


