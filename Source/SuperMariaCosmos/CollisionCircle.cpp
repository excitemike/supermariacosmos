#include "SuperMariaCosmos/CollisionCircle.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    CollisionCircle::CollisionCircle()
    {
    }

    CollisionCircle::CollisionCircle(
        float center_x, float center_y,
        float radius, bool is_outer) :
        CenterX(center_x),
        CenterY(center_y),
        Radius(radius),
        IsOuter(is_outer)
    {
    }

    // calculate the distance from a given point to this surface.
    float CollisionCircle::GetDistance(float x, float y)
    {
        float distance_from_center = Distance(x, y, CenterX, CenterY);
        
        return fabsf(distance_from_center - Radius);
    }

    // Projects both points onto the surface, and tries to move along 
    // the surface to get as near as it can to target without going beyond 
    // the distance limit.  If it would move off the end of the surface and 
    // onto another, then it will return a pointer to that surface.
    // Returns null if it did not move onto another surface.
    CollisionSurface* CollisionCircle::MoveAlongSurface(
        float start_x, float start_y,
        float target_x, float target_y,
        float max_distance,
        float object_radius,
        float *out_final_x,
        float *out_final_y,
        float *out_distance_travelled,
        float *out_normal_angle,
        Direction *out_direction)
    {
        float start_angle = GetAngleTo(start_x, start_y);
        float target_angle = GetAngleTo(target_x, target_y);

        float angle_change = GetShortestAngleTo(start_angle, target_angle);

        // stay within the limits
        float dividable_radius = (Radius > LENGTH_EPSILON) ? Radius : LENGTH_EPSILON;
        float max_angle_change = max_distance / dividable_radius;
        if (angle_change < -max_angle_change) 
        {
            angle_change = -max_angle_change;
        }
        if (angle_change > max_angle_change) 
        {
            angle_change = max_angle_change;
        }

        float final_angle = start_angle + angle_change;
        float total_radius;
        if (IsOuter)
        {
            total_radius = Radius + object_radius;
        }
        else
        {
            total_radius = Radius - object_radius;
        }

        if (NULL != out_final_x)
        {
            *out_final_x = CenterX + (total_radius * cosf(final_angle));
        }
        if (NULL != out_final_y)
        {
            *out_final_y = CenterY + (total_radius * sinf(final_angle));
        }
        if (NULL != out_distance_travelled)
        {
            *out_distance_travelled = fabsf(angle_change * Radius);
        }
        if (NULL  != out_normal_angle)
        {
            if (IsOuter)
            {
                *out_normal_angle = final_angle;
            }
            else
            {
                *out_normal_angle = final_angle + PI;
            }
        }
        if (NULL != out_direction)
        {
            if ( (-ANGLE_EPSILON < angle_change) && (angle_change < ANGLE_EPSILON) )
            {
                *out_direction = NONE;
            }
            else
            {
                *out_direction = (angle_change > 0.f) ? COUNTERCLOCKWISE : CLOCKWISE;
            }
        }

        // a circle has no ends, so there is never a handoff
        return NULL;
    }

    // find the point on the surface nearest the given point
    void CollisionCircle::GetNearestPointOnSurface(float x, float y, float *out_x, float *out_y)
    {
        float angle = GetAngleTo(x, y);

        *out_x = CenterX + (Radius * cosf(angle));
        *out_y = CenterY + (Radius * sinf(angle));
    }

    // returns angle from center to the specified point
    // returns a value in [-PI, PI]
    float CollisionCircle::GetAngleTo(float x, float y)
    {
        float xdiff = (x - CenterX);
        float ydiff = (y - CenterY);

        return SafeArcTan(ydiff, xdiff);
    }
}



