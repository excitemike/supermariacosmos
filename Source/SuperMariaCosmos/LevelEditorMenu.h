#ifndef SUPERMARIACOSMOS_LEVELEDITORMENU_H
#define SUPERMARIACOSMOS_LEVELEDITORMENU_H

#include "SuperMariaCosmos/SceneObjects/EditorTrayBackground.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObjectButton.h"
#include <vector>

namespace SuperMariaCosmos
{
    namespace GameStates
    {
        class LevelEditor;
    }

    class LevelEditorMenu
    {
    public:
        LevelEditorMenu();

        void Init();
        void Shutdown();

        void Show();
        void Hide();

        void SetLevelEditor(GameStates::LevelEditor* pLevelEditor){mLevelEditor = pLevelEditor;}

        void ProcessInput();
    protected:
        void AddLevelObjectButtons();
        void RemoveLevelObjectButtons();
        void HideLevelObjectButtons();
        void ShowLevelObjectButtons();
        void ProcessInputForObjectButtons();
        void PositionObjectButtons();
    private:
        SuperMariaCosmos::SceneObjects::BaseButton *mOpenMenuButton;

        SuperMariaCosmos::SceneObjects::EditorTrayBackground *mTrayBackground;
        SuperMariaCosmos::SceneObjects::BaseButton           *mPlayLevelButton;
        SuperMariaCosmos::SceneObjects::BaseButton           *mSaveAndExitButton;
        SuperMariaCosmos::SceneObjects::BaseButton           *mExitWithoutSaveButton;
        SuperMariaCosmos::SceneObjects::BaseButton           *mReturnButton;

        typedef std::vector<SuperMariaCosmos::SceneObjects::LevelObjects::LevelObjectButton*> LevelObjectButtonList;
        LevelObjectButtonList mLevelObjectButtons;

        GameStates::LevelEditor* mLevelEditor;
    };
}

#endif // SUPERMARIACOSMOS_LEVELEDITORMENU_H
