#include "SuperMariaCosmos/LevelEditorMenu.h"
#include "SuperMariaCosmos/GameStates/LevelEditor.h"
#include "SuperMariaCosmos/Shared.h"
#include <windows.h>


namespace SuperMariaCosmos
{
    LevelEditorMenu::LevelEditorMenu() :
        mOpenMenuButton(NULL),
        mTrayBackground(NULL),
        mPlayLevelButton(NULL),
        mSaveAndExitButton(NULL),
        mExitWithoutSaveButton(NULL),
        mReturnButton(NULL)
    {
    }

    void LevelEditorMenu::Init()
    {
        float button_width = 128.f;
        float button_height = 32.f;

        ADD_WIDGET(
            mTrayBackground,
            SuperMariaCosmos::SceneObjects::EditorTrayBackground,
            ());

        ADD_WIDGET(
            mOpenMenuButton,
            SuperMariaCosmos::SceneObjects::BaseButton,
            ("Content/Textures/openmenu_up.png", "Content/Textures/openmenu_down.png", button_width, button_height));

        ADD_WIDGET(
            mReturnButton,
            SuperMariaCosmos::SceneObjects::BaseButton,
            ("Content/Textures/return_up.png", "Content/Textures/return_down.png", button_width, button_height));

        ADD_WIDGET(
            mPlayLevelButton,
            SuperMariaCosmos::SceneObjects::BaseButton,
            ("Content/Textures/playlevel_up.png", "Content/Textures/playlevel_down.png", button_width, button_height));

        ADD_WIDGET(
            mExitWithoutSaveButton,
            SuperMariaCosmos::SceneObjects::BaseButton,
            ("Content/Textures/exit_up.png", "Content/Textures/exit_down.png", button_width, button_height));

        ADD_WIDGET(
            mSaveAndExitButton,
            SuperMariaCosmos::SceneObjects::BaseButton,
            ("Content/Textures/saveandexit_up.png", "Content/Textures/saveandexit_down.png", button_width, button_height));

        AddLevelObjectButtons();

        float bottom_button_y = 0.5f * button_height + 12.f;

        float screen_center_x = 1.f + SuperMariaCosmos::ScreenWidth * 0.5f;
        float button_row_4_y = bottom_button_y;
        float button_row_3_y = button_row_4_y + button_height;
        float button_row_2_y = button_row_3_y + button_height;
        float button_row_1_y = button_row_2_y + button_height;

        mOpenMenuButton->SetPosition(        screen_center_x, button_row_4_y);
        mReturnButton->SetPosition(          screen_center_x, button_row_4_y);
        mPlayLevelButton->SetPosition(       screen_center_x, button_row_3_y);
        mExitWithoutSaveButton->SetPosition( screen_center_x, button_row_2_y);
        mSaveAndExitButton->SetPosition(     screen_center_x, button_row_1_y );

        PositionObjectButtons();
    }


    // calculate positions for the level object buttons
    void LevelEditorMenu::PositionObjectButtons()
    {
        float level_object_button_y = 160.f;

        float num_level_objects = (float)mLevelObjectButtons.size();
        float width_of_button = mLevelObjectButtons.front()->GetWidth(); // hmm.  hack
        float half_width_of_button = width_of_button * 0.5f;
        float space_lost_to_button_widths = width_of_button * num_level_objects;
        float extra_space = ScreenWidth - space_lost_to_button_widths;
        // need space between buttons AND on either side of them
        float gap_size = extra_space / (num_level_objects + 1.f);
        float start_x = gap_size + half_width_of_button;
        float spacing = gap_size + width_of_button;

        float cur_x = start_x;

        LevelObjectButtonList::iterator it = mLevelObjectButtons.begin();

        for ( ; it != mLevelObjectButtons.end(); ++it)
        {
            (*it)->SetPosition(cur_x, level_object_button_y);
            cur_x += spacing;
        }
    }

    void LevelEditorMenu::AddLevelObjectButtons()
    {
        SceneObjects::LevelObjectType button_types[] =
            {
                SceneObjects::LEVEL_OBJECT_ARC,
                SceneObjects::LEVEL_OBJECT_CIRCLE,
                SceneObjects::LEVEL_OBJECT_RECT,
                SceneObjects::LEVEL_OBJECT_GOAL,
                SceneObjects::LEVEL_OBJECT_KEY,
                SceneObjects::LEVEL_OBJECT_MARIA
            };

        int num_buttons = sizeof(button_types) / sizeof(button_types[0]);

        for (int i = 0; i < num_buttons; ++i)
        {
            SceneObjects::LevelObjects::LevelObjectButton *pLevelObjectButton=NULL;

            ADD_WIDGET(
                pLevelObjectButton,
                SceneObjects::LevelObjects::LevelObjectButton,
                (button_types[i]));

            mLevelObjectButtons.push_back(pLevelObjectButton);
        }
    }

    void LevelEditorMenu::RemoveLevelObjectButtons()
    {
        LevelObjectButtonList::reverse_iterator it = mLevelObjectButtons.rbegin();

        for ( ; it != mLevelObjectButtons.rend(); ++it)
        {
            REMOVE_WIDGET(*it);
        }
        mLevelObjectButtons.clear();
    }

    void LevelEditorMenu::HideLevelObjectButtons()
    {
        LevelObjectButtonList::iterator it = mLevelObjectButtons.begin();

        for ( ; it != mLevelObjectButtons.end(); ++it)
        {
            (*it)->Hide();
        }
    }

    void LevelEditorMenu::ShowLevelObjectButtons()
    {
        LevelObjectButtonList::iterator it = mLevelObjectButtons.begin();

        for ( ; it != mLevelObjectButtons.end(); ++it)
        {
            (*it)->Show();
        }
    }

    void LevelEditorMenu::Shutdown()
    {
        RemoveLevelObjectButtons();

        REMOVE_WIDGET(mSaveAndExitButton);
        REMOVE_WIDGET(mExitWithoutSaveButton);
        REMOVE_WIDGET(mPlayLevelButton);
        REMOVE_WIDGET(mReturnButton);
        REMOVE_WIDGET(mTrayBackground);
        REMOVE_WIDGET(mOpenMenuButton);
    }

    void LevelEditorMenu::Hide()
    {
        mTrayBackground->Hide();
        mReturnButton->Hide();
        mPlayLevelButton->Hide();
        mExitWithoutSaveButton->Hide();
        mSaveAndExitButton->Hide();

        HideLevelObjectButtons();

        mOpenMenuButton->Show();
    }

    void LevelEditorMenu::Show()
    {
        mTrayBackground->Show();
        mReturnButton->Show();
        mPlayLevelButton->Show();
        mExitWithoutSaveButton->Show();
        mSaveAndExitButton->Show();

        ShowLevelObjectButtons();

        mOpenMenuButton->Hide();
    }


    void LevelEditorMenu::ProcessInput()
    {
        if (mSaveAndExitButton->IsHeld())
        {
            Sleep(100); // give it time to see that the button is down
            mLevelEditor->SaveAndExit();
        }
        else if (mOpenMenuButton->IsHeld())
        {
            Sleep(100); // give it time to see that the button is down
            Show();
        }
        else if (mReturnButton->IsHeld())
        {
            Sleep(100); // give it time to see that the button is down
            Hide();
        }
        else if (mExitWithoutSaveButton->IsHeld())
        {
            Sleep(100); // give it time to see that the button is down
            mLevelEditor->ExitWithoutSave();
        }
        else if (mPlayLevelButton->IsHeld())
        {
            Sleep(100); // give it time to see that the button is down
            mLevelEditor->PlayLevel();
        }
        else
        {
            ProcessInputForObjectButtons();
        }
    }


    void LevelEditorMenu::ProcessInputForObjectButtons()
    {
        LevelObjectButtonList::iterator it = mLevelObjectButtons.begin();

        for ( ; it != mLevelObjectButtons.end(); ++it)
        {
            if ( (*it)->IsHeld() )
            {
                Sleep(100);
                mLevelEditor->SetCurrentLevelObjectType( (*it)->GetType() );
                Hide();
                break;
            }
        }
    }
}
