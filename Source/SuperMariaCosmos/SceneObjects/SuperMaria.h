#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_SUPERMARIA_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_SUPERMARIA_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/SpriteLevelObject.h"
#include "SuperMariaCosmos/SpriteAnimation.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class SuperMaria : public SpriteLevelObject
        {
        public:
            SuperMaria();

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file);

            // set up this object based on what we read in
            virtual void Read(FILE *file);

            // for safely casting to a derived class if you have a LeveObject 
            // pointer.  Unfortunately, this does mean that my baseclass 
            // needs to know about all its children.  That happens to be fine 
            // for my case.
            // base class sets the ptr to NULL and returns false.  derived 
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(SuperMaria **ptr){*ptr = this; return true;}

            void SetVelocity(float velocityX, float velocityY) {mVelocityX = velocityX; mVelocityY = velocityY;}
            float GetVelocityX() const {return mVelocityX;}
            float GetVelocityY() const {return mVelocityY;}
            void AddToVelocity(float velocityX, float velocityY) {mVelocityX += velocityX; mVelocityY += velocityY;}

            void AccelToward(float x, float y, float deltaV);

            void RotateToward(float desired_angle, float max_rotation);

            void LimitSpeed(float speedLimit);

            void Update(float secondsSinceLastFrame);

            float GetCollisionRadius();

            enum State
            {
                FREEFALL,
                STAND,
                WALK,
                BURN,
                VICTORY
            };

            State GetState(){return mState;}
            void SetState(State state);
        private:
            static constexpr float IMAGE_WIDTH  = 24.f;
            static constexpr float IMAGE_HEIGHT = 24.f;

            SpriteAnimation *mCurrentAnimation;

            SpriteAnimation mJumpAnimation;
            SpriteAnimation mStandAnimation;
            SpriteAnimation mWalkAnimation;
            SpriteAnimation mBurnAnimation;
            SpriteAnimation mVictoryAnimation;

            float mVelocityX;
            float mVelocityY;

            State mState;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_SUPERMARIA_H
