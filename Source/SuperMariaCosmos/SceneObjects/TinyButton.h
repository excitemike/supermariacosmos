#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_TINYBUTTON_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_TINYBUTTON_H

#include "SuperMariaCosmos/SceneObjects/BaseButton.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class TinyButton : public SuperMariaCosmos::SceneObjects::BaseButton
        {
        public:
            TinyButton(const char* upImage, const char* downImage);
        private:
            static constexpr float IMAGE_WIDTH  = 36.f;
            static constexpr float IMAGE_HEIGHT = 36.f;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_TINYBUTTON_H
