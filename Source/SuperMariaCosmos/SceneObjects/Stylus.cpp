#include "Stylus.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        Stylus::Stylus() : BaseSceneItem()
        {
            mStylusTexture = Utilities::TextureManager::GetTexture("Content/Textures/stylus.png");
            mShadowTexture = Utilities::TextureManager::GetTexture("Content/Textures/stylusshadow.png");
        }

        // move the stylus graphic to indicate where the mouse is
        void Stylus::Update(float secondsSinceLastFrame)
        {
            float mouseX = SuperMariaCosmos::InputManager.GetMouseX();
            float mouseY = SuperMariaCosmos::InputManager.GetMouseY();

            SetPosition(mouseX, mouseY);

            // fudge the y up to simulate holding the stylus above where you would tap
            if (SuperMariaCosmos::InputManager.GetLeftMouseButton().IsDown())
            {
                mStylusFloat = 0;
            }
            else
            {
                mStylusFloat = STYLUS_FLOAT_HEIGHT;
            }
            mouseY += mStylusFloat;

            mStylusAngle = SafeArcTan(STYLUS_HAND_Y-mouseY, STYLUS_HAND_X-mouseX);
            mShadowAngle = SafeArcTan(SHADOW_HAND_Y-mouseY, SHADOW_HAND_X-mouseX);
        }

        void Stylus::Render(Subsystems::RenderManager* pRenderer)
        {
            pRenderer->RenderSprite(
                mShadowTexture,
                mXPos,
                mYPos + SHADOW_Y_OFFSET,
                SHADOW_IMAGE_WIDTH,
                SHADOW_IMAGE_HEIGHT,
                false, false,
                mShadowAngle,
                SHADOW_OFFSET_TO_ROTCENTER_X,
                SHADOW_OFFSET_TO_ROTCENTER_Y);
            pRenderer->RenderSprite(
                mStylusTexture,
                mXPos,
                mYPos + STYLUS_Y_OFFSET + mStylusFloat,
                STYLUS_IMAGE_WIDTH,
                STYLUS_IMAGE_HEIGHT,
                false, false,
                mStylusAngle,
                STYLUS_OFFSET_TO_ROTCENTER_X,
                STYLUS_OFFSET_TO_ROTCENTER_Y);
        }
    }
}
