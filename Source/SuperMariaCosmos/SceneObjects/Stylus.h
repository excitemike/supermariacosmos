#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_STYLUS_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_STYLUS_H

#include "SceneObjects/BaseSceneItem.h"
#include "Utilities/TextureManager.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class Stylus : public ::SceneObjects::BaseSceneItem
        {
        public:
            Stylus();
            
            // move the stylus graphic to indicate where the mouse is
            void Update(float secondsSinceLastFrame);

            void Render(Subsystems::RenderManager*);
        private:

            // where to make it look like the hand is coming from
            static constexpr float STYLUS_HAND_X = 300.f;
            static constexpr float STYLUS_HAND_Y = 300.f;
            static constexpr float SHADOW_HAND_X = 300.f;
            static constexpr float SHADOW_HAND_Y = 275.f;

            static constexpr float STYLUS_FLOAT_HEIGHT = 4.f;

            static constexpr float SHADOW_Y_OFFSET              = -8.f; // adjustment to get the point of the stylus at where the mouse cursor is
            static constexpr float SHADOW_IMAGE_HEIGHT          = 16.f;
            static constexpr float SHADOW_IMAGE_WIDTH           = 256.f;
            static constexpr float SHADOW_OFFSET_TO_ROTCENTER_X = 0.f;
            static constexpr float SHADOW_OFFSET_TO_ROTCENTER_Y = 0.5f * SHADOW_IMAGE_HEIGHT;

            static constexpr float STYLUS_Y_OFFSET              = -8.f;  // adjustment to get the point of the stylus at where the mouse cursor is
            static constexpr float STYLUS_IMAGE_HEIGHT          = 16.f;
            static constexpr float STYLUS_IMAGE_WIDTH           = 256.f;
            static constexpr float STYLUS_OFFSET_TO_ROTCENTER_X = 0.f;
            static constexpr float STYLUS_OFFSET_TO_ROTCENTER_Y = 0.5f * STYLUS_IMAGE_HEIGHT;

            const Utilities::Texture *mStylusTexture;
            const Utilities::Texture *mShadowTexture;

            float mShadowAngle;
            float mStylusAngle;
            float mStylusFloat;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_STYLUS_H
