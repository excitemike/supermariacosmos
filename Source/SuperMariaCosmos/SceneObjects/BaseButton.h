#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_BASEBUTTON_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_BASEBUTTON_H

#include "SceneObjects/BaseWidget.h"
#include "Utilities/TextureManager.h"
#include "Framework/ButtonState.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class BaseButton : public ::SceneObjects::BaseWidget
        {
        public:
            BaseButton(const char* upImage, const char* downImage, float width, float height);
                    
            // return true if this widget handled the event, false if it should be 
            // passed on to something else to handle it
            virtual bool OnMouseDown(float x, float y);
            
            // called every frame regardles of whether it actually moved
            virtual void OnMouseMove(float x, float y);

            void Render(Subsystems::RenderManager*);

            // allow checking of buttonstate
            bool IsUp() const { return mButtonState.IsUp(); }
            bool IsDown() const { return mButtonState.IsDown(); }
            bool IsHeld() const { return mButtonState.IsHeld(); }
            bool WasJustReleased() const { return mButtonState.WasJustReleased(); }
            bool WasJustPressed() const { return mButtonState.WasJustPressed(); }

            float GetWidth(){return mWidth;}
            float GetHeight(){return mHeight;}
        private:
            // tell whether the mouse is currently over this button
            bool IsMouseOver();

            const Utilities::Texture *mUpImage;
            const Utilities::Texture *mDownImage;
            
            Framework::ButtonState mButtonState;

            float mWidth;
            float mHeight;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_BASEBUTTON_H
