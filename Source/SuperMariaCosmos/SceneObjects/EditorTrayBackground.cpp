#include "SuperMariaCosmos/SceneObjects/EditorTrayBackground.h"
#include "SuperMariaCosmos/Shared.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        EditorTrayBackground::EditorTrayBackground() :
            ::SceneObjects::BaseWidget()
        {
            mTexture = Utilities::TextureManager::GetTexture("Content/Textures/screen_overlay.png");
        }

        
        void EditorTrayBackground::Render(Subsystems::RenderManager* pRenderer)
        {
            ::SceneObjects::BaseWidget::Render(pRenderer);

            if (IsVisible())
            {
                pRenderer->RenderSprite(
                    mTexture, 
                    GetX(), GetY(), 
                    SuperMariaCosmos::ScreenWidth, SuperMariaCosmos::ScreenHeight);
            }
        }
    
        // return true if this widget handled the event, false if it should be 
        // passed on to something else to handle it
        bool EditorTrayBackground::OnMouseDown(float x, float y)
        {
            ::SceneObjects::BaseWidget::OnMouseDown(x, y);

            return IsVisible();
        }
    }
}



