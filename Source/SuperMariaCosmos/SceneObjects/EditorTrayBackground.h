#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_EDITORTRAYBACKGROUND_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_EDITORTRAYBACKGROUND_H

#include "SceneObjects/BaseWidget.h"
#include "Utilities/TextureManager.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class EditorTrayBackground : public ::SceneObjects::BaseWidget
        {
        public:
            EditorTrayBackground();

            // return true if this widget handled the event, false if it should be 
            // passed on to something else to handle it
            virtual bool OnMouseDown(float x, float y);

            virtual void Render(Subsystems::RenderManager* pRenderer);
        private:
            const Utilities::Texture *mTexture;
        };
    }
}

#endif // SUPERMARIACOSMOS_SCENEOBJECTS_EDITORTRAYBACKGROUND_H

