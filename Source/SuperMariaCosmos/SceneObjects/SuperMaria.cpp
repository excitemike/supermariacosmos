#include "SuperMariaCosmos/SceneObjects/SuperMaria.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

// images for animations
static const char *JUMP_TEXTURES[]    = {"Content/Textures/character2.png"};
static const char *STAND_TEXTURES[]   = {"Content/Textures/character.png"};
static const char *WALK_TEXTURES[]    = {"Content/Textures/character.png", "Content/Textures/character2.png"};
static const char *BURN_TEXTURES[]    = {"Content/Textures/character_ow.png", "Content/Textures/character_burn1.png", "Content/Textures/character_burn2.png", "Content/Textures/character_ashes1.png", "Content/Textures/character_ashes2.png"};
static const char *VICTORY_TEXTURES[] = {"Content/Textures/character_stand.png", "Content/Textures/character_victory.png"};

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // constructor
        SuperMaria::SuperMaria() :
            SpriteLevelObject(),
            mCurrentAnimation(NULL),
            mVelocityX(0.f),
            mVelocityY(0.f),
            mState(FREEFALL)
        {
            SetDeleteButtonOffset(18.f,18.f);

            mImageWidth = IMAGE_WIDTH;
            mImageHeight = IMAGE_HEIGHT;

            mJumpAnimation.Init(    1.f,   1, JUMP_TEXTURES, false);
            mStandAnimation.Init(   1.f,   1, STAND_TEXTURES, false);
            mWalkAnimation.Init(    0.1f,  2, WALK_TEXTURES, true);
            mBurnAnimation.Init(    0.1f, 5, BURN_TEXTURES, false);
            mVictoryAnimation.Init( 0.25f,  2, VICTORY_TEXTURES, false);

            mCurrentAnimation = &mStandAnimation;
            SetTexture(mCurrentAnimation->GetCurrentFrame());
        }

        // move to where that velocity would take it in the specified time
        void SuperMaria::Update(float secondsSinceLastFrame)
        {
            SetX(GetX() + (mVelocityX*secondsSinceLastFrame));
            SetY(GetY() + (mVelocityY*secondsSinceLastFrame));

            if (NULL != mCurrentAnimation)
            {
                mCurrentAnimation->Update(secondsSinceLastFrame);
                SetTexture(mCurrentAnimation->GetCurrentFrame());
            }
        }

        // if it is set to move faster than the given speed, slow it down
        void SuperMaria::LimitSpeed(float speedLimit)
        {
            float speedSquared = (mVelocityX*mVelocityX)+(mVelocityY*mVelocityY);
            float limitSquared = speedLimit*speedLimit;

            if (speedSquared > (limitSquared + LENGTH_EPSILON))
            {
                // scale it down
                float scale = sqrtf(limitSquared / speedSquared);
                mVelocityX *= scale;
                mVelocityY *= scale;
            }
        }

        // accelerate this thing toward a point
        void SuperMaria::AccelToward(float x, float y, float deltaV)
        {
            float diffx = x - GetX();
            float diffy = y - GetY();
            float dist = sqrtf( (diffx*diffx)+(diffy*diffy) );

            if (dist > LENGTH_EPSILON)
            {
                float scale = deltaV / dist;
                AddToVelocity( diffx * scale, diffy * scale );
            }
        }

        void SuperMaria::RotateToward(float desired_angle, float max_rotation)
        {
            float delta = GetShortestAngleTo(mRotation, desired_angle);

            // speed limit
            if (delta < -max_rotation) {delta = -max_rotation;}
            if (delta > max_rotation) {delta = max_rotation;}

            mRotation = GetShortestAngleTo(0.f, mRotation+delta); // rotate and keep it in a valid range
        }

        //
        float SuperMaria::GetCollisionRadius()
        {
            return mImageWidth * 0.5f;
        }

        // write the representation of this object to a file (for later reading)
        void SuperMaria::Write(FILE *file)
        {
            fprintf(file, "%d\n", LEVEL_OBJECT_MARIA);
            fprintf(file, "  %f\n", mXPos);
            fprintf(file, "  %f\n", mYPos);
        }

        // set up this object based on what we read in
        void SuperMaria::Read(FILE *file)
        {
            // the line for indicating which type this is should already have been read
            int scanresult;

            scanresult = fscanf(file, "%f", &mXPos);
            scanresult = fscanf(file, "%f", &mYPos);
        }

        void SuperMaria::SetState(State state)
        {
            if (state != mState)
            {
                mState = state;

                // update animations
                switch (mState)
                {
                case FREEFALL:
                    mCurrentAnimation = &mJumpAnimation;
                    break;
                case STAND:
                    mCurrentAnimation = &mStandAnimation;
                    break;
                case WALK:
                    mCurrentAnimation = &mWalkAnimation;
                    break;
                case BURN:
                    mCurrentAnimation = &mBurnAnimation;
                    break;
                case VICTORY:
                    mCurrentAnimation = &mVictoryAnimation;
                    break;
                }
                mCurrentAnimation->Restart();
            }
        }
    }
}
