#include "StarrySky.h"
#include "Framework/math.h"
#include "SuperMariaCosmos/Shared.h"

// all these in pixels per second
static const float StarScrollSpeed_y  = 7.f;
static const float CloudScrollSpeed_y = -25.f;
static const float StarScrollSpeed_x  = -5.f;
static const float CloudScrollSpeed_x = +8.f;

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // constructor
        StarrySky::StarrySky(bool darkened) :
            BaseSceneItem(),
            mBackgroundTexture(NULL),
            mStarsTexture(NULL),
            mCloudsTexture(NULL),
            mCloudScrollAmount_x(RandFloat() * ScreenWidth),
            mCloudScrollAmount_y(RandFloat() * ScreenHeight),
            mStarScrollAmount_x(RandFloat() * ScreenWidth),
            mStarScrollAmount_y(RandFloat() * ScreenHeight)
        {
            if (darkened)
            {
                mBackgroundTexture = Utilities::TextureManager::GetTexture("Content/Textures/darkskybackground.png");
            }
            else
            {
                mBackgroundTexture = Utilities::TextureManager::GetTexture("Content/Textures/skybackground.png");
            }

            mStarsTexture = Utilities::TextureManager::GetTexture("Content/Textures/stars.png");
            mCloudsTexture = Utilities::TextureManager::GetTexture("Content/Textures/background_clouds.png");
        }

        void StarrySky::Update(float secondsSinceLastFrame)
        {
            BaseSceneItem::Update(secondsSinceLastFrame);

            // scrolling!

            mStarScrollAmount_x += (StarScrollSpeed_x * secondsSinceLastFrame);
            mStarScrollAmount_x = fmodf(mStarScrollAmount_x, ScreenWidth);
            if (mStarScrollAmount_x < 0.f) {mStarScrollAmount_x += ScreenWidth;}

            mStarScrollAmount_y += (StarScrollSpeed_y * secondsSinceLastFrame);
            mStarScrollAmount_y = fmodf(mStarScrollAmount_y, ScreenHeight);
            if (mStarScrollAmount_y < 0.f) {mStarScrollAmount_y += ScreenHeight;}

            mCloudScrollAmount_x += (CloudScrollSpeed_x * secondsSinceLastFrame);
            mCloudScrollAmount_x = fmodf(mCloudScrollAmount_x, ScreenWidth);
            if (mCloudScrollAmount_x < 0.f) {mCloudScrollAmount_x += ScreenWidth;}


            mCloudScrollAmount_y += (CloudScrollSpeed_y * secondsSinceLastFrame);
            mCloudScrollAmount_y = fmodf(mCloudScrollAmount_y, ScreenHeight);
            if (mCloudScrollAmount_y < 0.f) {mCloudScrollAmount_y += ScreenHeight;}
        }

        void StarrySky::Render(Subsystems::RenderManager* pRenderer)
        {
            BaseSceneItem::Render(pRenderer);

            if (IsVisible())
            {
                float x = GetX();
                float y = GetY();
                float width = ScreenWidth;
                float height = ScreenHeight;

                pRenderer->RenderSprite( mBackgroundTexture, x, y, width, height);

                RenderScrolledSprite(
                    mCloudsTexture,
                    pRenderer,
                    mCloudScrollAmount_x,
                    mCloudScrollAmount_y);

                RenderScrolledSprite(
                    mStarsTexture,
                    pRenderer,
                    mStarScrollAmount_x,
                    mStarScrollAmount_y);
            }
        }

        void StarrySky::RenderScrolledSprite(
                const Utilities::Texture *texture,
                Subsystems::RenderManager* pRenderer,
                float scroll_x, float scroll_y)
        {
                float x = GetX();
                float y = GetY();
                float width = ScreenWidth;
                float height = ScreenHeight;

                float x1 = x+scroll_x;
                float x2 = x+scroll_x-width;
                float y1 = y+scroll_y;
                float y2 = y+scroll_y-height;

                pRenderer->RenderSprite(
                    texture,
                    x1, y1,
                    width, height);

                pRenderer->RenderSprite(
                    texture,
                    x1, y2,
                    width, height);

                pRenderer->RenderSprite(
                    texture,
                    x2, y1,
                    width, height);

                pRenderer->RenderSprite(
                    texture,
                    x2, y2,
                    width, height);
        }
    }
}
