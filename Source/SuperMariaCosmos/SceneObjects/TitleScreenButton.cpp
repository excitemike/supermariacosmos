#include "SuperMariaCosmos/SceneObjects/TitleScreenButton.h"
#include "SuperMariaCosmos/Shared.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // constructor
        TitleScreenButton::TitleScreenButton(const char* upImage, const char* downImage)
            : SuperMariaCosmos::SceneObjects::BaseButton(upImage, downImage, IMAGE_WIDTH, IMAGE_HEIGHT)
        {
        }
    }
}
