#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_TITLESCREENBUTTON_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_TITLESCREENBUTTON_H

#include "SuperMariaCosmos/SceneObjects/BaseButton.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class TitleScreenButton : public SuperMariaCosmos::SceneObjects::BaseButton
        {
        public:
            TitleScreenButton(const char* upImage, const char* downImage);
        private:
            static constexpr float IMAGE_WIDTH  = 192.f;
            static constexpr float IMAGE_HEIGHT = 48.f;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_TITLESCREENBUTTON_H
