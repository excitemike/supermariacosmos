#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_STARRYSKY_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_STARRYSKY_H

#include "SceneObjects/BaseSceneItem.h"
#include "Utilities/TextureManager.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class StarrySky : public ::SceneObjects::BaseSceneItem
        {
        public:
            StarrySky(bool darkened = false);

            void Update(float secondsSinceLastFrame);

            void Render(Subsystems::RenderManager*);
        private:
            void RenderScrolledSprite(
                const Utilities::Texture *texture,
                Subsystems::RenderManager* pRenderer,
                float scroll_x, float scroll_y);

            const Utilities::Texture *mBackgroundTexture;
            const Utilities::Texture *mStarsTexture;
            const Utilities::Texture *mCloudsTexture;

            float mCloudScrollAmount_x;
            float mCloudScrollAmount_y;

            float mStarScrollAmount_x;
            float mStarScrollAmount_y;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_STARRYSKY_H
