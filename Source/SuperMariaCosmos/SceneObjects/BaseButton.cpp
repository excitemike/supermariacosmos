#include "SuperMariaCosmos/SceneObjects/BaseButton.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // constructor
        BaseButton::BaseButton(const char* upImage, const char* downImage, float width, float height)
            : ::SceneObjects::BaseWidget(), mUpImage(NULL), mDownImage(NULL), mButtonState(), mWidth(width), mHeight(height)
        {
            mUpImage = Utilities::TextureManager::GetTexture(upImage);
            mDownImage = Utilities::TextureManager::GetTexture(downImage);
        }

        // return true if this widget handled the event, false if it should be
        // passed on to something else to handle it
        bool BaseButton::OnMouseDown(float x, float y)
        {
            if (IsVisible() && IsMouseOver())
            {
                // let this take it to just-pressed but nothing else
                if (mButtonState.IsUp())
                {
                    mButtonState.Update(true);
                }
                return true;
            }

            return ::SceneObjects::BaseWidget::OnMouseDown(x, y);
        }

        // called every frame regardles of whether it actually moved
        void BaseButton::OnMouseMove(float x, float y)
        {
            // let this take the button to held, but not just-pressed
            bool mouseIsDown = SuperMariaCosmos::InputManager.GetLeftMouseButton().IsDown();
            mButtonState.Update( IsMouseOver() && mouseIsDown && mButtonState.IsDown());

            ::SceneObjects::BaseWidget::OnMouseMove(x, y);
        }

        // tell whether the mouse is currently over this button
        bool BaseButton::IsMouseOver()
        {
            float mouseX = SuperMariaCosmos::InputManager.GetMouseX();
            float mouseY = SuperMariaCosmos::InputManager.GetMouseY();

            float left = GetX() - (0.5f*mWidth);
            float right = left + mWidth;
            float bottom = GetY() - (0.5f*mHeight);
            float top = bottom + mHeight;

            return (left <= mouseX)
                && (mouseX <= right)
                && (bottom <= mouseY)
                && (mouseY <= top);
        }

        // draw me!
        void BaseButton::Render(Subsystems::RenderManager* pRenderer)
        {
            ::SceneObjects::BaseWidget::Render(pRenderer);

            if (mIsVisible)
            {
                const Utilities::Texture *image = IsUp() ? mUpImage : mDownImage;
                pRenderer->RenderSprite(
                    image,
                    mXPos-(0.5f*mWidth),
                    mYPos-(0.5f*mHeight),
                    mWidth, mHeight);
            }
        }
    }
}
