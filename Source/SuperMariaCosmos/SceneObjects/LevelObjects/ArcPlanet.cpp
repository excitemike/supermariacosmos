#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/ArcPlanet.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"
#include <assert.h>


namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        const Subsystems::RenderManager::Color ArcPlanet::INNER_COLOR = {0.f,0.8f,0.f,1.f};
        const Subsystems::RenderManager::Color ArcPlanet::MIDDLE_COLOR = {0.f,0.f,0.f,1.f};
        const Subsystems::RenderManager::Color ArcPlanet::OUTER_COLOR = {0.f,0.8f,0.f,1.f};
        const Subsystems::RenderManager::Color ArcPlanet::DANGER_INNER_COLOR =  {1.f,1.f,0.5f,1.f};
        const Subsystems::RenderManager::Color ArcPlanet::DANGER_MIDDLE_COLOR = {1.f,0.f,0.f,1.f};
        const Subsystems::RenderManager::Color ArcPlanet::DANGER_OUTER_COLOR =  {1.f,1.f,0.5f,1.f};
        const float ArcPlanet::SELECTION_BORDER_SIZE = 2.f;
        const float ArcPlanet::MIN_ARC_THICKNESS = 12.f;
        const float ArcPlanet::MIN_INNER_RADIUS = 2.f * (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);

        ArcPlanet::ArcPlanet() :
            BasePlanet(),
            mStartAngle(0.75f * PI),
            mEndAngle(0.25f * PI),
            mInnerRadius(24.f),
            mOuterRadius(36.f)
        {
            // set up collision surface handoffs
            unsigned index=0;
            mSurfacesInOrder[index++] = &mInnerStartCornerCollisionArc;
            mSurfacesInOrder[index++] = &mStartCollisionLine;
            mSurfacesInOrder[index++] = &mOuterStartCornerCollisionArc;
            mSurfacesInOrder[index++] = &mOuterCollisionArc;
            mSurfacesInOrder[index++] = &mOuterEndCornerCollisionArc;
            mSurfacesInOrder[index++] = &mEndCollisionLine;
            mSurfacesInOrder[index++] = &mInnerEndCornerCollisionArc;
            mSurfacesInOrder[index++] = &mInnerCollisionArc;

            for (unsigned i=0; i<NUM_SURFACES; ++i)
            {
                unsigned prev = (i) % NUM_SURFACES;
                unsigned cur  = (i+1) % NUM_SURFACES;
                unsigned next = (i+2) % NUM_SURFACES;

                mSurfacesInOrder[cur]->SetPrevSurface( mSurfacesInOrder[prev] );
                mSurfacesInOrder[cur]->SetNextSurface( mSurfacesInOrder[next] );
            }
            UpdateButtonPositions();
        }

        void ArcPlanet::DragUpdate(BasePlanet::HandleId handle, float x, float y)
        {
            // hey I guess we are dragging something
            switch (mCurrentHandle)
            {
            case NONE:
                // not actually dragging anything
                break;
            case MOVE:
                SetX( x - mDragInfo.start_xdiff );
                SetY( y - mDragInfo.start_ydiff );
                break;
            case START_ANGLE:
                DragUpdate_StartAngle(x, y);
                break;
            case END_ANGLE:
                DragUpdate_EndAngle(x, y);
                break;
            case INNER_RADIUS:
                DragUpdate_InnerRadius(x, y);
                break;
            case OUTER_RADIUS:
                DragUpdate_OuterRadius(x, y);
                break;
            case INNER_START_CORNER:
                DragUpdate_InnerRadius(x, y);
                DragUpdate_StartAngle(x, y);
                break;
            case OUTER_START_CORNER:
                DragUpdate_OuterRadius(x, y);
                DragUpdate_StartAngle(x, y);
                break;
            case INNER_END_CORNER:
                DragUpdate_InnerRadius(x, y);
                DragUpdate_EndAngle(x, y);
                break;
            case OUTER_END_CORNER:
                DragUpdate_OuterRadius(x, y);
                DragUpdate_EndAngle(x, y);
                break;
            }
        }

        void ArcPlanet::DragUpdate_InnerRadius(float mousex, float mousey)
        {
            float mouse_radius = Distance(GetX(), GetY(), mousex, mousey);

            mInnerRadius = mouse_radius - mDragInfo.start_inner_radius;

            // enforce min/max
            if (mInnerRadius < MIN_INNER_RADIUS)
            {
                mInnerRadius = MIN_INNER_RADIUS;
            }

            float arc_thickness = mOuterRadius - mInnerRadius;
            if (arc_thickness < MIN_ARC_THICKNESS)
            {
                mOuterRadius = mInnerRadius + MIN_ARC_THICKNESS;
            }

            UpdateButtonPositions();
        }

        void ArcPlanet::DragUpdate_OuterRadius(float mousex, float mousey)
        {
            float mouse_radius = Distance(GetX(), GetY(), mousex, mousey);

            mOuterRadius = mouse_radius - mDragInfo.start_outer_radius;

            // enforce min/max
            float arc_thickness = mOuterRadius - mInnerRadius;
            if (arc_thickness < MIN_ARC_THICKNESS)
            {
                mInnerRadius = mOuterRadius - MIN_ARC_THICKNESS;

                // got to keep THAT from going too low, too
                if (mInnerRadius < MIN_INNER_RADIUS)
                {
                    mInnerRadius = MIN_INNER_RADIUS;
                    mOuterRadius = mInnerRadius + MIN_ARC_THICKNESS;
                }
            }

            UpdateButtonPositions();
        }

        void ArcPlanet::DragUpdate_StartAngle(float mousex, float mousey)
        {
            float mouse_angle = SafeArcTan( mousey-GetY(), mousex-GetX() );
            mStartAngle = AngleDiff(mDragInfo.start_anglediff, mouse_angle);

            // enforce max/min angle
            float total_arc = AngleDiff(mStartAngle, mEndAngle);
            float how_much_end_handles_stick_out = (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);

            // how big a slice those end handles take up
            float end_handle_slice = SafeArcTan(how_much_end_handles_stick_out, mInnerRadius);
            float max_arc = (2.f * PI) - (2.f * end_handle_slice);
            float min_arc = end_handle_slice; // and make sure we have at least that much for the body/radius handles

            if ( max_arc < total_arc )
            {
                mEndAngle = mStartAngle + max_arc;
            }
            if ( total_arc < min_arc  )
            {
                mEndAngle = mStartAngle + min_arc;
            }

            UpdateButtonPositions();
        }

        void ArcPlanet::DragUpdate_EndAngle(float mousex, float mousey)
        {
            float mouse_angle = SafeArcTan( mousey-GetY(), mousex-GetX() );
            mEndAngle = AngleDiff(mDragInfo.start_endanglediff, mouse_angle);

            // enforce max/min angle
            float total_arc = AngleDiff(mStartAngle, mEndAngle);
            float how_much_end_handles_stick_out = (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);

            // how big a slice those end handles take up
            float end_handle_slice = SafeArcTan(how_much_end_handles_stick_out, mInnerRadius);
            float max_arc = (2.f * PI) - (2.f * end_handle_slice);
            float min_arc = end_handle_slice; // and make sure we have at least that much for the body/radius handles
            if ( max_arc < total_arc )
            {
                mStartAngle = mEndAngle - max_arc;
            }
            if ( total_arc < min_arc  )
            {
                mStartAngle = mEndAngle - min_arc;
            }

            UpdateButtonPositions();
        }

        // renders differently when selected
        void ArcPlanet::DrawSelected(Subsystems::RenderManager* pRenderer)
        {
            DrawSelectionBackground(pRenderer);

            // planet still visible
            DrawPlanet(pRenderer);

            // outer handle
            pRenderer->RenderArc(
                GetX(),
                GetY(),
                mStartAngle,
                mEndAngle,
                mOuterRadius + HANDLE_SPACING + HANDLE_THICKNESS,
                mOuterRadius + HANDLE_SPACING,
                EDGE_HANDLE_COLOR,
                EDGE_HANDLE_COLOR);

            // inner handle
            pRenderer->RenderArc(
                GetX(),
                GetY(),
                mStartAngle,
                mEndAngle,
                mInnerRadius - HANDLE_SPACING,
                mInnerRadius - HANDLE_THICKNESS - HANDLE_SPACING,
                EDGE_HANDLE_COLOR,
                EDGE_HANDLE_COLOR);

            DrawEndHandles(pRenderer);
        }

        void ArcPlanet::DrawSelectionBackground(Subsystems::RenderManager* pRenderer)
        {
            float background_inner_radius = (mInnerRadius - HANDLE_SPACING - HANDLE_SPACING - HANDLE_THICKNESS);
            float background_outer_radius = (mOuterRadius + HANDLE_SPACING + HANDLE_SPACING + HANDLE_THICKNESS);

            float arc_thickness = mOuterRadius - mInnerRadius;
            float end_background_height = HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING;
            float end_background_width = HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING + arc_thickness + HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING;

            // arc adjustor background
            pRenderer->RenderArc(
                GetX(),
                GetY(),
                mStartAngle,
                mEndAngle,
                background_outer_radius,
                background_inner_radius,
                HANDLE_BG_COLOR,
                HANDLE_BG_COLOR
                );
            // background at ends
            RenderAtEnd( pRenderer, mStartAngle,
                background_inner_radius, -end_background_height,
                end_background_width, end_background_height, HANDLE_BG_COLOR);
            RenderAtEnd( pRenderer, mEndAngle,
                background_inner_radius, 0,
                end_background_width, end_background_height, HANDLE_BG_COLOR);
        }

        // render a quad rotating to make the x axis normal to the arc at that
        // angle, and the y axis to be tangent
        void ArcPlanet::RenderAtEnd(
            Subsystems::RenderManager* pRenderer,
            float angle,
            float x, float y,
            float width, float height,
            Subsystems::RenderManager::Color color)
        {
            float left  = x;
            float right = x + width;
            float bottom = y;
            float top = y+height;

            pRenderer->PushRotateAndTranslation(angle, GetX(), GetY());
            pRenderer->RenderQuad(
                left, right,
                bottom, top,
                color
                );
            pRenderer->PopTransformation();
        }

        void ArcPlanet::DrawEndHandles(Subsystems::RenderManager* pRenderer)
        {
            float cur_x;
            float cur_y;
            float arc_thickness = mOuterRadius - mInnerRadius;

            // start inner corner
            cur_x = mInnerRadius - HANDLE_SPACING - HANDLE_THICKNESS;
            cur_y = -HANDLE_SPACING - HANDLE_THICKNESS;
            RenderAtEnd( pRenderer, mStartAngle,
                cur_x, cur_y,
                HANDLE_THICKNESS, HANDLE_THICKNESS,
                CORNER_HANDLE_COLOR );

            // start angle changer
            cur_x = mInnerRadius;
            RenderAtEnd( pRenderer, mStartAngle,
                cur_x, cur_y,
                arc_thickness, HANDLE_THICKNESS,
                EDGE_HANDLE_COLOR);

            // start outer corner
            cur_x = mOuterRadius + HANDLE_SPACING;
            RenderAtEnd( pRenderer, mStartAngle,
                cur_x, cur_y,
                HANDLE_THICKNESS, HANDLE_THICKNESS,
                CORNER_HANDLE_COLOR );

            // end inner corner
            cur_x = mInnerRadius - HANDLE_SPACING - HANDLE_THICKNESS;
            cur_y = HANDLE_SPACING;
            RenderAtEnd( pRenderer, mEndAngle,
                cur_x, cur_y,
                HANDLE_THICKNESS, HANDLE_THICKNESS,
                CORNER_HANDLE_COLOR );

            // start angle changer
            cur_x = mInnerRadius;
            RenderAtEnd( pRenderer, mEndAngle,
                cur_x, cur_y,
                arc_thickness, HANDLE_THICKNESS,
                EDGE_HANDLE_COLOR);

            // start outer corner
            cur_x = mOuterRadius + HANDLE_SPACING;
            RenderAtEnd( pRenderer, mEndAngle,
                cur_x, cur_y,
                HANDLE_THICKNESS, HANDLE_THICKNESS,
                CORNER_HANDLE_COLOR );
        }

        // draw it
        void ArcPlanet::DrawPlanet(Subsystems::RenderManager* pRenderer)
        {
            float middle_radius = 0.5f * (mOuterRadius + mInnerRadius);

            const Subsystems::RenderManager::Color *pInnerColor = (CollisionSurface::DEATHZONE == mSurfaceType)  ? &DANGER_INNER_COLOR  : &INNER_COLOR;
            const Subsystems::RenderManager::Color *pMiddleColor = (CollisionSurface::DEATHZONE == mSurfaceType) ? &DANGER_MIDDLE_COLOR : &MIDDLE_COLOR;
            const Subsystems::RenderManager::Color *pOuterColor = (CollisionSurface::DEATHZONE == mSurfaceType)  ? &DANGER_OUTER_COLOR  : &OUTER_COLOR;

            // actual planet
            pRenderer->RenderArc(
                GetX(),
                GetY(),
                mStartAngle,
                mEndAngle,
                mOuterRadius,
                middle_radius,
                *pOuterColor,
                *pMiddleColor);
            pRenderer->RenderArc(
                GetX(),
                GetY(),
                mStartAngle,
                mEndAngle,
                middle_radius,
                mInnerRadius,
                *pMiddleColor,
                *pInnerColor);
        }


        BasePlanet::HandleId ArcPlanet::GetHandleUnderMouse(float x, float y)
        {
            float xdiff = x - GetX();
            float ydiff = y - GetY();
            float distance_to_mouse = Distance(0.f, 0.f, xdiff, ydiff);
            float angle_diff_to_mouse = AngleDiff(mStartAngle, SafeArcTan(ydiff, xdiff) );
            float angle_diff_to_end   = AngleDiff(mStartAngle, mEndAngle );
            float handle_and_borders_thickness = HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING;
            float inner_handle_radius  = mInnerRadius - handle_and_borders_thickness;
            float outer_handle_radius  = mOuterRadius + handle_and_borders_thickness;

            // get the anglediff at which it would be clockwise from the start handles
            float angle_to_min_handle_angle = AngleDiff( 0.f, SafeArcTan( -handle_and_borders_thickness, distance_to_mouse) );

            // if anglediff goes above this, we have gone past the end handles
            float angle_to_max_handle_angle = angle_diff_to_end + AngleDiff( 0.f, SafeArcTan( handle_and_borders_thickness, distance_to_mouse) );

            // let's condense that all down into some useful booleans so the rest is remotely readable
            bool on_inner_handles  = (inner_handle_radius <= distance_to_mouse) && (distance_to_mouse < mInnerRadius );
            bool on_middle_handles = (mInnerRadius <= distance_to_mouse) && (distance_to_mouse < mOuterRadius );
            bool on_outer_handles  = (mOuterRadius <= distance_to_mouse) && (distance_to_mouse <= outer_handle_radius );

            bool on_start_handles = (angle_to_min_handle_angle <= angle_diff_to_mouse);
            bool on_handles_within_arcs = (angle_diff_to_mouse <= angle_diff_to_end);
            bool on_end_handles = (angle_diff_to_end <= angle_diff_to_mouse) && (angle_diff_to_mouse <= angle_to_max_handle_angle);

            // middle
            if (on_middle_handles && on_handles_within_arcs) return MOVE;

            // radii adjustors
            if (on_outer_handles && on_handles_within_arcs)  return OUTER_RADIUS;
            if (on_inner_handles && on_handles_within_arcs)  return INNER_RADIUS;

            // angle adjustors
            if (on_middle_handles && on_start_handles)       return START_ANGLE;
            if (on_middle_handles && on_end_handles)         return END_ANGLE;

            // corners
            if (on_outer_handles && on_start_handles)        return OUTER_START_CORNER;
            if (on_outer_handles && on_end_handles)          return OUTER_END_CORNER;
            if (on_inner_handles && on_start_handles)        return INNER_START_CORNER;
            if (on_inner_handles && on_end_handles)          return INNER_END_CORNER;

            // didn't find anything at all under the mouse
            return NONE;
        }

        // find out whether the mouse is over the planet (does not consider handles)
        bool ArcPlanet::IsMouseOverPlanet(float x, float y)
        {
            float xdiff = x - GetX();
            float ydiff = y - GetY();

            float dist_squared = (xdiff*xdiff) + (ydiff*ydiff);

            if ((mInnerRadius*mInnerRadius) <= dist_squared)
            {
                if (dist_squared <= (mOuterRadius*mOuterRadius))
                {
                    float angle = SafeArcTan(ydiff, xdiff);

                    if (AngleDiff(mStartAngle, angle) <= AngleDiff(mStartAngle, mEndAngle) )
                    {
                        return true;
                    }
                }
            }

            // guess it's not over it
            return false;
        }

        // write the representation of this object to a file (for later reading)
        void ArcPlanet::Write(FILE *file)
        {
            fprintf(file, "%d\n", LEVEL_OBJECT_ARC);
            fprintf(file, "  %f\n", mXPos);
            fprintf(file, "  %f\n", mYPos);
            fprintf(file, "  %f\n", mStartAngle);
            fprintf(file, "  %f\n", mEndAngle);
            fprintf(file, "  %f\n", mInnerRadius);
            fprintf(file, "  %f\n", mOuterRadius);
            fprintf(file, "  %d\n", mSurfaceType);
        }

        // set up this object based on what we read in
        void ArcPlanet::Read(FILE *file)
        {
            // the line for indicating which type this is should already have been read
            int scanresult;

            scanresult = fscanf(file, "%f", &mXPos);
            scanresult = fscanf(file, "%f", &mYPos);
            scanresult = fscanf(file, "%f", &mStartAngle);
            scanresult = fscanf(file, "%f", &mEndAngle);
            scanresult = fscanf(file, "%f", &mInnerRadius);
            scanresult = fscanf(file, "%f", &mOuterRadius);
            scanresult = fscanf(file, "%d", &mSurfaceType);

            UpdateCollisionSurfaces();
            UpdateButtonPositions();
        }

        // update handles and collision info based on internal
        // variables
        void ArcPlanet::UpdateCollisionSurfaces()
        {
            mOuterCollisionArc.CenterX = GetX();
            mOuterCollisionArc.CenterY = GetY();
            mOuterCollisionArc.StartAngle = mStartAngle;
            mOuterCollisionArc.EndAngle = mEndAngle;
            mOuterCollisionArc.Radius = mOuterRadius;
            mOuterCollisionArc.IsOuter = true;
            mOuterCollisionArc.SetSurfaceType(mSurfaceType);

            mInnerCollisionArc.CenterX = GetX();
            mInnerCollisionArc.CenterY = GetY();
            mInnerCollisionArc.StartAngle = mStartAngle;
            mInnerCollisionArc.EndAngle = mEndAngle;
            mInnerCollisionArc.Radius = mInnerRadius;
            mInnerCollisionArc.IsOuter = false;
            mInnerCollisionArc.SetSurfaceType(mSurfaceType);

            mStartCollisionLine.StartX = GetX() + (mInnerRadius * cosf(mStartAngle) );
            mStartCollisionLine.StartY = GetY() + (mInnerRadius * sinf(mStartAngle) );
            mStartCollisionLine.EndX = GetX() + (mOuterRadius * cosf(mStartAngle) );
            mStartCollisionLine.EndY = GetY() + (mOuterRadius * sinf(mStartAngle) );
            mStartCollisionLine.SetSurfaceType(mSurfaceType);

            mEndCollisionLine.StartX = GetX() + (mOuterRadius * cosf(mEndAngle) );
            mEndCollisionLine.StartY = GetY() + (mOuterRadius * sinf(mEndAngle) );
            mEndCollisionLine.EndX = GetX() + (mInnerRadius * cosf(mEndAngle) );
            mEndCollisionLine.EndY = GetY() + (mInnerRadius * sinf(mEndAngle) );
            mEndCollisionLine.SetSurfaceType(mSurfaceType);

            mInnerStartCornerCollisionArc.CenterX = mStartCollisionLine.StartX;
            mInnerStartCornerCollisionArc.CenterY = mStartCollisionLine.StartY;
            mInnerStartCornerCollisionArc.StartAngle = mStartAngle - PI;
            mInnerStartCornerCollisionArc.EndAngle = mStartAngle - (0.5f*PI);
            mInnerStartCornerCollisionArc.Radius = 0.f;
            mInnerStartCornerCollisionArc.IsOuter = true;
            mInnerStartCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mOuterStartCornerCollisionArc.CenterX = mStartCollisionLine.EndX;
            mOuterStartCornerCollisionArc.CenterY = mStartCollisionLine.EndY;
            mOuterStartCornerCollisionArc.StartAngle = mStartAngle - (0.5f*PI);
            mOuterStartCornerCollisionArc.EndAngle = mStartAngle;
            mOuterStartCornerCollisionArc.Radius = 0.f;
            mOuterStartCornerCollisionArc.IsOuter = true;
            mOuterStartCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mOuterEndCornerCollisionArc.CenterX = mEndCollisionLine.StartX;
            mOuterEndCornerCollisionArc.CenterY = mEndCollisionLine.StartY;
            mOuterEndCornerCollisionArc.StartAngle = mEndAngle;
            mOuterEndCornerCollisionArc.EndAngle = mEndAngle + (0.5f*PI);
            mOuterEndCornerCollisionArc.Radius = 0.f;
            mOuterEndCornerCollisionArc.IsOuter = true;
            mOuterEndCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mInnerEndCornerCollisionArc.CenterX = mEndCollisionLine.EndX;
            mInnerEndCornerCollisionArc.CenterY = mEndCollisionLine.EndY;
            mInnerEndCornerCollisionArc.StartAngle = mEndAngle + (0.5f*PI);
            mInnerEndCornerCollisionArc.EndAngle = mEndAngle + PI;
            mInnerEndCornerCollisionArc.Radius = 0.f;
            mInnerEndCornerCollisionArc.IsOuter = true;
            mInnerEndCornerCollisionArc.SetSurfaceType(mSurfaceType);
        }

        // how many surfaces does this level object have?
        unsigned ArcPlanet::GetNumSurfaces()
        {
            return NUM_SURFACES;
        }

        // how many surfaces does this level object have?
        SuperMariaCosmos::CollisionSurface* ArcPlanet::GetSurface(unsigned index)
        {
            if (index < NUM_SURFACES)
            {
                return mSurfacesInOrder[index];
            }
            else
            {
                assert(false);
                return NULL;
            }

        }

        // record where a drag started so that we can look at how much it
        // has changed since then
        void ArcPlanet::StoreDragInfo(float mousex, float mousey)
        {
            mDragInfo.start_xdiff = mousex - GetX();
            mDragInfo.start_ydiff = mousey - GetY();

            float mouse_radius = Distance(0.f, 0.f, mDragInfo.start_xdiff, mDragInfo.start_ydiff);
            mDragInfo.start_inner_radius = mouse_radius - mInnerRadius;
            mDragInfo.start_outer_radius = mouse_radius - mOuterRadius;

            float mouse_angle = SafeArcTan(mDragInfo.start_ydiff, mDragInfo.start_xdiff);
            mDragInfo.start_anglediff = AngleDiff(mStartAngle, mouse_angle);
            mDragInfo.start_endanglediff = AngleDiff(mEndAngle, mouse_angle);
        }

        // so that we can display this thing in an icon
        void ArcPlanet::Iconify()
        {
            Deselect();
            mStartAngle     = -0.25f * PI;
            mEndAngle       = 1.25f * PI;
            mInnerRadius    = 5.f;
            mOuterRadius    = 12.f;
        }


        // tell it where to draw the delete button
        void ArcPlanet::UpdateButtonPositions()
        {
            float radii_offset = 12.f + (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);
            float type_button_radius = mOuterRadius + radii_offset;
            float delete_button_radius = mInnerRadius - radii_offset;
            float direction = mStartAngle + (0.5f * AngleDiff(mStartAngle, mEndAngle) );
            SetDeleteButtonOffset(
                delete_button_radius * cosf(direction),
                delete_button_radius * sinf(direction) );
            SetTypeButtonOffset(
                type_button_radius * cosf(direction),
                type_button_radius * sinf(direction) );
        }

        // per-frame processing
        void ArcPlanet::Update(float secondsSinceLastFrame)
        {
            BasePlanet::Update(secondsSinceLastFrame);

            if (CollisionSurface::DEATHZONE == mSurfaceType)
            {
                EmitParticles(secondsSinceLastFrame);
            }
        }

        // spawn particles for flame effects
        void ArcPlanet::EmitParticles(float timeSinceLastFrame)
        {
            float arc = AngleDiff(mStartAngle, mEndAngle);
            float outer_length = mOuterRadius * arc;
            float inner_length = mInnerRadius * arc;
            float end_length = mOuterRadius - mInnerRadius;

            unsigned outer_particles = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * outer_length) + RandFloat());
            unsigned inner_particles = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * inner_length) + RandFloat());
            unsigned start_particles = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * end_length) + RandFloat());

            unsigned end_particles   = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * end_length) + RandFloat());

            for (unsigned i=0; i<outer_particles; ++i)
            {
                float direction = mStartAngle + ( arc * RandFloat());
                SpawnParticle(
                    mXPos + (mOuterRadius * cosf(direction) ),
                    mYPos + (mOuterRadius * sinf(direction) ),
                    direction
                    );
            }
            for (unsigned i=0; i<inner_particles; ++i)
            {
                float direction = mStartAngle + ( arc * RandFloat());
                SpawnParticle(
                    mXPos + (mInnerRadius * cosf(direction) ),
                    mYPos + (mInnerRadius * sinf(direction) ),
                    PI+direction
                    );
            }
            for (unsigned i=0; i<start_particles; ++i)
            {
                float direction = mStartAngle;
                float radius = mInnerRadius + (end_length * RandFloat());
                SpawnParticle(
                    mXPos + (radius * cosf(direction) ),
                    mYPos + (radius * sinf(direction) ),
                    mStartAngle - 0.5f*PI
                    );
            }
            for (unsigned i=0; i<end_particles; ++i)
            {
                float direction = mEndAngle;
                float radius = mInnerRadius + (end_length * RandFloat());
                SpawnParticle(
                    mXPos + (radius * cosf(direction) ),
                    mYPos + (radius * sinf(direction) ),
                    mEndAngle + (0.5f*PI));
            }
        }
    }
}



