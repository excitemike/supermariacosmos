#ifndef SUPERMARIACOSMOS_RECTPLANET_H
#define SUPERMARIACOSMOS_RECTPLANET_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/BasePlanet.h"
#include "SuperMariaCosmos/CollisionArc.h"
#include "SuperMariaCosmos/CollisionLine.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class RectPlanet : public BasePlanet
        {
        public:
            RectPlanet();

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file);

            // set up this object based on what we read in
            virtual void Read(FILE *file);

            // how many surfaces does this level object have?
            virtual unsigned GetNumSurfaces();

            // how many surfaces does this level object have?
            virtual SuperMariaCosmos::CollisionSurface* GetSurface(unsigned index);

            virtual void SetPosition(float x, float y) { LevelObject::SetPosition(x, y); UpdateCollisionSurfaces(); }

            // so that we can display this thing in an icon
            virtual void Iconify();

            // for safely casting to a derived class if you have a LeveObject
            // pointer.  kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(RectPlanet **ptr){*ptr = this; return true;}

            // per-frame processing
            virtual void Update(float secondsSinceLastFrame);
        protected:
            // update based on the new mouse position and which handle got moved
            void DragUpdate(BasePlanet::HandleId handle, float x, float y);

            void DragUpdate_Left(float x, float y);
            void DragUpdate_Right(float x, float y);
            void DragUpdate_Top(float x, float y);
            void DragUpdate_Bottom(float x, float y);

            BasePlanet::HandleId GetHandleUnderMouse(float x, float y);

            // find out whether the mouse is over the planet (does not consider handles)
            bool IsMouseOverPlanet(float x, float y);

            // collision info based on changes to internal variables
            void UpdateCollisionSurfaces();

            // renders differently when selected
            void DrawSelected(Subsystems::RenderManager* pRenderer);

            // draw it
            void DrawPlanet(Subsystems::RenderManager* pRenderer);

            //draw pieces
            void DrawSelectionBackground(Subsystems::RenderManager* pRenderer);

            // record where a drag started so that we can look at how much it
            // has changed since then
            void StoreDragInfo(float mousex, float mousey);

            // tell it where to draw the delete button
            void UpdateButtonPositions();

            // spawn particles for flame effects
            void EmitParticles(float timeSinceLastFrame);
        private:
            enum Handle
            {
                NONE = BasePlanet::NONE,
                MOVE = BasePlanet::MOVE,
                LEFT_EDGE,
                RIGHT_EDGE,
                TOP_EDGE,
                BOTTOM_EDGE,
                UPPER_LEFT_CORNER,
                UPPER_RIGHT_CORNER,
                LOWER_LEFT_CORNER,
                LOWER_RIGHT_CORNER
            };

            struct MouseDragInfo
            {
                float start_x;
                float start_y;
                float start_mouse_x;
                float start_mouse_y;
                float start_width;
                float start_height;
            };

            static const Subsystems::RenderManager::Color INNER_COLOR;
            static const Subsystems::RenderManager::Color OUTER_COLOR;
            static const Subsystems::RenderManager::Color DANGER_INNER_COLOR;
            static const Subsystems::RenderManager::Color DANGER_OUTER_COLOR;
            static const float MIN_SIZE;

            CollisionLine mLeftEdgeCollisionLine;
            CollisionLine mRightEdgeCollisionLine;
            CollisionLine mTopEdgeCollisionLine;
            CollisionLine mBottomEdgeCollisionLine;

            CollisionArc mLowerRightCornerCollisionArc;
            CollisionArc mUpperRightCornerCollisionArc;
            CollisionArc mUpperLeftCornerCollisionArc;
            CollisionArc mLowerLeftCornerCollisionArc;

            static const unsigned NUM_SURFACES = 8;
            CollisionSurface* mSurfacesInOrder[NUM_SURFACES];

            float mWidth;
            float mHeight;

            MouseDragInfo mDragInfo;
        };
    }
}

#endif // SUPERMARIACOSMOS_RECTPLANET_H


