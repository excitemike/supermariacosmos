#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_KEY_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_KEY_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/SpriteLevelObject.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class Key : public SpriteLevelObject
        {
        public:
            Key();

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file);

            // set up this object based on what we read in
            virtual void Read(FILE *file);

            // for safely casting to a derived class if you have a LeveObject 
            // pointer.  Kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived 
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(Key **ptr){*ptr = this; return true;}

            bool IsFound() {return mFound;}
            void SetFound(bool found) {mFound = found;}

            // move toward a point within acceptable_distance_from_target of 
            // the given x, y, but with a limit on how far one call of this 
            // function can take it
            void MoveToward(float x, float y, float acceptable_distance_from_target, float max_distance);
        private:
            static constexpr float IMAGE_WIDTH = 24.f;
            static constexpr float IMAGE_HEIGHT = 24.f;

            bool mFound;
        };
    }
}

#endif // SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_KEY_H


