#ifndef SUPERMARIACOSMOS_GOAL_H
#define SUPERMARIACOSMOS_GOAL_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/SpriteLevelObject.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class Goal : public SpriteLevelObject
        {
        public:
            Goal();

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file);

            // set up this object based on what we read in
            virtual void Read(FILE *file);

            // for safely casting to a derived class if you have a LeveObject 
            // pointer.  Kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived 
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(Goal **ptr){*ptr = this; return true;}

        private:
            static constexpr float IMAGE_WIDTH = 32.f;
            static constexpr float IMAGE_HEIGHT = 32.f;
        };
    }
}

#endif // SUPERMARIACOSMOS_GOAL_H


