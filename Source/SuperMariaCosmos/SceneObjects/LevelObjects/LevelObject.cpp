#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        const float LevelObject::DELETEBUTTON_WIDTH = 24.f;
        const float LevelObject::DELETEBUTTON_HEIGHT = 24.f;

        LevelObject::LevelObject() :
            ::SceneObjects::BaseWidget(),
            mDeleteButtonTexture(NULL),
            mObserver(NULL),
            mDeleteButtonOffsetX(0.f),
            mDeleteButtonOffsetY(0.f)
        {
            mDeleteButtonTexture = Utilities::TextureManager::GetTexture("Content/Textures/deletebutton.png");
        }

        void LevelObject::DrawDeleteButton(Subsystems::RenderManager* pRenderer)
        {
            float x;
            float y;
            GetDeleteButtonPosition(x,y);

            pRenderer->RenderSprite(
                mDeleteButtonTexture,
                x -( 0.5f*DELETEBUTTON_WIDTH),
                y -( 0.5f*DELETEBUTTON_HEIGHT),
                DELETEBUTTON_WIDTH,
                DELETEBUTTON_HEIGHT);
        }

        void LevelObject::SetDeleteButtonOffset(float x, float y)
        {
            mDeleteButtonOffsetX = x;
            mDeleteButtonOffsetY = y;
        }

        bool LevelObject::IsOverDeleteButton(float mousex, float mousey)
        {
            float delete_button_x;
            float delete_button_y;
            GetDeleteButtonPosition(delete_button_x, delete_button_y);

            float distance = Distance(delete_button_x, delete_button_y, mousex, mousey);

            return distance <= (0.5f*DELETEBUTTON_WIDTH);
        }


        void LevelObject::Delete()
        {
            if (NULL != mObserver)
            {
                mObserver->DeleteButtonPressed(this);
            }
        }

        // find the delete button based on the offset
        void LevelObject::GetDeleteButtonPosition(float &outX, float &outY)
        {
            float x = mXPos + mDeleteButtonOffsetX;
            float y = mYPos + mDeleteButtonOffsetY;

            outX = x;
            outY = y;
        }

        void LevelObject::Render(Subsystems::RenderManager* pRenderer)
        {
            ::SceneObjects::BaseWidget::Render(pRenderer);

            if (IsSelected())
            {
                DrawDeleteButton(pRenderer);
            }
        }
    }
}
