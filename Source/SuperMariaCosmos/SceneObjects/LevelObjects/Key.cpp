#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/Key.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"
#include <assert.h>

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        Key::Key() :
            SpriteLevelObject(),
            mFound(false)
        {
            SetTexture( Utilities::TextureManager::GetTexture("Content/Textures/key.png") );
            SetDeleteButtonOffset(18.f,18.f);

            mImageWidth = IMAGE_WIDTH;
            mImageHeight = IMAGE_HEIGHT;
        }

        // write the representation of this object to a file (for later reading)
        void Key::Write(FILE *file)
        {
            fprintf(file, "%d\n", LEVEL_OBJECT_KEY);
            fprintf(file, "  %f\n", mXPos);
            fprintf(file, "  %f\n", mYPos);
        }

        // set up this object based on what we read in
        void Key::Read(FILE *file)
        {
            // the line for indicating which type this is should already have been read
            int scanresult;

            scanresult = fscanf(file, "%f", &mXPos);
            scanresult = fscanf(file, "%f", &mYPos);
        }

        // move toward a point within acceptable_distance_from_target of 
        // the given x, y, but with a limit on how far one call of this 
        // function can take it
        void Key::MoveToward(
            float x, float y, 
            float acceptable_distance_from_target, 
            float max_distance)
        {
            ::MoveToward(mXPos, mYPos, x, y, mXPos, mYPos, acceptable_distance_from_target, max_distance);
        }
    }
}

