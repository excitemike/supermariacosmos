#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/Goal.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"
#include <assert.h>

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        Goal::Goal() :
            SpriteLevelObject()
        {
            SetTexture( Utilities::TextureManager::GetTexture("Content/Textures/door.png") );
            SetDeleteButtonOffset(23.f,23.f);

            mImageWidth = IMAGE_WIDTH;
            mImageHeight = IMAGE_HEIGHT;
        }

        // write the representation of this object to a file (for later reading)
        void Goal::Write(FILE *file)
        {
            fprintf(file, "%d\n", LEVEL_OBJECT_GOAL);
            fprintf(file, "  %f\n", mXPos);
            fprintf(file, "  %f\n", mYPos);
        }

        // set up this object based on what we read in
        void Goal::Read(FILE *file)
        {
            // the line for indicating which type this is should already have been read
            int scanresult;

            scanresult = fscanf(file, "%f", &mXPos);
            scanresult = fscanf(file, "%f", &mYPos);
        }
    }
}

