#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/ArcPlanet.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/CirclePlanet.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/RectPlanet.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/Goal.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/Key.h"
#include "SuperMariaCosmos/SceneObjects/SuperMaria.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // new a planet
        LevelObject* AllocateNewPlanet(LevelObjectType type)
        {
            switch(type)
            {
            case LEVEL_OBJECT_ARC:
                return new ArcPlanet();
                break;
            case LEVEL_OBJECT_GOAL:
                return new Goal();
            case LEVEL_OBJECT_KEY:
                return new Key();
            case LEVEL_OBJECT_MARIA:
                return new SuperMaria();
            case LEVEL_OBJECT_CIRCLE:
                return new CirclePlanet();
            case LEVEL_OBJECT_RECT:
                return new RectPlanet();
            default:
                // you know, I think crashing is the best thing to do here
                int a;
                a = *(int*)0x00000000;
                return NULL;
            }
        }

        // free a planet made with AllocateNewPlanet
        void FreePlanet(const LevelObject* pPlanet)
        {
            delete pPlanet;
        }
    }
}
