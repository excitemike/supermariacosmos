#ifndef SUPERMARIACOSMOS_CIRCLEPLANET_H
#define SUPERMARIACOSMOS_CIRCLEPLANET_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/BasePlanet.h"
#include "SuperMariaCosmos/CollisionCircle.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class CirclePlanet : public BasePlanet
        {
        public:
            CirclePlanet();

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file);

            // set up this object based on what we read in
            virtual void Read(FILE *file);

            // how many surfaces does this level object have?
            virtual unsigned GetNumSurfaces();

            // how many surfaces does this level object have?
            virtual SuperMariaCosmos::CollisionSurface* GetSurface(unsigned index);

            virtual void SetPosition(float x, float y) { LevelObject::SetPosition(x, y); UpdateCollisionSurfaces(); }

            // so that we can display this thing in an icon
            virtual void Iconify();

            // for safely casting to a derived class if you have a LeveObject
            // pointer.  kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(CirclePlanet **ptr){*ptr = this; return true;}

            // per-frame processing
            virtual void Update(float secondsSinceLastFrame);
        private:
            enum Handle
            {
                NONE = BasePlanet::NONE,
                MOVE = BasePlanet::MOVE,
                RADIUS
            };

            struct MouseDragInfo
            {
                float start_xdiff;
                float start_ydiff;
                float start_radius;
            };

            // update based on the new mouse position and which handle got moved
            void DragUpdate(BasePlanet::HandleId handle, float x, float y);

            void DragUpdate_Radius(float mousex, float mousey);

            static const Subsystems::RenderManager::Color INNER_COLOR;
            static const Subsystems::RenderManager::Color OUTER_COLOR;
            static const Subsystems::RenderManager::Color DANGER_INNER_COLOR;
            static const Subsystems::RenderManager::Color DANGER_OUTER_COLOR;
            static const float SELECTION_BORDER_SIZE;
            static const float MIN_RADIUS;

            BasePlanet::HandleId GetHandleUnderMouse(float x, float y);

            // find out whether the mouse is over the planet (does not consider handles)
            bool IsMouseOverPlanet(float x, float y);

            // collision info based on changes to internal variables
            void UpdateCollisionSurfaces();

            // tell it where to draw the delete button
            void UpdateButtonPositions();

            // renders differently when selected
            void DrawSelected(Subsystems::RenderManager* pRenderer);

            // draw it
            void DrawPlanet(Subsystems::RenderManager* pRenderer);

            // record where a drag started so that we can look at how much it
            // has changed since then
            void StoreDragInfo(float mousex, float mousey);

            // spawn particles for flame effects
            void EmitParticles(float timeSinceLastFrame);

            CollisionCircle mCollisionCircle;

            float mRadius;

            Handle mCurrentHandle;
            MouseDragInfo mDragInfo;
        };
    }
}

#endif // SUPERMARIACOSMOS_CIRCLEPLANET_H


