#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/SpriteLevelObject.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"
#include <assert.h>

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        SpriteLevelObject::SpriteLevelObject() :
            LevelObject(),
            mImageWidth(32.f),
            mImageHeight(32.f),
            mRotation(0.f),
            mHFlip(false),
            mVFlip(false),
            mTexture(NULL)
        {
            mDragInfo.is_dragging = false;
        }

        // return true if the coord given is over the planet
        bool SpriteLevelObject::OnMouseDown(float x, float y)
        {
            if (IsSelected() && IsOverDeleteButton(x,y))
            {
                Delete();
                return true;
            }

            bool mouseIsOver = IsMouseOver(x,y);

            if (mouseIsOver)
            {
                if (!IsSelected())
                {
                    SuperMariaCosmos::WidgetManager.Select(this);
                    StoreDragInfo(x, y);
                }

                mDragInfo.is_dragging = true;
            }

            return mouseIsOver;
        }

        void SpriteLevelObject::OnMouseMove(float x, float y)
        {
            if (mDragInfo.is_dragging)
            {
                SetX( x - mDragInfo.start_xdiff );
                SetY( y - mDragInfo.start_ydiff );
            }
        }

        void SpriteLevelObject::OnMouseUp(float x, float y)
        {
            // make sure we don't get stuck dragging
            mDragInfo.is_dragging = false;
        }

        // draw it
        void SpriteLevelObject::Render(Subsystems::RenderManager* pRenderer)
        {
            LevelObject::Render(pRenderer);

            float offset_x = (0.5f*mImageWidth);
            float offset_y = (0.5f*mImageHeight);

            // selection highlight
            if (IsSelected())
            {
                // to render the full circle, we need two arcs
                pRenderer->RenderArc(
                    mXPos, mYPos,
                    0.f, PI,
                    offset_x + HANDLE_SPACING,
                    0.f,
                    EDGE_HANDLE_COLOR, EDGE_HANDLE_COLOR);
                pRenderer->RenderArc(
                    mXPos, mYPos,
                    PI, 0.f,
                    offset_x + HANDLE_SPACING,
                    0.f,
                    EDGE_HANDLE_COLOR, EDGE_HANDLE_COLOR);
            }

            pRenderer->RenderSprite(
                mTexture,
                mXPos-offset_x,
                mYPos-offset_y,
                mImageWidth, mImageHeight,
                mHFlip, mVFlip,
                mRotation,
                offset_x, offset_y);
        }

        // find out whether the mouse is over the planet (does not consider handles)
        bool SpriteLevelObject::IsMouseOver(float x, float y)
        {
            mDragInfo.start_xdiff = x - GetX();
            mDragInfo.start_ydiff = y - GetY();

            float mouse_radius = Distance(0.f, 0.f, mDragInfo.start_xdiff, mDragInfo.start_ydiff);

            return (mouse_radius <= GetRadius() );
        }

        // how many surfaces does this level object have?
        unsigned SpriteLevelObject::GetNumSurfaces()
        {
            return 0;
        }

        // Goals don't have any collision surfaces
        SuperMariaCosmos::CollisionSurface* SpriteLevelObject::GetSurface(unsigned index)
        {
            return NULL;
        }

        void SpriteLevelObject::Deselect()
        {
            LevelObject::Deselect();
            mDragInfo.is_dragging = false;
        }

        // record where a drag started so that we can look at how much it
        // has changed since then
        void SpriteLevelObject::StoreDragInfo(float mousex, float mousey)
        {
            mDragInfo.start_xdiff = mousex - GetX();
            mDragInfo.start_ydiff = mousey - GetY();
        }

        // so that we can display this thing in an icon
        void SpriteLevelObject::Iconify()
        {
            Deselect();
            mImageWidth *= 0.75f;
            mImageHeight *= 0.75f;
        }

        // collision radius for detecting when things hit this
        float SpriteLevelObject::GetRadius()
        {
            return (0.5f * mImageWidth);
        }
    }
}

