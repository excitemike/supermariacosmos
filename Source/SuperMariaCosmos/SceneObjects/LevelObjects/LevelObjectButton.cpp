#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObjectButton.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"

static const float WIDTH = 36.f;
static const float HEIGHT = 36.f;

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        namespace LevelObjects
        {
            LevelObjectButton::LevelObjectButton(LevelObjectType icon) : 
                BaseButton( "Content/Textures/iconbutton_shading_up.png", "Content/Textures/iconbutton_shading_down.png", WIDTH, HEIGHT),
                mBackgroundTexture(NULL),
                mIcon(NULL),
                mType(icon)
            {
                mBackgroundTexture = Utilities::TextureManager::GetTexture("Content/Textures/iconbutton_background.png");
                mIcon = AllocateNewPlanet(icon);
                mIcon->Iconify();
            }


            LevelObjectButton::~LevelObjectButton()
            {
                FreePlanet(mIcon);
            }


            void LevelObjectButton::Render(Subsystems::RenderManager* pRenderer)
            {
                // looks a little nicer with a small offset
                float icon_offset_x = IsDown() ? 1.f : 0.f;
                float icon_offset_y = IsDown() ? -2.f : 0.f;

                if (IsVisible())
                {
                    // background first
                    pRenderer->RenderSprite( 
                        mBackgroundTexture, 
                        mXPos-(0.5f*GetWidth()), 
                        mYPos-(0.5f*GetHeight()),
                        GetWidth(), GetHeight());

                    // then the icon
                    mIcon->SetPosition(mXPos+icon_offset_x, mYPos+icon_offset_y);
                    mIcon->Render(pRenderer);

                    // then the shading
                    BaseButton::Render(pRenderer);
                }
            }
        }
    }
}

