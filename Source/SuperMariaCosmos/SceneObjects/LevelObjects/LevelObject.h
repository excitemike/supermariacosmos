#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECT_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECT_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObjectObserver.h"
#include "SceneObjects/BaseWidget.h"
#include "SuperMariaCosmos/CollisionSurface.h"
#include "Utilities/TextureManager.h"
#include <stdio.h>

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class BasePlanet;
        class ArcPlanet;
        class CirclePlanet;
        class RectPlanet;
        class Goal;
        class Key;
        class SpriteLevelObject;
        class SuperMaria;

        class LevelObject : public ::SceneObjects::BaseWidget
        {
        public:
            LevelObject();
            virtual ~LevelObject(){}

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file) = 0;

            // set up this object based on what we read in
            virtual void Read(FILE *file) = 0;

            // how many surfaces does this level object have?
            virtual unsigned GetNumSurfaces() = 0;

            // how many surfaces does this level object have?
            virtual SuperMariaCosmos::CollisionSurface* GetSurface(unsigned index) = 0;

            // so that we can display this thing in an icon
            virtual void Iconify() = 0;

            // start watching this level object
            virtual void SetObserver(SceneObjects::LevelObjects::LevelObjectObserver *pObserver){mObserver = pObserver;}

            void Render(Subsystems::RenderManager* pRenderer);

            // for safely casting to a derived class if you have a LeveObject 
            // pointer.  kind of a poor man's COM.
            // sets the ptr to NULL and returns false if it cannot do the cast
            virtual bool CastMe(BasePlanet **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(ArcPlanet **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(CirclePlanet **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(RectPlanet **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(Goal **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(Key **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(SpriteLevelObject **ptr) {*ptr = NULL; return false;}
            virtual bool CastMe(SuperMaria **ptr) {*ptr = NULL; return false;}

        protected:
            // all level objects use the same delete handle
            static const float DELETEBUTTON_WIDTH;
            static const float DELETEBUTTON_HEIGHT;
            void DrawDeleteButton(Subsystems::RenderManager* pRenderer);
            void SetDeleteButtonOffset(float x, float y);
            bool IsOverDeleteButton(float mousex, float mousey);
            void Delete();

        private:
            // go from it's offset in relation to the level object to it's 
            // "real" location after we force it to be onscreen
            void GetDeleteButtonPosition(float &outX, float &outY);

            const Utilities::Texture *mDeleteButtonTexture;
            SceneObjects::LevelObjects::LevelObjectObserver *mObserver;

            float mDeleteButtonOffsetX;
            float mDeleteButtonOffsetY;
        };
    }
}



#endif // SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECT_H
