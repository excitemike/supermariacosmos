#include "SuperMariaCosmos/SceneObjects/LevelObjects/BasePlanet.h"
#include "SuperMariaCosmos/Subsystems.h"
#include "SuperMariaCosmos/Shared.h"

static const char * TYPE_BUTTON_IMAGES[] = {
    "Content/Textures/safeplanet.png",
    "Content/Textures/deathzone.png"};

static const float FLAME_SPEED = 4.f;
static const float FLAME_LIFETIME = 0.75f;
static const char * FLAME_IMAGE = "Content/Textures/flame.png";

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // how many flame particles to emit per pixel of perimeter per second
        const float BasePlanet::FLAME_PARTICLE_EMIT_RATE = 0.125f;

        BasePlanet::BasePlanet() :
            LevelObject(),
            mParticleManager(),
            mFlameTexture(NULL),
            mSurfaceType(CollisionSurface::NORMAL),
            mTypeButtonOffsetX(0.f),
            mTypeButtonOffsetY(0.f),
            mCurrentHandle(NONE)
        {
            UpdateCollisionSurfaces();

            for (int i=0; i<CollisionSurface::NUM_TYPES; ++i)
            {
                mTypeButtonTextures[i] = Utilities::TextureManager::GetTexture(TYPE_BUTTON_IMAGES[i]);
            }

            mParticleManager.Init(32);
            mFlameTexture = Utilities::TextureManager::GetTexture(FLAME_IMAGE);
        }

        // clean up
        BasePlanet::~BasePlanet()
        {
            mParticleManager.Shutdown();
        }

        // return true if the coord given is over the planet
        bool BasePlanet::OnMouseDown(float x, float y)
        {
            if (IsSelected())
            {
                return OnMouseDown_Selected(x,y);
            }
            else
            {
                return OnMouseDown_Unselected(x,y);
            }
        }

        bool BasePlanet::OnMouseDown_Unselected(float x, float y)
        {
            if (IsMouseOverPlanet(x,y))
            {
                SuperMariaCosmos::WidgetManager.Select(this);
                StoreDragInfo(x, y);
                mCurrentHandle = MOVE;
                return true;
            }
            else
            {
                return false;
            }
        }

        bool BasePlanet::OnMouseDown_Selected(float x, float y)
        {
            if (IsOverDeleteButton(x,y))
            {
                Delete();
                return true;
            }
            else if (IsOverTypeButton(x,y))
            {
                ChangeType();
                return true;
            }
            else
            {
                HandleId clicked_handle = (HandleId)GetHandleUnderMouse(x, y);

                if (NONE != clicked_handle)
                {
                    mCurrentHandle = clicked_handle;
                    StoreDragInfo(x, y);
                    return true;
                }
                else
                {
                    mCurrentHandle = NONE;
                    return false;
                }
            }
        }

        void BasePlanet::OnMouseUp(float x, float y)
        {
            // make sure we don't get stuck dragging
            mCurrentHandle = NONE;
        }

        void BasePlanet::OnMouseMove(float x, float y)
        {
            DragUpdate(mCurrentHandle, x, y);
        }

        // draw it
        void BasePlanet::Render(Subsystems::RenderManager* pRenderer)
        {
            LevelObject::Render(pRenderer);

            bool isDragging = (NONE != mCurrentHandle);

            mParticleManager.Render(pRenderer);

            // selection highlight -- but we don't draw it while dragging a handle
            if (IsSelected() && (!isDragging))
            {
                DrawSelected(pRenderer);
            }
            else
            {
                DrawPlanet(pRenderer);
            }

            if (IsSelected())
            {
                DrawTypeButton(pRenderer);
            }
        }

        void BasePlanet::Select()
        {
            LevelObject::Select();
        }

        void BasePlanet::Deselect()
        {
            LevelObject::Deselect();
            mCurrentHandle = NONE;
        }

        bool BasePlanet::IsDragging()
        {
            return (NONE != mCurrentHandle);
        }

        bool BasePlanet::IsOverTypeButton(float mousex, float mousey)
        {
            float button_x;
            float button_y;
            GetTypeButtonPosition(button_x, button_y);

            float distance = Distance(button_x, button_y, mousex, mousey);

            return distance <= (0.5f*DELETEBUTTON_WIDTH);
        }

        // find the delete button based on the offset
        void BasePlanet::GetTypeButtonPosition(float &outX, float &outY)
        {
            float x = mXPos + mTypeButtonOffsetX;
            float y = mYPos + mTypeButtonOffsetY;

            outX = x;
            outY = y;
        }

        // cycle through the planet types
        void BasePlanet::ChangeType()
        {
            mSurfaceType = (CollisionSurface::SurfaceType)((mSurfaceType+1) % CollisionSurface::NUM_TYPES);
        }

        void BasePlanet::DrawTypeButton(Subsystems::RenderManager* pRenderer)
        {
            float x;
            float y;
            GetTypeButtonPosition(x,y);

            // we want to draw the type that we will switch to
            unsigned texture_index = (mSurfaceType+1) % CollisionSurface::NUM_TYPES;

            pRenderer->RenderSprite(
                mTypeButtonTextures[texture_index],
                x -( 0.5f*DELETEBUTTON_WIDTH),
                y -( 0.5f*DELETEBUTTON_HEIGHT),
                DELETEBUTTON_WIDTH,
                DELETEBUTTON_HEIGHT);
        }

        void BasePlanet::SetTypeButtonOffset(float x, float y)
        {
            mTypeButtonOffsetX = x;
            mTypeButtonOffsetY = y;
        }

        // spawn particles for flame effects
        void BasePlanet::SpawnParticle(float x, float y, float direction)
        {
            SuperMariaCosmos::ParticleManager::ParticleData particle;
            particle.texture = mFlameTexture;
            particle.x = x;
            particle.y = y;
            particle.velocity_x = FLAME_SPEED * cosf(direction);
            particle.velocity_y = FLAME_SPEED * sinf(direction);
            particle.remaining_lifetime = FLAME_LIFETIME;
            mParticleManager.SpawnParticle(particle);
        }

        //
        void BasePlanet::Update(float secondsSinceLastFrame)
        {
            LevelObject::Update(secondsSinceLastFrame);

            mParticleManager.Update(secondsSinceLastFrame);
        }
    }
}

