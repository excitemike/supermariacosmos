#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_SPRITELEVELOBJECT_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_SPRITELEVELOBJECT_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "Utilities/TextureManager.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class SpriteLevelObject : public LevelObject
        {
        public:
            SpriteLevelObject();

            // return true if the coord given is over the planet
            virtual bool OnMouseDown(float x, float y);

            virtual void OnMouseMove(float x, float y);
            virtual void OnMouseUp(float x, float y);

            // draw it
            virtual void Render(Subsystems::RenderManager*);

            // how many surfaces does this level object have?
            virtual unsigned GetNumSurfaces();

            // how many surfaces does this level object have?
            virtual SuperMariaCosmos::CollisionSurface* GetSurface(unsigned index);
            
            virtual void Deselect();
            
            // so that we can display this thing in an icon
            virtual void Iconify();

            // collision radius for detecting when things hit this
            virtual float GetRadius();

            // rotate the sprite around its center
            void SetRotation(float angle) {mRotation = angle;}

            // for safely casting to a derived class if you have a LeveObject 
            // pointer.  Kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived 
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(SpriteLevelObject **ptr){*ptr = this; return true;}

            virtual void SetHFlip(bool flip) {mHFlip = flip;}
            virtual void SetVFlip(bool flip) {mVFlip = flip;}
        protected:
            void SetTexture(const Utilities::Texture *texture) {mTexture = texture;}

            float mImageWidth;
            float mImageHeight;
            float mRotation;
            bool mHFlip;
            bool mVFlip;

        private:
            const Utilities::Texture *mTexture;

            struct MouseDragInfo
            {
                float start_xdiff;
                float start_ydiff;
                bool is_dragging;
            };

            bool IsMouseOver(float x, float y);

            // record where a drag started so that we can look at how much it 
            // has changed since then
            void StoreDragInfo(float mousex, float mousey);

            MouseDragInfo mDragInfo;
        };
    }
}

#endif // SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_SPRITELEVELOBJECT_H


