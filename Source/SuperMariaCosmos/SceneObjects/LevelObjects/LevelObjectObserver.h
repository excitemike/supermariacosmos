#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECTOBSERVER_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECTOBSERVER_H


namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class LevelObject;

        namespace LevelObjects
        {
            class LevelObjectObserver
            {
            public:
                virtual void DeleteButtonPressed(SuperMariaCosmos::SceneObjects::LevelObject* pObject) = 0;
            };
        }
    }
}


#endif // SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECTOBSERVER_H
