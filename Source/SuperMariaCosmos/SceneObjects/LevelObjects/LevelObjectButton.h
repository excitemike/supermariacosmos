#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECTBUTTON_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_LEVELOBJECTBUTTON_H

#include "SuperMariaCosmos/SceneObjects/BaseButton.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "Utilities/TextureManager.h"


namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        namespace LevelObjects
        {
            class LevelObjectButton : public BaseButton
            {
            public:
                LevelObjectButton(LevelObjectType icon);
                ~LevelObjectButton();

                void Render(Subsystems::RenderManager* pRenderer);

                LevelObjectType GetType() {return mType;}
            private:
                const Utilities::Texture *mBackgroundTexture;
                LevelObject* mIcon;
                LevelObjectType mType;
            };
        }
    }
}

#endif // SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_H
