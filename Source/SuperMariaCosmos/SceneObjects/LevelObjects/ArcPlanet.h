#ifndef SUPERMARIACOSMOS_ARCPLANET_H
#define SUPERMARIACOSMOS_ARCPLANET_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/BasePlanet.h"
#include "SuperMariaCosmos/CollisionArc.h"
#include "SuperMariaCosmos/CollisionLine.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class ArcPlanet : public BasePlanet
        {
        public:
            ArcPlanet();

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file);

            // set up this object based on what we read in
            virtual void Read(FILE *file);

            // how many surfaces does this level object have?
            virtual unsigned GetNumSurfaces();

            // how many surfaces does this level object have?
            virtual SuperMariaCosmos::CollisionSurface* GetSurface(unsigned index);

            virtual void SetPosition(float x, float y) { LevelObject::SetPosition(x, y); UpdateCollisionSurfaces(); }

            // so that we can display this thing in an icon
            virtual void Iconify();

            // for safely casting to a derived class if you have a LeveObject
            // pointer.  kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(ArcPlanet **ptr){*ptr = this; return true;}

            // per-frame processing
            virtual void Update(float secondsSinceLastFrame);
        private:
            enum Handle
            {
                NONE = BasePlanet::NONE,
                MOVE = BasePlanet::MOVE,
                START_ANGLE,
                END_ANGLE,
                INNER_RADIUS,
                OUTER_RADIUS,
                INNER_START_CORNER,
                OUTER_START_CORNER,
                INNER_END_CORNER,
                OUTER_END_CORNER
            };

            struct MouseDragInfo
            {
                float start_xdiff;
                float start_ydiff;
                float start_anglediff;
                float start_endanglediff;
                float start_inner_radius;
                float start_outer_radius;
            };

            // update based on the new mouse position and which handle got moved
            void DragUpdate(BasePlanet::HandleId handle, float x, float y);
            void DragUpdate_InnerRadius(float mousex, float mousey);
            void DragUpdate_OuterRadius(float mousex, float mousey);
            void DragUpdate_StartAngle(float mousex, float mousey);
            void DragUpdate_EndAngle(float mousex, float mousey);

            static const Subsystems::RenderManager::Color INNER_COLOR;
            static const Subsystems::RenderManager::Color MIDDLE_COLOR;
            static const Subsystems::RenderManager::Color OUTER_COLOR;
            static const Subsystems::RenderManager::Color DANGER_INNER_COLOR;
            static const Subsystems::RenderManager::Color DANGER_MIDDLE_COLOR;
            static const Subsystems::RenderManager::Color DANGER_OUTER_COLOR;
            static const float SELECTION_BORDER_SIZE;
            static const float MIN_ARC_THICKNESS;
            static const float MIN_INNER_RADIUS;

            BasePlanet::HandleId GetHandleUnderMouse(float x, float y);

            // find out whether the mouse is over the planet (does not consider handles)
            bool IsMouseOverPlanet(float x, float y);

            // collision info based on changes to internal variables
            void UpdateCollisionSurfaces();

            // renders differently when selected
            void DrawSelected(Subsystems::RenderManager* pRenderer);

            // draw it
            void DrawPlanet(Subsystems::RenderManager* pRenderer);

            //draw pieces
            void DrawSelectionBackground(Subsystems::RenderManager* pRenderer);
            void DrawEndHandles(Subsystems::RenderManager* pRenderer);

            // render a quad rotating to make the x axis normal to the arc at that
            // angle, and the y axis to be tangent
            void RenderAtEnd(
                Subsystems::RenderManager* pRenderer,
                float angle,
                float x, float y,
                float width, float height,
                Subsystems::RenderManager::Color color);

            // record where a drag started so that we can look at how much it
            // has changed since then
            void StoreDragInfo(float mousex, float mousey);

            // tell it where to draw the delete button
            void UpdateButtonPositions();

            // spawn particles for flame effects
            virtual void EmitParticles(float timeSinceLastFrame);

            CollisionArc mOuterCollisionArc;
            CollisionArc mInnerCollisionArc;
            CollisionLine mStartCollisionLine;
            CollisionLine mEndCollisionLine;

            CollisionArc mInnerStartCornerCollisionArc;
            CollisionArc mOuterStartCornerCollisionArc;
            CollisionArc mOuterEndCornerCollisionArc;
            CollisionArc mInnerEndCornerCollisionArc;

            static const unsigned NUM_SURFACES = 8;
            CollisionSurface* mSurfacesInOrder[NUM_SURFACES];

            float mStartAngle;
            float mEndAngle;
            float mInnerRadius;
            float mOuterRadius;

            MouseDragInfo mDragInfo;
        };
    }
}

#endif // SUPERMARIACOSMOS_ARCPLANET_H


