#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/CirclePlanet.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"
#include <assert.h>


namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        const Subsystems::RenderManager::Color CirclePlanet::INNER_COLOR = {0.f,0.f,0.f,1.f};
        const Subsystems::RenderManager::Color CirclePlanet::OUTER_COLOR = {0.f,0.8f,0.f,1.f};
        const Subsystems::RenderManager::Color CirclePlanet::DANGER_INNER_COLOR = {1.f,0.f,0.f,1.f};
        const Subsystems::RenderManager::Color CirclePlanet::DANGER_OUTER_COLOR = {1.f,1.f,0.5f,1.f};
        const float CirclePlanet::SELECTION_BORDER_SIZE = 2.f;
        const float CirclePlanet::MIN_RADIUS = (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);

        CirclePlanet::CirclePlanet() :
            BasePlanet(),
            mRadius(36.f)
        {
            UpdateButtonPositions();
        }

        // update based on the new mouse position and which handle got moved
        void CirclePlanet::DragUpdate(BasePlanet::HandleId handle, float x, float y)
        {
            // hey I guess we are dragging something
            switch (handle)
            {
            case NONE:
                // not actually dragging anything
                break;
            case MOVE:
                SetX( x - mDragInfo.start_xdiff );
                SetY( y - mDragInfo.start_ydiff );
                break;
            case RADIUS:
                DragUpdate_Radius(x, y);
                break;
            }
        }

        void CirclePlanet::DragUpdate_Radius(float mousex, float mousey)
        {
            float mouse_radius = Distance(GetX(), GetY(), mousex, mousey);

            mRadius = mouse_radius - mDragInfo.start_radius;

            if (mRadius < MIN_RADIUS)
            {
                mRadius = MIN_RADIUS;
            }

            UpdateButtonPositions();
        }


        // tell it where to draw the delete button
        void CirclePlanet::UpdateButtonPositions()
        {
            float delete_button_radius = mRadius + 12.f + (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);
            float delete_button_direction = 0.25f * PI;
            float delete_button_x = delete_button_radius * sinf(delete_button_direction);
            float delete_button_y = delete_button_radius * cosf(delete_button_direction);

            SetDeleteButtonOffset(delete_button_x, delete_button_y );
            SetTypeButtonOffset(-delete_button_x, -delete_button_y);
        }

        // renders differently when selected
        void CirclePlanet::DrawSelected(Subsystems::RenderManager* pRenderer)
        {
            // background
            pRenderer->RenderCircle(
                GetX(),
                GetY(),
                mRadius + HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING,
                HANDLE_BG_COLOR,
                HANDLE_BG_COLOR);

            // radius handle
            pRenderer->RenderCircle(
                GetX(),
                GetY(),
                mRadius + HANDLE_SPACING + HANDLE_THICKNESS,
                EDGE_HANDLE_COLOR,
                EDGE_HANDLE_COLOR);

            // border to separate handle from planet
            pRenderer->RenderCircle(
                GetX(),
                GetY(),
                mRadius + HANDLE_SPACING,
                HANDLE_BG_COLOR,
                HANDLE_BG_COLOR);

            // planet still visible on top of all that
            DrawPlanet(pRenderer);
        }

        // draw it
        void CirclePlanet::DrawPlanet(Subsystems::RenderManager* pRenderer)
        {
            const Subsystems::RenderManager::Color *pInnerColor = (CollisionSurface::DEATHZONE == mSurfaceType)  ? &DANGER_INNER_COLOR  : &INNER_COLOR;
            const Subsystems::RenderManager::Color *pOuterColor = (CollisionSurface::DEATHZONE == mSurfaceType)  ? &DANGER_OUTER_COLOR  : &OUTER_COLOR;

            pRenderer->RenderCircle(
                GetX(),
                GetY(),
                mRadius,
                *pOuterColor,
                *pInnerColor);
        }


        BasePlanet::HandleId CirclePlanet::GetHandleUnderMouse(float x, float y)
        {
            float xdiff = x - GetX();
            float ydiff = y - GetY();
            float distance_to_mouse = Distance(0.f, 0.f, xdiff, ydiff);
            float handle_and_borders_thickness = HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING;
            float outer_radius  = mRadius + handle_and_borders_thickness;

            // middle
            if (distance_to_mouse <= mRadius) return MOVE;

            // radius adjustor
            if (distance_to_mouse <= outer_radius) return RADIUS;

            // didn't find anything at all under the mouse
            return NONE;
        }

        // find out whether the mouse is over the planet (does not consider handles)
        bool CirclePlanet::IsMouseOverPlanet(float x, float y)
        {
            return (MOVE == GetHandleUnderMouse(x,y));
        }

        // write the representation of this object to a file (for later reading)
        void CirclePlanet::Write(FILE *file)
        {
            fprintf(file, "%d\n", LEVEL_OBJECT_CIRCLE);
            fprintf(file, "  %f\n", mXPos);
            fprintf(file, "  %f\n", mYPos);
            fprintf(file, "  %f\n", mRadius);
            fprintf(file, "  %d\n", mSurfaceType);
        }

        // set up this object based on what we read in
        void CirclePlanet::Read(FILE *file)
        {
            // the line for indicating which type this is should already have been read
            int scanresult;

            scanresult = fscanf(file, "%f", &mXPos);
            scanresult = fscanf(file, "%f", &mYPos);
            scanresult = fscanf(file, "%f", &mRadius);
            scanresult = fscanf(file, "%d", &mSurfaceType);

            UpdateCollisionSurfaces();
            UpdateButtonPositions();
        }

        // update handles and collision info based on internal
        // variables
        void CirclePlanet::UpdateCollisionSurfaces()
        {
            mCollisionCircle.CenterX = GetX();
            mCollisionCircle.CenterY = GetY();
            mCollisionCircle.Radius = mRadius;
            mCollisionCircle.IsOuter = true;
            mCollisionCircle.SetSurfaceType(mSurfaceType);
    }

        // how many surfaces does this level object have?
        unsigned CirclePlanet::GetNumSurfaces()
        {
            return 1;
        }

        // how many surfaces does this level object have?
        SuperMariaCosmos::CollisionSurface* CirclePlanet::GetSurface(unsigned index)
        {
            switch (index)
            {
            case 0:
                return &mCollisionCircle;
                break;
            default:
                assert(false);
                return NULL;
                break;
            }
        }

        // record where a drag started so that we can look at how much it
        // has changed since then
        void CirclePlanet::StoreDragInfo(float mousex, float mousey)
        {
            mDragInfo.start_xdiff = mousex - GetX();
            mDragInfo.start_ydiff = mousey - GetY();

            float mouse_radius = Distance(0.f, 0.f, mDragInfo.start_xdiff, mDragInfo.start_ydiff);
            mDragInfo.start_radius = mouse_radius - mRadius;
        }

        // so that we can display this thing in an icon
        void CirclePlanet::Iconify()
        {
            Deselect();
            mRadius = 12.f;
        }

        // per-frame processing
        void CirclePlanet::Update(float secondsSinceLastFrame)
        {
            BasePlanet::Update(secondsSinceLastFrame);

            if (CollisionSurface::DEATHZONE == mSurfaceType)
            {
                EmitParticles(secondsSinceLastFrame);
            }
        }

        // spawn particles for flame effects
        void CirclePlanet::EmitParticles(float timeSinceLastFrame)
        {
            float circumference = mRadius * 2.f * PI;

            unsigned num_particles = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * circumference) + RandFloat());

            for (unsigned i=0; i<num_particles; ++i)
            {
                float direction = 2.f * PI * RandFloat();
                SpawnParticle(
                    mXPos + (mRadius * cosf(direction) ),
                    mYPos + (mRadius * sinf(direction) ),
                    direction
                    );
            }
        }
    }
}

