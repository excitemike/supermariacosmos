#ifndef SUPERMARIACOSMOS_BASEPLANET_H
#define SUPERMARIACOSMOS_BASEPLANET_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "SuperMariaCosmos/ParticleManager.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        class BasePlanet : public LevelObject
        {
        public:
            BasePlanet();
            virtual ~BasePlanet();

            virtual void Update(float secondsSinceLastFrame);

            // return true if the coord given is over the planet
            virtual bool OnMouseDown(float x, float y);

            virtual void OnMouseUp(float x, float y);
            virtual void OnMouseMove(float x, float y);

            // draw it
            virtual void Render(Subsystems::RenderManager*);

            // write the representation of this object to a file (for later reading)
            virtual void Write(FILE *file) = 0;

            // set up this object based on what we read in
            virtual void Read(FILE *file) = 0;

            // how many surfaces does this level object have?
            virtual unsigned GetNumSurfaces() = 0;

            // how many surfaces does this level object have?
            virtual SuperMariaCosmos::CollisionSurface* GetSurface(unsigned index) = 0;

            virtual void SetPosition(float x, float y) { LevelObject::SetPosition(x, y); UpdateCollisionSurfaces(); }

            virtual void Select();
            virtual void Deselect();

            // so that we can display this thing in an icon
            virtual void Iconify() = 0;

            // for safely casting to a derived class if you have a LeveObject
            // pointer.  kind of a poor man's COM.
            // base class sets the ptr to NULL and returns false.  derived
            // classes override their version so that everything is hunky dory
            virtual bool CastMe(BasePlanet **ptr){*ptr = this; return true;}

            // uh, for some reason the compiler says child classes can't use this if it's protected...
            typedef unsigned HandleId;
        protected:
            // how many flame particles to emit per pixel of perimeter per second
            static const float FLAME_PARTICLE_EMIT_RATE;

            enum Handle
            {
                NONE,
                MOVE
            };

            virtual HandleId GetHandleUnderMouse(float x, float y) = 0;

            // update based on the new mouse position and which handle got moved
            virtual void DragUpdate(HandleId handle, float x, float y) = 0;

            virtual bool OnMouseDown_Unselected(float x, float y);
            virtual bool OnMouseDown_Selected(float x, float y);

            // renders differently when selected
            virtual void DrawSelected(Subsystems::RenderManager* pRenderer) = 0;

            // draw it unselected
            virtual void DrawPlanet(Subsystems::RenderManager* pRenderer) = 0;

            virtual bool IsDragging();

            // record where a drag started so that we can look at how much it
            // has changed since then
            virtual void StoreDragInfo(float mousex, float mousey) = 0;

            // PLEASE OVERRIDE
            // collision info based on changes to internal variables
            virtual void UpdateCollisionSurfaces() {}

            // find out whether the mouse is over the planet (does not consider handles)
            virtual bool IsMouseOverPlanet(float x, float y) = 0;

            // spawn particles for flame effects
            virtual void SpawnParticle(float x, float y, float direction);

            // find out whether the mouse is over the button for changing planet type
            virtual bool IsOverTypeButton(float mousex, float mousey);

            // cycle through the planet types
            virtual void ChangeType();

            // find the delete button based on the offset
            virtual void GetTypeButtonPosition(float &outX, float &outY);
            virtual void SetTypeButtonOffset(float x, float y);

            virtual void DrawTypeButton(Subsystems::RenderManager* pRenderer);

            ParticleManager mParticleManager;
            const Utilities::Texture *mFlameTexture;

            const Utilities::Texture *mTypeButtonTextures[CollisionSurface::NUM_TYPES];
            CollisionSurface::SurfaceType mSurfaceType;
            float mTypeButtonOffsetX;
            float mTypeButtonOffsetY;

            HandleId mCurrentHandle;
        };
    }
}

#endif // SUPERMARIACOSMOS_BASEPLANET_H


