#ifndef SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_PLANETTYPES_H
#define SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_PLANETTYPES_H

#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "Subsystems/RenderManager.h"

namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        // what types of planets you can make
        enum LevelObjectType
        {
            LEVEL_OBJECT_ARC,
            LEVEL_OBJECT_CIRCLE,
            LEVEL_OBJECT_RECT,
            LEVEL_OBJECT_GOAL,
            LEVEL_OBJECT_KEY,
            LEVEL_OBJECT_MARIA
        };
        
        // new a planet
        LevelObject* AllocateNewPlanet(LevelObjectType type);

        // free a planet made with AllocateNewPlanet
        void FreePlanet(const LevelObject* pPlanet);
    }
}

#endif // SUPERMARIACOSMOS_SCENEOBJECTS_LEVELOBJECTS_PLANETTYPES_H

