#include "SuperMariaCosmos/SceneObjects/LevelObjects/RectPlanet.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"


namespace SuperMariaCosmos
{
    namespace SceneObjects
    {
        const Subsystems::RenderManager::Color RectPlanet::INNER_COLOR = {0.f,0.f,0.f,1.f};
        const Subsystems::RenderManager::Color RectPlanet::OUTER_COLOR = {0.f,0.8f,0.f,1.f};
        const Subsystems::RenderManager::Color RectPlanet::DANGER_INNER_COLOR = {1.f,0.f,0.f,1.f};
        const Subsystems::RenderManager::Color RectPlanet::DANGER_OUTER_COLOR = {1.f,1.f,0.5f,1.f};
        const float RectPlanet::MIN_SIZE = (HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING);

        RectPlanet::RectPlanet() :
            BasePlanet(),
            mWidth(32.f),
            mHeight(16.f)
        {
            // set up collision surface handoffs
            unsigned index=0;
            mSurfacesInOrder[index++] = &mLowerRightCornerCollisionArc;
            mSurfacesInOrder[index++] = &mRightEdgeCollisionLine;
            mSurfacesInOrder[index++] = &mUpperRightCornerCollisionArc;
            mSurfacesInOrder[index++] = &mTopEdgeCollisionLine;
            mSurfacesInOrder[index++] = &mUpperLeftCornerCollisionArc;
            mSurfacesInOrder[index++] = &mLeftEdgeCollisionLine;
            mSurfacesInOrder[index++] = &mLowerLeftCornerCollisionArc;
            mSurfacesInOrder[index++] = &mBottomEdgeCollisionLine;

            for (unsigned i=0; i<NUM_SURFACES; ++i)
            {
                unsigned prev = (i) % NUM_SURFACES;
                unsigned cur  = (i+1) % NUM_SURFACES;
                unsigned next = (i+2) % NUM_SURFACES;

                mSurfacesInOrder[cur]->SetPrevSurface( mSurfacesInOrder[prev] );
                mSurfacesInOrder[cur]->SetNextSurface( mSurfacesInOrder[next] );
            }
        }

        void RectPlanet::DragUpdate(BasePlanet::HandleId handle, float x, float y)
        {
            switch (mCurrentHandle)
            {
            case NONE:
                // not actually dragging anything
                break;
            case MOVE:
                {
                    float delta_x = x - mDragInfo.start_mouse_x;
                    float delta_y = y - mDragInfo.start_mouse_y;
                    SetX( mDragInfo.start_x + delta_x );
                    SetY( mDragInfo.start_y + delta_y );
                }
                break;
            case LEFT_EDGE:
                DragUpdate_Left(x,y);
                break;
            case RIGHT_EDGE:
                DragUpdate_Right(x,y);
                break;
            case TOP_EDGE:
                DragUpdate_Top(x,y);
                break;
            case BOTTOM_EDGE:
                DragUpdate_Bottom(x,y);
                break;
            case UPPER_LEFT_CORNER:
                DragUpdate_Left(x,y);
                DragUpdate_Top(x,y);
                break;
            case UPPER_RIGHT_CORNER:
                DragUpdate_Right(x,y);
                DragUpdate_Top(x,y);
                break;
            case LOWER_LEFT_CORNER:
                DragUpdate_Left(x,y);
                DragUpdate_Bottom(x,y);
                break;
            case LOWER_RIGHT_CORNER:
                DragUpdate_Right(x,y);
                DragUpdate_Bottom(x,y);
                break;
            }
            UpdateButtonPositions();
        }

        void RectPlanet::DragUpdate_Left(float x, float y)
        {
            float delta_x = x - mDragInfo.start_mouse_x;
            float max_delta = mDragInfo.start_width - MIN_SIZE;

            if (delta_x > max_delta) {delta_x = max_delta;}

            SetX( mDragInfo.start_x + (0.5f * delta_x) );
            mWidth = mDragInfo.start_width - delta_x;
        }

        void RectPlanet::DragUpdate_Right(float x, float y)
        {
            float delta_x = x - mDragInfo.start_mouse_x;
            float min_delta = MIN_SIZE - mDragInfo.start_width;

            if (delta_x < min_delta) {delta_x = min_delta;}

            SetX( mDragInfo.start_x + (0.5f * delta_x) );
            mWidth = mDragInfo.start_width + delta_x;
        }

        void RectPlanet::DragUpdate_Top(float x, float y)
        {
            float delta_y = y - mDragInfo.start_mouse_y;
            float min_delta = MIN_SIZE - mDragInfo.start_height;

            if (delta_y < min_delta) {delta_y = min_delta;}

            SetY( mDragInfo.start_y + (0.5f * delta_y) );
            mHeight = mDragInfo.start_height + delta_y;
        }

        void RectPlanet::DragUpdate_Bottom(float x, float y)
        {
            float delta_y = y - mDragInfo.start_mouse_y;
            float may_delta = mDragInfo.start_height - MIN_SIZE;

            if (delta_y > may_delta) {delta_y = may_delta;}

            SetY( mDragInfo.start_y + (0.5f * delta_y) );
            mHeight = mDragInfo.start_height - delta_y;
        }

        bool RectPlanet::IsMouseOverPlanet(float x, float y)
        {
            float half_w = mWidth * 0.5f;
            float half_h = mHeight * 0.5f;
            float left  = GetX() - half_w;
            float right = GetX() + half_w;
            float bottom = GetY() - half_h;
            float top    = GetY() + half_h;

            return (left <= x)&&(x <= right)&&(bottom <= y)&&(y <= top);
        }

        void RectPlanet::UpdateCollisionSurfaces()
        {
            float half_w = mWidth * 0.5f;
            float half_h = mHeight * 0.5f;
            float left  = GetX() - half_w;
            float right = GetX() + half_w;
            float bottom = GetY() - half_h;
            float top    = GetY() + half_h;

            mLowerRightCornerCollisionArc.CenterX = right;
            mLowerRightCornerCollisionArc.CenterY = bottom;
            mLowerRightCornerCollisionArc.StartAngle = 1.5f * PI;
            mLowerRightCornerCollisionArc.EndAngle = 0.f * PI;
            mLowerRightCornerCollisionArc.Radius = 0.f;
            mLowerRightCornerCollisionArc.IsOuter = true;
            mLowerRightCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mRightEdgeCollisionLine.StartX = right;
            mRightEdgeCollisionLine.StartY = bottom;
            mRightEdgeCollisionLine.EndX   = right;
            mRightEdgeCollisionLine.EndY   = top;
            mRightEdgeCollisionLine.SetSurfaceType(mSurfaceType);

            mUpperRightCornerCollisionArc.CenterX = right;
            mUpperRightCornerCollisionArc.CenterY = top;
            mUpperRightCornerCollisionArc.StartAngle = 0.f;
            mUpperRightCornerCollisionArc.EndAngle = 0.5f * PI;
            mUpperRightCornerCollisionArc.Radius = 0.f;
            mUpperRightCornerCollisionArc.IsOuter = true;
            mUpperRightCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mTopEdgeCollisionLine.StartX = right;
            mTopEdgeCollisionLine.StartY = top;
            mTopEdgeCollisionLine.EndX   = left;
            mTopEdgeCollisionLine.EndY   = top;
            mTopEdgeCollisionLine.SetSurfaceType(mSurfaceType);

            mUpperLeftCornerCollisionArc.CenterX = left;
            mUpperLeftCornerCollisionArc.CenterY = top;
            mUpperLeftCornerCollisionArc.StartAngle = 0.5f * PI;
            mUpperLeftCornerCollisionArc.EndAngle = PI;
            mUpperLeftCornerCollisionArc.Radius = 0.f;
            mUpperLeftCornerCollisionArc.IsOuter = true;
            mUpperLeftCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mLeftEdgeCollisionLine.StartX = left;
            mLeftEdgeCollisionLine.StartY = top;
            mLeftEdgeCollisionLine.EndX   = left;
            mLeftEdgeCollisionLine.EndY   = bottom;
            mLeftEdgeCollisionLine.SetSurfaceType(mSurfaceType);

            mLowerLeftCornerCollisionArc.CenterX = left;
            mLowerLeftCornerCollisionArc.CenterY = bottom;
            mLowerLeftCornerCollisionArc.StartAngle = PI;
            mLowerLeftCornerCollisionArc.EndAngle = 1.5f * PI;
            mLowerLeftCornerCollisionArc.Radius = 0.f;
            mLowerLeftCornerCollisionArc.IsOuter = true;
            mLowerLeftCornerCollisionArc.SetSurfaceType(mSurfaceType);

            mBottomEdgeCollisionLine.StartX = left;
            mBottomEdgeCollisionLine.StartY = bottom;
            mBottomEdgeCollisionLine.EndX   = right;
            mBottomEdgeCollisionLine.EndY   = bottom;
            mBottomEdgeCollisionLine.SetSurfaceType(mSurfaceType);
        }

        // record where a drag started so that we can look at how much it
        // has changed since then
        void RectPlanet::StoreDragInfo(float mousex, float mousey)
        {
            mDragInfo.start_x       = GetX();
            mDragInfo.start_y       = GetY();
            mDragInfo.start_mouse_x = mousex;
            mDragInfo.start_mouse_y = mousey;
            mDragInfo.start_width   = mWidth;
            mDragInfo.start_height  = mHeight;
        }

        // draw it
        void RectPlanet::DrawPlanet(Subsystems::RenderManager* pRenderer)
        {
            float half_w = mWidth * 0.5f;
            float half_h = mHeight * 0.5f;
            float left  = GetX() - half_w;
            float right = GetX() + half_w;
            float bottom = GetY() - half_h;
            float top    = GetY() + half_h;

            const Subsystems::RenderManager::Color *pInnerColor = (CollisionSurface::DEATHZONE == mSurfaceType)  ? &DANGER_INNER_COLOR  : &INNER_COLOR;
            const Subsystems::RenderManager::Color *pOuterColor = (CollisionSurface::DEATHZONE == mSurfaceType)  ? &DANGER_OUTER_COLOR  : &OUTER_COLOR;


            pRenderer->RenderRect(left, right, bottom, top, *pOuterColor, *pInnerColor);
        }

        // renders differently when selected
        void RectPlanet::DrawSelected(Subsystems::RenderManager* pRenderer)
        {
            float half_w = mWidth * 0.5f;
            float half_h = mHeight * 0.5f;
            float left  = GetX() - half_w;
            float right = GetX() + half_w;
            float bottom = GetY() - half_h;
            float top    = GetY() + half_h;

            float offset_to_inner_handle_edge   = HANDLE_SPACING;
            float offset_to_outside_handle_edge = offset_to_inner_handle_edge + HANDLE_THICKNESS;
            float total_handle_border_width     = offset_to_outside_handle_edge + HANDLE_SPACING;

            // background
            pRenderer->RenderQuad(
                left   - total_handle_border_width,
                right  + total_handle_border_width,
                bottom - total_handle_border_width,
                top    + total_handle_border_width,
                HANDLE_BG_COLOR);

            // upperleft corner
            pRenderer->RenderQuad(
                left - offset_to_outside_handle_edge,
                left - offset_to_inner_handle_edge,
                top + offset_to_inner_handle_edge,
                top + offset_to_outside_handle_edge,
                CORNER_HANDLE_COLOR);

            // upperright corner
            pRenderer->RenderQuad(
                right + offset_to_inner_handle_edge,
                right + offset_to_outside_handle_edge,
                top + offset_to_inner_handle_edge,
                top + offset_to_outside_handle_edge,
                CORNER_HANDLE_COLOR);

            // lowerleft corner
            pRenderer->RenderQuad(
                left - offset_to_outside_handle_edge,
                left - offset_to_inner_handle_edge,
                bottom - offset_to_outside_handle_edge,
                bottom - offset_to_inner_handle_edge,
                CORNER_HANDLE_COLOR);

            // lowerright corner
            pRenderer->RenderQuad(
                right + offset_to_inner_handle_edge,
                right + offset_to_outside_handle_edge,
                bottom - offset_to_outside_handle_edge,
                bottom - offset_to_inner_handle_edge,
                CORNER_HANDLE_COLOR);

            // top edge
            pRenderer->RenderQuad(
                left,
                right,
                top + offset_to_inner_handle_edge,
                top + offset_to_outside_handle_edge,
                EDGE_HANDLE_COLOR);

            // bottom edge
            pRenderer->RenderQuad(
                left,
                right,
                bottom - offset_to_outside_handle_edge,
                bottom - offset_to_inner_handle_edge,
                EDGE_HANDLE_COLOR);

            // left edge
            pRenderer->RenderQuad(
                left - offset_to_outside_handle_edge,
                left - offset_to_inner_handle_edge,
                bottom,
                top,
                EDGE_HANDLE_COLOR);

            // right edge
            pRenderer->RenderQuad(
                right + offset_to_inner_handle_edge,
                right + offset_to_outside_handle_edge,
                bottom,
                top,
                EDGE_HANDLE_COLOR);

            // main thing
            DrawPlanet(pRenderer);
        }

        BasePlanet::HandleId RectPlanet::GetHandleUnderMouse(float x, float y)
        {
            float half_w = mWidth * 0.5f;
            float half_h = mHeight * 0.5f;
            float additional_border_from_handles = HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING;

            float planet_left  = GetX() - half_w;
            float planet_right = GetX() + half_w;
            float planet_bottom = GetY() - half_h;
            float planet_top    = GetY() + half_h;

            float total_left   = planet_left   - additional_border_from_handles;
            float total_right  = planet_right  + additional_border_from_handles;
            float total_bottom = planet_bottom - additional_border_from_handles;
            float total_top    = planet_top    + additional_border_from_handles;

            // we actually have a set of clickable boxes stacked 3x3
            // if we figure out col/row numbers the rest is cake
            int col;
            int row;

            if (total_left > x) {col = -1;}
            else if (planet_left > x) {col = 0;}
            else if (planet_right > x) {col = 1;}
            else if (total_right > x) {col = 2;}
            else {col = 3;}

            if (total_bottom > y) {row = -1;}
            else if (planet_bottom > y) {row = 0;}
            else if (planet_top > y) {row = 1;}
            else if (total_top > y) {row = 2;}
            else {row = 3;}

            if      ((row==0) && (col==0)) {return LOWER_LEFT_CORNER;}
            else if ((row==0) && (col==1)) {return BOTTOM_EDGE;}
            else if ((row==0) && (col==2)) {return LOWER_RIGHT_CORNER;}
            else if ((row==1) && (col==0)) {return LEFT_EDGE;}
            else if ((row==1) && (col==1)) {return MOVE;}
            else if ((row==1) && (col==2)) {return RIGHT_EDGE;}
            else if ((row==2) && (col==0)) {return UPPER_LEFT_CORNER;}
            else if ((row==2) && (col==1)) {return TOP_EDGE;}
            else if ((row==2) && (col==2)) {return UPPER_RIGHT_CORNER;}
            else {return NONE;}
        }


        // so that we can display this thing in an icon
        void RectPlanet::Iconify()
        {
            Deselect();
            mWidth = 20.f;
            mHeight = 20.f;
        }

        // how many surfaces does this level object have?
        SuperMariaCosmos::CollisionSurface* RectPlanet::GetSurface(unsigned index)
        {
            if (index < NUM_SURFACES)
            {
                return mSurfacesInOrder[index];
            }
            else
            {
                assert(false);
                return NULL;
            }
        }

        // how many surfaces does this level object have?
        unsigned RectPlanet::GetNumSurfaces()
        {
            return NUM_SURFACES;
        }

        // write the representation of this object to a file (for later reading)
        void RectPlanet::Write(FILE *file)
        {
            fprintf(file, "%d\n", LEVEL_OBJECT_RECT);
            fprintf(file, "  %f\n", mXPos);
            fprintf(file, "  %f\n", mYPos);
            fprintf(file, "  %f\n", mWidth);
            fprintf(file, "  %f\n", mHeight);
            fprintf(file, "  %d\n", mSurfaceType);
        }

        // set up this object based on what we read in
        void RectPlanet::Read(FILE *file)
        {
            // the line for indicating which type this is should already have been read
            int scanresult;

            scanresult = fscanf(file, "%f", &mXPos);
            scanresult = fscanf(file, "%f", &mYPos);
            scanresult = fscanf(file, "%f", &mWidth);
            scanresult = fscanf(file, "%f", &mHeight);
            scanresult = fscanf(file, "%d", &mSurfaceType);

            UpdateCollisionSurfaces();
            UpdateButtonPositions();
        }

        // tell it where to draw the delete button
        void RectPlanet::UpdateButtonPositions()
        {
            float half_w = mWidth * 0.5f;
            float half_h = mHeight * 0.5f;
            float additional_border_from_handles = HANDLE_SPACING + HANDLE_THICKNESS + HANDLE_SPACING;
            float additional_offset_to_look_nice = 8.f;
            float delete_x = half_w + additional_border_from_handles + additional_offset_to_look_nice;
            float delete_y = half_h + additional_border_from_handles + additional_offset_to_look_nice;

            SetDeleteButtonOffset( delete_x, delete_y );
            SetTypeButtonOffset(-delete_x, -delete_y);
        }
        // per-frame processing
        void RectPlanet::Update(float secondsSinceLastFrame)
        {
            BasePlanet::Update(secondsSinceLastFrame);

            if (CollisionSurface::DEATHZONE == mSurfaceType)
            {
                EmitParticles(secondsSinceLastFrame);
            }
        }

        // spawn particles for flame effects
        void RectPlanet::EmitParticles(float timeSinceLastFrame)
        {
            unsigned bottom_particles = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * mWidth) + RandFloat());
            unsigned top_particles    = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * mWidth) + RandFloat());
            unsigned left_particles   = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * mHeight) + RandFloat());
            unsigned right_particles  = unsigned( (FLAME_PARTICLE_EMIT_RATE * timeSinceLastFrame * mHeight) + RandFloat());

            float bottom = mYPos - 0.5f * mHeight;
            float top    = bottom + mHeight;
            float left   = mXPos - 0.5f * mWidth;
            float right  = left + mWidth;

            for (unsigned i=0; i<bottom_particles; ++i)
            {
                SpawnParticle(
                    left + (mWidth * RandFloat()),
                    bottom,
                    -0.5f * PI
                    );
            }
            for (unsigned i=0; i<top_particles; ++i)
            {
                SpawnParticle(
                    left + (mWidth * RandFloat()),
                    top,
                    0.5f * PI
                    );
            }
            for (unsigned i=0; i<left_particles; ++i)
            {
                SpawnParticle(
                    left,
                    bottom + (mHeight * RandFloat()),
                    PI
                    );
            }
            for (unsigned i=0; i<right_particles; ++i)
            {
                SpawnParticle(
                    right,
                    bottom + (mHeight * RandFloat()),
                    0.f
                    );
            }
        }
    }
}

