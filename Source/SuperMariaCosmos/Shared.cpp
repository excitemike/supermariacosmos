#include "SuperMariaCosmos/Shared.h"

namespace SuperMariaCosmos
{
    float ScreenWidth = 256.f;
    float ScreenHeight = 192.f;
    float SpaceBetweenScreens = 30.f;
    float WindowWidth = ScreenWidth;
    float WindowHeight = (ScreenHeight * 2.f) + SpaceBetweenScreens;
    
    const Subsystems::RenderManager::Color MOVE_HANDLE_COLOR   = { 1.f, 1.f, 1.f, 1.f};
    const Subsystems::RenderManager::Color EDGE_HANDLE_COLOR   = { 1.f, 1.f, 0.5f, 1.f};
    const Subsystems::RenderManager::Color CORNER_HANDLE_COLOR = { 1.f, 0.f, 0.f, 1.f};
    const Subsystems::RenderManager::Color HANDLE_BG_COLOR     = { 0.f, 0.f, 0.f, 1.f};

    float HANDLE_THICKNESS = 8.f;
    float HANDLE_SPACING = 2.f;
}
