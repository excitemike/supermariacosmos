#include "SuperMariaCosmos/LevelManager.h"

#include <iostream>
#include <fstream>
#include <assert.h>

const char * PROGRESSION_FILE = "levelprogression.txt";

namespace SuperMariaCosmos
{
    namespace SuperMariaSubsystems
    {
        LevelManager::LevelManager()
        {
        }

        void LevelManager::Init()
        {
            // load the level progression file
            std::ifstream file;
            file.open(PROGRESSION_FILE, std::fstream::in );

            // if we failed to open it, not sure what we ought to do here
            if (!file.is_open())
            {
                assert("failed to open level progession file");
            }
            else
            {
                while (!file.eof())
                {
                    std::string level;
                    file >> level;

                    if (level.size())
                    {
                        mLevelList.push_back(level);
                    }
                }
            }
        }

        // start the level progression over from the beginning
        void LevelManager::Restart()
        {
            mCurrentLevel = 0;
        }

        // get the filename to load for the level we are currently on
        const char* LevelManager::GetLevelFilename()
        {
            return mLevelList[mCurrentLevel].c_str();
        }

        // advance to the next level (affects future calls to GetLevelFilename())
        // returns a boolean to indicate whether it actually had a level to 
        // advance to
        bool LevelManager::AdvanceToNextLevel()
        {
            mCurrentLevel++;
            
            return (mCurrentLevel < mLevelList.size());
        }

        // returns whether we are at the end of the level progression.
        bool LevelManager::GameComplete()
        {
            return (mCurrentLevel >= mLevelList.size());
        }

    }
}

