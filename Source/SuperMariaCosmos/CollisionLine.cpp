#include "SuperMariaCosmos/CollisionLine.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    CollisionLine::CollisionLine()
    {}

    CollisionLine::CollisionLine(
        float start_x,
        float start_y,
        float end_x,
        float end_y) :
        StartX(start_y),
        StartY(start_y),
        EndX(end_x),
        EndY(end_y)
    {
    }

    // calculate the distance from a given point to this surface.
    float CollisionLine::GetDistance(float x, float y)
    {
        float surface_x;
        float surface_y;
        GetNearestPointOnSurface(x, y, &surface_x, &surface_y);

        float xdiff = x - surface_x;
        float ydiff = y - surface_y;
        float distance_squared = (xdiff*xdiff)+(ydiff*ydiff);

        return sqrtf(distance_squared);
    }

    // Projects both points onto the surface, and tries to move along 
    // the surface to get as near as it can to target without going beyond 
    // the distance limit.  If it would move off the end of the surface and 
    // onto another, then it will return a pointer to that surface.
    // Returns null if it did not move onto another surface.
    CollisionSurface* CollisionLine::MoveAlongSurface(
        float start_x, float start_y,
        float target_x, float target_y,
        float max_distance, 
        float object_radius,
        float *out_final_x,
        float *out_final_y,
        float *out_distance_travelled,
        float *out_normal_angle,
        Direction *out_direction)
    {
        CollisionSurface* pHandOffSurface = GetHandOffSurface(target_x, target_y);

        // first correct the inputs of necesary so that they are both ON the line
        GetNearestPointOnSurface(start_x, start_y, &start_x, &start_y);
        GetNearestPointOnSurface(target_x, target_y, &target_x, &target_y);

        float length;
        float direc_x;
        float direc_y;
        GetLineLengthAndDirection(length, direc_x, direc_y);
        float normal_x = direc_y;
        float normal_y = -direc_x;
        float normal_angle = SafeArcTan(normal_y, normal_x);

        float object_radius_offset_x = normal_x * object_radius;
        float object_radius_offset_y = normal_y * object_radius;

        // things would break pretty horribly if we tried to go a negative distance
        if (max_distance<0.f) {max_distance = -max_distance;}

        float distance_between = Distance(start_x, start_y, target_x, target_y);

        // maybe we can go all the way there
        if (distance_between < (max_distance + LENGTH_EPSILON) )
        {
            // we can go all the way there!
            if (NULL != out_final_x)
            {
                *out_final_x = target_x + object_radius_offset_x;
            }
            if (NULL != out_final_y)
            {
                *out_final_y = target_y + object_radius_offset_y;
            }
            if (NULL != out_distance_travelled)
            {
                *out_distance_travelled = distance_between;
            }
        }
        else
        {
            float lerp_value = max_distance / distance_between;

            // can't make it all the way
            if (NULL != out_final_x)
            {
                *out_final_x = Lerp(start_x, target_x, lerp_value) + object_radius_offset_x;
            }
            if (NULL != out_final_y)
            {
                *out_final_y = Lerp(start_y, target_y, lerp_value) + object_radius_offset_y;
            }
            if (NULL != out_distance_travelled)
            {
                * out_distance_travelled = max_distance;
            }

            // if we can't even hit an end of the line, we don't need to hand off
            pHandOffSurface = NULL;
        }

        // normal angle is always the same for lines
        if (NULL != out_normal_angle)
        {
            *out_normal_angle = normal_angle;
        }

        if (NULL != out_direction)
        {
            // compare dot products to see which way we are going here
            float start_dot_product = (start_x*direc_x) + (start_y*direc_y);
            float end_dot_product = (target_x*direc_x) + (target_y*direc_y);

            float difference = end_dot_product - start_dot_product;

            if (difference > LENGTH_EPSILON)
            {
                *out_direction = COUNTERCLOCKWISE;
            }
            else if (difference < -LENGTH_EPSILON)
            {
                *out_direction = CLOCKWISE;
            }
            else 
            {
                *out_direction = NONE;
            }
        }

        return pHandOffSurface;
    }

    // find the point on the surface nearest the given point
    void CollisionLine::GetNearestPointOnSurface(float x, float y, float *out_x, float *out_y)
    {
        float length;
        float direction_vec_x;
        float direction_vec_y;

        float xdiff = x - StartX;
        float ydiff = y - StartY;

        GetLineLengthAndDirection(length, direction_vec_x, direction_vec_y);

        // project the point onto the line
        float cross_product = (direction_vec_x * xdiff) + (direction_vec_y * ydiff);

        // make sure it is between the endpoints
        if (cross_product < 0.f) {cross_product = 0.f;}
        if (cross_product > length) {cross_product = length;}

        *out_x = StartX + (cross_product * direction_vec_x);
        *out_y = StartY + (cross_product * direction_vec_y);
    }
    
    void CollisionLine::GetLineLengthAndDirection(float &length, float &direction_vec_x, float &direction_vec_y)
    {
        float xdiff = EndX-StartX;
        float ydiff = EndY-StartY;
        float length_squared = (xdiff*xdiff)+(ydiff*ydiff);
        
        length = sqrtf(length_squared);

        if (length > LENGTH_EPSILON)
        {
            float scale = 1.f / length;
            direction_vec_x = xdiff * scale;
            direction_vec_y = ydiff * scale;
        }
        else
        {
            direction_vec_x = 1.f;
            direction_vec_y = 0.f;
        }
    }

    // IF moving toward this point would require a handoff, return which 
    // surface we would go to.
    // returns NULL if there would be no handoff
    CollisionSurface* CollisionLine::GetHandOffSurface(float target_x, float target_y)
    {
        float length;
        float direction_vec_x;
        float direction_vec_y;

        float xdiff = target_x - StartX;
        float ydiff = target_y - StartY;

        GetLineLengthAndDirection(length, direction_vec_x, direction_vec_y);

        // project the point onto the line
        float cross_product = (direction_vec_x * xdiff) + (direction_vec_y * ydiff);

        // make sure it is between the endpoints
        if (cross_product < 0.f) 
        {
            return mPrevSurface;
        }
        else if (cross_product > length) 
        {
            return mNextSurface;
        }
        else
        {
            return NULL;
        }
    }
}



