#include "SuperMariaCosmos/Subsystems.h"


namespace SuperMariaCosmos
{
    Subsystems::GameWindow GameWindow;
    Subsystems::RenderManager Renderer;
    Subsystems::SceneManager TopScreenScene;
    Subsystems::SceneManager BottomScreenScene;
    Subsystems::InputManager InputManager;
    Subsystems::WidgetManager WidgetManager;
    Subsystems::Sound Sound;
    SuperMariaSubsystems::LevelManager LevelManager;
}
