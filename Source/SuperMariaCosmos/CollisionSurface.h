#ifndef SUPERMARIACOSMOS_COLLISIONSURFACE_H
#define SUPERMARIACOSMOS_COLLISIONSURFACE_H

#ifndef NULL
#define NULL 0
#endif

namespace SuperMariaCosmos
{
    class CollisionSurface
    {
    public:
    	CollisionSurface();
        virtual ~CollisionSurface(){}

        // calculate the distance from a given point to this surface.
        virtual float GetDistance(float x, float y) = 0;

        enum Direction
        {
            CLOCKWISE,
            COUNTERCLOCKWISE,
            NONE
        };

        enum SurfaceType
        {
            NORMAL = 0,
            DEATHZONE,
            NUM_TYPES
        };

        // Projects both points onto the surface, and tries to move along
        // the surface to get as near as it can to target without going beyond
        // the distance limit.  If it would move off the end of the surface and
        // onto another, then it will return a pointer to that surface.
        // Returns null if it did not move onto another surface.
        virtual CollisionSurface* MoveAlongSurface(
            float start_x, float start_y,
            float target_x, float target_y,
            float max_distance,
            float object_radius = 0.f,
            float *out_final_x = NULL,
            float *out_final_y = NULL,
            float *out_distance_travelled = NULL,
            float *out_normal_angle = NULL,
            Direction *out_direction = NULL) = 0;

        // find the point on the surface nearest the given point
        virtual void GetNearestPointOnSurface(float x, float y, float *out_x, float *out_y) = 0;

        // set what surface to go to after running off the end of this one
        // in the "forward" direction
        void SetNextSurface(CollisionSurface* pNextSurface){mNextSurface = pNextSurface;}

        // set what surface to go to after running off the end of this one
        // in the "backwards" direction
        void SetPrevSurface(CollisionSurface* pPrevSurface){mPrevSurface = pPrevSurface;}

        // get what kind of surface it is
        virtual SurfaceType GetSurfaceType() {return mType;}

        // set what kind of surface it is
        virtual void SetSurfaceType(SurfaceType type) {mType = type;}
    protected:
        CollisionSurface* mNextSurface;
        CollisionSurface* mPrevSurface;
        SurfaceType mType;
    };
}


#endif // SUPERMARIACOSMOS_COLLISIONSURFACE_H

