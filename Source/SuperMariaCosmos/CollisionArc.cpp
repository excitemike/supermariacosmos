#include "SuperMariaCosmos/CollisionArc.h"
#include "SuperMariaCosmos/Shared.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    CollisionArc::CollisionArc()
    {}

    CollisionArc::CollisionArc(
        float center_x, float center_y,
        float start_angle, float end_angle,
        float radius, bool is_outer) :
        CenterX(center_x),
        CenterY(center_y),
        StartAngle(start_angle),
        EndAngle(end_angle),
        Radius(radius),
        IsOuter(is_outer)
    {
    }

    // calculate the distance from a given point to this surface.
    float CollisionArc::GetDistance(float x, float y)
    {
        float surface_x;
        float surface_y;
        GetNearestPointOnSurface(x, y, &surface_x, &surface_y);

        float xdiff = x - surface_x;
        float ydiff = y - surface_y;
        float distance_squared = (xdiff*xdiff)+(ydiff*ydiff);

        return sqrtf(distance_squared);
    }

    // Projects both points onto the surface, and tries to move along 
    // the surface to get as near as it can to target without going beyond 
    // the distance limit.  If it would move off the end of the surface and 
    // onto another, then it will return a pointer to that surface.
    // Returns null if it did not move onto another surface.
    CollisionSurface* CollisionArc::MoveAlongSurface(
        float start_x, float start_y,
        float target_x, float target_y,
        float max_distance,
        float object_radius,
        float *out_final_x,
        float *out_final_y,
        float *out_distance_travelled,
        float *out_normal_angle,
        Direction *out_direction)
    {
        CollisionSurface* pHandOffSurface = GetHandOffSurface(target_x, target_y);

        float start_angle = GetNearestAngleTo(start_x, start_y);
        float target_angle = GetNearestAngleTo(target_x, target_y);

        float angle_change = AngleDiff(StartAngle, target_angle) - AngleDiff(StartAngle, start_angle);

        // stay within the limits
        float dividable_radius = (Radius > LENGTH_EPSILON) ? Radius : LENGTH_EPSILON;
        float max_angle_change = max_distance / dividable_radius;
        if (angle_change < -max_angle_change) 
        {
            angle_change = -max_angle_change;

            // if we aren't actually making the full move, no handoff will be needed
            pHandOffSurface = NULL;
        }
        if (angle_change > max_angle_change) 
        {
            angle_change = max_angle_change;

            // if we aren't actually making the full move, no handoff will be needed
            pHandOffSurface = NULL;
        }

        float final_angle = start_angle + angle_change;
        float total_radius;
        if (IsOuter)
        {
            total_radius = Radius + object_radius;
        }
        else
        {
            total_radius = Radius - object_radius;
        }

        if (NULL != out_final_x)
        {
            *out_final_x = CenterX + (total_radius * cosf(final_angle));
        }
        if (NULL != out_final_y)
        {
            *out_final_y = CenterY + (total_radius * sinf(final_angle));
        }
        if (NULL != out_distance_travelled)
        {
            *out_distance_travelled = fabsf(angle_change * Radius);
        }
        if (NULL  != out_normal_angle)
        {
            if (IsOuter)
            {
                *out_normal_angle = final_angle;
            }
            else
            {
                *out_normal_angle = final_angle + PI;
            }
        }
        if (NULL != out_direction)
        {
            if ( (-ANGLE_EPSILON < angle_change) && (angle_change < ANGLE_EPSILON) )
            {
                *out_direction = NONE;
            }
            else
            {
                if (IsOuter)
                {
                    *out_direction = (angle_change > 0.f) ? COUNTERCLOCKWISE : CLOCKWISE;
                }
                else
                {
                    *out_direction = (angle_change > 0.f) ? CLOCKWISE : COUNTERCLOCKWISE;
                }
            }
        }

        return pHandOffSurface;
    }

    // find the point on the surface nearest the given point
    void CollisionArc::GetNearestPointOnSurface(float x, float y, float *out_x, float *out_y)
    {
        float angle = GetAngleTo(x, y);

        float angle_diff = AngleDiff(StartAngle, angle);
        float arc_angle = AngleDiff(StartAngle, EndAngle);

        if (angle_diff <= arc_angle )
        {
            // it is within the arc
            *out_x = CenterX + (Radius * cosf(angle));
            *out_y = CenterY + (Radius * sinf(angle));
        }
        else
        {
            // it is not within the arc, so we just have to figure out which
            // endpoint to give them
            float midpoint_of_arc = arc_angle * 0.5f;
            float divider = midpoint_of_arc + PI; // past this angle, startpoint is nearer than endpoint

            if (angle_diff < divider)
            {
                *out_x = CenterX + (Radius * cosf(EndAngle));
                *out_y = CenterY + (Radius * sinf(EndAngle));
            }
            else
            {
                *out_x = CenterX + (Radius * cosf(StartAngle));
                *out_y = CenterY + (Radius * sinf(StartAngle));
            }
        }
    }

    // returns angle from center to the specified point
    // returns a value in [-PI, PI]
    float CollisionArc::GetAngleTo(float x, float y)
    {
        float xdiff = (x - CenterX);
        float ydiff = (y - CenterY);

        return SafeArcTan(ydiff, xdiff);
    }
    
    // returns an angle from center to the specified point
    // as near as it can get without leaving the arc
    float CollisionArc::GetNearestAngleTo(float x, float y)
    {
        float angle = GetAngleTo(x, y);
        
        float angle_diff = AngleDiff(StartAngle, angle);
        float arc_angle = AngleDiff(StartAngle, EndAngle);

        if (angle_diff <= arc_angle)
        {
            // within the arc, actual angle is fine
            return angle;
        }
        else
        {
            // not within the arc.  got to pick an endpoint
            float midpoint_of_arc = arc_angle * 0.5f;
            float divider = midpoint_of_arc + PI; // past this angle, startpoint is nearer than endpoint
            
            if (angle_diff < divider)
            {
                return EndAngle;
            }
            else
            {
                return StartAngle;
            }
        }
    }

    // IF moving toward this point would require a handoff, return which 
    // surface we would go to.
    // returns NULL if there would be no handoff
    CollisionSurface* CollisionArc::GetHandOffSurface(float target_x, float target_y)
    {
        float angle = GetAngleTo(target_x, target_y);
        
        float angle_diff = AngleDiff(StartAngle, angle);
        float arc_angle = AngleDiff(StartAngle, EndAngle);

        if (angle_diff <= arc_angle)
        {
            return NULL;
        }
        else
        {
            // not within the arc.  got to pick an endpoint
            float midpoint_of_arc = arc_angle * 0.5f;
            float divider = midpoint_of_arc + PI; // past this angle, startpoint is nearer than endpoint
            
            if (angle_diff < divider)
            {
                return IsOuter ? mNextSurface : mPrevSurface;
            }
            else
            {
                return IsOuter ? mPrevSurface : mNextSurface;
            }
        }
    }
}



