#ifndef SUPERMARIACOSMOS_LEVELMANAGER_H
#define SUPERMARIACOSMOS_LEVELMANAGER_H

#include "Framework/BaseSubsystem.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"
#include <vector>
#include <string>

namespace SuperMariaCosmos
{
    namespace SuperMariaSubsystems
    {
        class LevelManager : public Framework::BaseSubsystem
        {
        public:
            LevelManager();

            void Init();

            // start the level progression over from the beginning
            void Restart();

            // get the filename to load for the level we are currently on
            const char* GetLevelFilename();

            // advance to the next level (affects future calls to GetLevelFilename())
            // returns a boolean to indicate whether it actually had a level to 
            // advance to
            bool AdvanceToNextLevel();

            // returns whether we are at the end of the level progression.
            bool GameComplete();

        private:
            unsigned mCurrentLevel;

            static const unsigned LEVEL_NAME_BUF_SIZE = 256;

            typedef std::string LevelName;
            typedef std::vector<LevelName> LevelList;
            LevelList mLevelList;
        };
    }
}

#endif // SUPERMARIACOSMOS_LEVELMANAGER_H

