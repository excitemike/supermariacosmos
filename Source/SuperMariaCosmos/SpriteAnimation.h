#ifndef SUPERMARIACOSMOS_SPRITEANIMATION_H
#define SUPERMARIACOSMOS_SPRITEANIMATION_H

namespace Utilities
{
    class Texture;
}

namespace SuperMariaCosmos
{
    class SpriteAnimation
    {
    public: 
        SpriteAnimation();
        ~SpriteAnimation(){Shutdown();}

        // set up an animation
        // texture_files should point to an array of numFrames char* s
        void Init(float secondsPerFrame, unsigned numFrames, const char **texture_files, bool loop=false);

        // cleanup 
        void Shutdown();

        // advance the play head
        void Update(float elapsedTime);

        // figure out which texture goes with where we are in the animation
        const Utilities::Texture * GetCurrentFrame();

        // restart the animation
        void Restart();
    private:
        float mCurrentTime;
        float mSecondsPerFrame;
        unsigned mNumFrames;
        const Utilities::Texture **mFrames;
        bool mLoop;
    };
}

#endif // SUPERMARIACOSMOS_SPRITEANIMATION_H

