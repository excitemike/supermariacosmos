// handy code shared by the different SuperMariaCosmos gamestates and the game class
#ifndef SUPERMARIACOSMOS_SHARED_H
#define SUPERMARIACOSMOS_SHARED_H
#include "Subsystems.h"

namespace SuperMariaCosmos
{
    enum SceneLayer
    {
        LAYER_BACKGROUND = 0,
        LAYER_WIDGETS,
        LAYER_PLAYER,
        LAYER_FOREGROUND,
        LAYER_COUNT
    };

    extern float ScreenWidth;
    extern float ScreenHeight;
    extern float SpaceBetweenScreens;
    extern float WindowWidth;
    extern float WindowHeight;

    // colors used in the level editor
    extern const Subsystems::RenderManager::Color MOVE_HANDLE_COLOR;
    extern const Subsystems::RenderManager::Color EDGE_HANDLE_COLOR;
    extern const Subsystems::RenderManager::Color CORNER_HANDLE_COLOR;
    extern const Subsystems::RenderManager::Color HANDLE_BG_COLOR;

    extern float HANDLE_THICKNESS;
    extern float HANDLE_SPACING;

    // function for determining whether the mouse location is on the touchscreen
    inline bool IsOnTouchScreen( float mouseX, float mouseY )
    {
        return ( mouseY < SuperMariaCosmos::ScreenHeight )
            && ( mouseY > 0 )
            && ( mouseX < SuperMariaCosmos::ScreenWidth )
            && ( mouseX > 0 );
    }
}

// this should be built into the language/framework
#define SAFEDELETE(ptr) if (NULL!=ptr) {delete ptr; ptr=NULL;}

#define ADD_WIDGET(var, className, constructorArgs) \
    REMOVE_WIDGET(var); \
    var = new className constructorArgs ; \
    SuperMariaCosmos::WidgetManager.AddWidget(var);

#define REMOVE_WIDGET(var) \
    if (var != NULL) \
    {   \
        SuperMariaCosmos::WidgetManager.RemoveWidget(var); \
        delete var; \
        var = NULL; \
    }

// allocate an object and add it to the scene
#define ADD_OBJECT(var, className, constructorArgs, layer, scenemgr) \
    REMOVE_OBJECT(var, layer, scenemgr); \
    var = new className constructorArgs ; \
    scenemgr.AddObject(var, layer);

// remove the object from the scene and free the memory
#define REMOVE_OBJECT(var, layer, scenemgr) \
    if (var!=NULL) \
    {\
        scenemgr.RemoveObject(var, layer); \
        delete var; \
        var=NULL; \
    }

// allocate an object and add it to the scene
#define ADD_OBJECT_TOP(var, className, constructorArgs, layer) ADD_OBJECT(var, className, constructorArgs, layer, SuperMariaCosmos::TopScreenScene)
#define ADD_OBJECT_BOTTOM(var, className, constructorArgs, layer) ADD_OBJECT(var, className, constructorArgs, layer, SuperMariaCosmos::BottomScreenScene)
#define REMOVE_OBJECT_TOP(var, layer) REMOVE_OBJECT(var, layer, SuperMariaCosmos::TopScreenScene)
#define REMOVE_OBJECT_BOTTOM(var, layer) REMOVE_OBJECT(var, layer, SuperMariaCosmos::BottomScreenScene)


#endif // SUPERMARIACOSMOS_SHARED_H
