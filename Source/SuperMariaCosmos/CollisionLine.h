#ifndef SUPERMARIACOSMOS_COLLISIONLINE_H
#define SUPERMARIACOSMOS_COLLISIONLINE_H

#include "SuperMariaCosmos/CollisionSurface.h"

namespace SuperMariaCosmos
{
    class CollisionLine : public CollisionSurface
    {
    public:
        CollisionLine();

        CollisionLine(
            float start_x, float start_y,
            float end_x, float end_y);

        // calculate the distance from a given point to this surface.
        virtual float GetDistance(float x, float y);

        // Projects both points onto the surface, and tries to move along 
        // the surface to get as near as it can to target without going beyond 
        // the distance limit.  If it would move off the end of the surface and 
        // onto another, then it will return a pointer to that surface.
        // Returns null if it did not move onto another surface.
        virtual CollisionSurface* MoveAlongSurface(
            float start_x, float start_y,
            float target_x, float target_y,
            float max_distance, 
            float object_radius = 0.f,
            float *out_final_x = NULL,
            float *out_final_y = NULL,
            float *out_distance_travelled = NULL,
            float *out_normal_angle = NULL,
            Direction *out_direction = NULL);

        // find the point on the surface nearest the given point
        virtual void GetNearestPointOnSurface(float x, float y, float *out_x, float *out_y);

        //
        // public members because... well, why not?
        //

        float StartX;
        float StartY;
        float EndX;
        float EndY;
    private:
        void GetLineLengthAndDirection(float &length, float &direction_vec_x, float &direction_vec_y);

        // IF moving toward this point would require a handoff, return which 
        // surface we would go to.
        // returns NULL if there would be no handoff
        CollisionSurface* GetHandOffSurface(float target_x, float target_y);
    };
}

#endif // SUPERMARIACOSMOS_COLLISIONLINE_H

