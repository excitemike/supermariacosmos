#ifndef SUBSYSTEMS_H
#define SUBSYSTEMS_H

#include "Subsystems/GameWindow.h"
#include "Subsystems/RenderManager.h"
#include "Subsystems/SceneManager.h"
#include "Subsystems/InputManager.h"
#include "Subsystems/WidgetManager.h"
#include "Subsystems/Sound.h"
#include "SuperMariaCosmos/LevelManager.h"

namespace SuperMariaCosmos
{
    extern Subsystems::GameWindow GameWindow;
    extern Subsystems::RenderManager Renderer;
    extern Subsystems::SceneManager TopScreenScene;
    extern Subsystems::SceneManager BottomScreenScene;
    extern Subsystems::InputManager InputManager;
    extern Subsystems::WidgetManager WidgetManager;
    extern Subsystems::Sound Sound;
    extern SuperMariaSubsystems::LevelManager LevelManager;
}

#endif // SUBSYSTEMS_H
