#ifndef SUPERMARIACOSMOS_LEVEL_H
#define SUPERMARIACOSMOS_LEVEL_H

#include "SceneObjects/WidgetHolder.h"
#include "SceneObjects/Sprite.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/LevelObject.h"
#include "SuperMariaCosmos/SceneObjects/LevelObjects/PlanetTypes.h"

namespace SuperMariaCosmos {
    namespace SceneObjects {
        namespace LevelObjects {
            class LevelObjectObserver;
        }
    }
}

namespace SuperMariaCosmos
{
    // class to create and manage the game's scene and the map
    class Level
    {
    public:
        static const float CAMERA_MIN_X;
        static const float CAMERA_MAX_X;
        static const float CAMERA_MIN_Y;
        static const float CAMERA_MAX_Y;

        Level();
        virtual ~Level(){}

        typedef std::vector<SuperMariaCosmos::SceneObjects::LevelObject*> LevelObjectList;

        // if you call this, make sure you also call unload somewheres
        // loads a level file and add its objects to the scene
        // you may also optionally specify a LevelObjectObserver to be
        // registered with the loaded level objects
        virtual void Load(const char *filename, SceneObjects::LevelObjects::LevelObjectObserver *pObserver = NULL);

        // cleans up the sceneobjects and widgets it has new'd and given to
        // the managers
        virtual void Unload();

        // create an object and add it to the scene
        virtual SceneObjects::LevelObject * AddNewObject(SceneObjects::LevelObjectType type);

        // remove an object from the scene, free the memory, everything
        virtual void DeleteObject(SuperMariaCosmos::SceneObjects::LevelObject* pObject);

        // write the level to a file
        virtual void Save(const char *filename);

        // so clients can loop over objects in the scene and do whatever is needed
        virtual const LevelObjectList* GetLevelObjectList() {return &mLevelObjects;}

        // pan around the level
        virtual void SetCameraPosition(float x, float y);

        // map coordinates from the level's space
        virtual void ApplyLevelTransform(float in_x, float in_y, float &out_x, float &out_y);

        // map coordinates into the level's space
        virtual void ApplyLevelTransformInverse(float in_x, float in_y, float &out_x, float &out_y);
    protected:
        // load a file and put all the things it tells you to make into mLevelObjectsHolder (and mMapHolder)
        // (DOES NOT CHECK FOR NULL)
        virtual void LoadLevelIntoHolders(const char *filename, SceneObjects::LevelObjects::LevelObjectObserver *pObserver);

        // load a file and allocate and init the objects it tells us to
        virtual void ReadFileAndCreateObjects(const char *filename, SceneObjects::LevelObjects::LevelObjectObserver *pObserver);

        // loads the next level object from the given, already opened file
        // returns true if it is ok to continue
        // (object is NOT added to the scene by this)
        virtual bool LoadNextObject(FILE* file, SceneObjects::LevelObjects::LevelObjectObserver *pObserver);

        // create an object and add it to the list of created objects but do NOT
        // add it to the scene
        virtual SceneObjects::LevelObject * CreateNewObject(SceneObjects::LevelObjectType type);

        // once we have loaded the level and created and initialized the objects
        // (all of which is taken care of by ReadFileAndCreateObjects) we add
        // them to the scene here.
        // really should only be called in LoadLevelIntoHolders
        virtual void AddCreatedObjectsToScene();

        // put a created object into the scene (as the last rendered thing)
        void AddToScene(SuperMariaCosmos::SceneObjects::LevelObject *pLevelObject);

        // move the scroll indicator to show what chunk of the map ew are zoomed in on
        virtual void UpdateScrollIndicator();

        // return true if A should appear before B when sorting.
        // Used to sort the newly created objects before putting them in the scene
        static bool LevelObjectSort(SuperMariaCosmos::SceneObjects::LevelObject *pA, SuperMariaCosmos::SceneObjects::LevelObject *pB);

        // return a number indicating at what layer in the scene the object
        // should appear.  (used by LevelObjectSort)
        static int GetLayer(SuperMariaCosmos::SceneObjects::LevelObject *pObject);
    private:
        ::SceneObjects::BaseWidget *mLevelObjectsHolder;
        ::SceneObjects::BaseWidget *mMapHolder;
        ::SceneObjects::Sprite *mScrollIndicator;

        LevelObjectList mLevelObjects;
    };
}

#endif // SUPERMARIACOSMOS_LEVEL_H
