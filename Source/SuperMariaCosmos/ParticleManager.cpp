#include "SuperMariaCosmos/ParticleManager.h"
#include "Utilities/algorithm.h"
#include "Framework/math.h"

#include <float.h> // for FLT_MAX
#include <assert.h>

namespace SuperMariaCosmos
{
    typedef std::binary_function<const SuperMariaCosmos::ParticleManager::ParticleData&, float, void> UpdateDelegate;

    ParticleManager::ParticleManager()
    {
    }

    void ParticleManager::Init(unsigned maxParticles)
    {
        mParticles.clear();
        mParticles.reserve(maxParticles);
    }

    void ParticleManager::Render(Subsystems::RenderManager* pRenderer)
    {
        Utilities::for_each(
            mParticles,
            &ParticleManager::RenderParticle,
            pRenderer);
    }

    void ParticleManager::Update(float secondsSinceLastFrame)
    {
        Utilities::for_each(
            mParticles,
            &ParticleManager::UpdateParticle,
            secondsSinceLastFrame);
    }

    // clean up
    void ParticleManager::Shutdown()
    {
        Utilities::for_each(mParticles, DeleteParticle);
        mParticles.clear();
    }

    void ParticleManager::SpawnParticle(const ParticleData& particle_data)
    {
        using namespace Utilities;

        if (mParticles.size() < mParticles.capacity())
        {
            mParticles.push_back(new Particle(particle_data));
        }
        else
        {
            // okay we're going to have to use a different one.
            // find a dead one or at least the closest one to
            // dying
            ParticleList::iterator it = mParticles.begin();
            float lowest_remaining_life_time = FLT_MAX;
            Particle* nearest_to_dying = NULL;
            for (; it != mParticles.end(); ++it)
            {
                if (((*it)->remaining_lifetime < lowest_remaining_life_time) || (NULL == nearest_to_dying))
                {
                    nearest_to_dying = *it;
                    lowest_remaining_life_time = nearest_to_dying->remaining_lifetime;
                }

                if (!nearest_to_dying->IsAlive())
                {
                    break;
                }
            }

            // okay we found it and make it be the new particle
            if (NULL != nearest_to_dying)
            {
                (*nearest_to_dying) = particle_data;
            }
            else
            {
                // somehow we failed to find anything
                assert(false);
            }
        }
    }

    // helper function for Shutdown
    void ParticleManager::DeleteParticle(ParticleManager::Particle* p)
    {
        delete p;
    }

	// helper function for when iterating a collection
	void ParticleManager::RenderParticle(ParticleManager::Particle *pParticle, Subsystems::RenderManager* pRenderer)
	{
		pParticle->Render(pRenderer);
	}

	// helper function for when iterating a collection
	void ParticleManager::UpdateParticle(ParticleManager::Particle *pParticle, float secondsSinceLastFrame)
	{
		pParticle->Update(secondsSinceLastFrame);
	}

    // initializes particles
    ParticleManager::Particle::Particle()
    {
        texture = NULL;
        remaining_lifetime = 0.f;
    }

    // cast/copy constructor
    ParticleManager::Particle::Particle(const ParticleData& data)
    {
        *this = data;
    }

    // set particle data
    ParticleManager::Particle& ParticleManager::Particle::operator=(const ParticleData& data)
    {
        texture            = data.texture;
        x                  = data.x;
        y                  = data.y;
        velocity_x         = data.velocity_x;
        velocity_y         = data.velocity_y;
        remaining_lifetime = data.remaining_lifetime;
        rotation           = SafeArcTan(velocity_y, velocity_x);

        return *this;
    }

    // move / age the particle
    void ParticleManager::Particle::Update(float secondsSinceLastFrame)
    {
        if (IsAlive())
        {
            remaining_lifetime -= secondsSinceLastFrame;

            x += (velocity_x * secondsSinceLastFrame);
            y += (velocity_y * secondsSinceLastFrame);
        }
    }

    // find out if the particle is still alive
    bool ParticleManager::Particle::IsAlive() const
    {
        return remaining_lifetime > 0.f;
    }

    // draw the particle
    void ParticleManager::Particle::Render(Subsystems::RenderManager* pRenderer)
    {
        if (IsAlive())
        {
            float half_width = 0.5f * texture->width;
            float half_height = 0.5f * texture->height;
            pRenderer->RenderSprite(
                texture,
                x-half_width, y-half_height,
                texture->width, texture->height,
                false, false,
                rotation,
                half_width, half_height);
        }
    }
}

