#include "SuperMariaCosmos/Level.h"
#include "SuperMariaCosmos/Shared.h"

#include <algorithm>
#include <functional>

static const float MAP_SCALE = 0.25f;

static const float TOTAL_MAP_WIDTH = (SuperMariaCosmos::ScreenWidth / MAP_SCALE);
static const float TOTAL_MAP_HEIGHT = (SuperMariaCosmos::ScreenHeight / MAP_SCALE);
static const float SCROLLABLE_MAP_WIDTH = TOTAL_MAP_WIDTH - SuperMariaCosmos::ScreenWidth;
static const float SCROLLABLE_MAP_HEIGHT = TOTAL_MAP_HEIGHT - SuperMariaCosmos::ScreenHeight;

const float SuperMariaCosmos::Level::CAMERA_MIN_X = -0.5f * SCROLLABLE_MAP_WIDTH + (SuperMariaCosmos::ScreenWidth * 0.5f);
const float SuperMariaCosmos::Level::CAMERA_MAX_X =  0.5f * SCROLLABLE_MAP_WIDTH + (SuperMariaCosmos::ScreenWidth * 0.5f);
const float SuperMariaCosmos::Level::CAMERA_MIN_Y = -0.5f * SCROLLABLE_MAP_HEIGHT + (SuperMariaCosmos::ScreenHeight * 0.5f);
const float SuperMariaCosmos::Level::CAMERA_MAX_Y =  0.5f * SCROLLABLE_MAP_HEIGHT + (SuperMariaCosmos::ScreenHeight * 0.5f);

// playing around with <functional>
typedef void (SuperMariaCosmos::Level::*ptr_member_fun)(SuperMariaCosmos::SceneObjects::LevelObject *pObject);
class member_fun
{
public:
    typedef SuperMariaCosmos::SceneObjects::LevelObject *argument_type;
    typedef void result_type;

    member_fun( SuperMariaCosmos::Level*pThis, ptr_member_fun func)
    {
        mThis = pThis;
        mFunc = func;
    }
    void operator()(SuperMariaCosmos::SceneObjects::LevelObject *pObject)
    {
        (mThis->*mFunc)(pObject);
    }
private:
    SuperMariaCosmos::Level* mThis;
    ptr_member_fun mFunc;
};

namespace SuperMariaCosmos
{
    Level::Level() :
        mLevelObjectsHolder(NULL),
        mMapHolder(NULL),
        mScrollIndicator(NULL)
    {
    }

    // if you call this, make sure you also call unload somewheres
    // loads a level file and add its objects to the scene
    // you may also optionally specify a LevelObjectObserver to be
    // registered with the loaded level objects
    void Level::Load(const char *filename, SceneObjects::LevelObjects::LevelObjectObserver *pObserver)
    {
        ADD_WIDGET(
            mLevelObjectsHolder,
            ::SceneObjects::BaseWidget,
            ());
        ADD_OBJECT_TOP(
            mMapHolder,
            ::SceneObjects::BaseWidget,
            (),
            LAYER_WIDGETS);
        ADD_OBJECT_TOP(
            mScrollIndicator,
            ::SceneObjects::Sprite,
            ("Content/Textures/scroll_indicator.png", 66.f, 50.f),
            LAYER_FOREGROUND);

        mMapHolder->SetScale(MAP_SCALE);

        // make the centers line up
        float translate_scale = 0.5f * (1.f - MAP_SCALE);
        mMapHolder->SetTranslation(ScreenWidth*translate_scale, ScreenHeight*translate_scale);

        mMapHolder->SetUpdateChildWidgets(false);

        LoadLevelIntoHolders(filename, pObserver );
    }

    // load a file and put all the things it tells you to make into mLevelObjectsHolder (and mMapHolder)
    // (DOES NOT CHECK FOR NULL)
    void Level::LoadLevelIntoHolders(const char *filename, SceneObjects::LevelObjects::LevelObjectObserver *pObserver)
    {
        ReadFileAndCreateObjects(filename, pObserver);
        AddCreatedObjectsToScene();
    }

    // load a file and allocate and init the objects it tells us to
    void Level::ReadFileAndCreateObjects(const char *filename, SceneObjects::LevelObjects::LevelObjectObserver *pObserver)
    {
        FILE* file = fopen(filename, "r");

        // if we failed to open it here, just nevermind and we'll fall
        // back on starting with an empty level
        if (NULL != file)
        {
            // as long as we still have lines to look at...
            // check the line to see what type of thing we are creating.
            while ( LoadNextObject(file, pObserver) && !feof(file) )
            {
            }

            fclose(file);
        }
    }

    // loads the next level object from the given, already opened file
    // returns true if it is ok to continue
    // (object is NOT added to the scene by this)
    bool Level::LoadNextObject(FILE* file, SceneObjects::LevelObjects::LevelObjectObserver *pObserver)
    {
        int type = 0xbaddc0de; // initialize to something to appease Mr. Compiler
        int scanResult = fscanf(file, "%d", &type);

        bool found_type = ((scanResult > 0) && (scanResult != EOF));

        if (found_type)
        {
            // create it and have it read its config
            SuperMariaCosmos::SceneObjects::LevelObjectType levelObjectType
                = (SuperMariaCosmos::SceneObjects::LevelObjectType)type;

            SuperMariaCosmos::SceneObjects::LevelObject *pObject
                = CreateNewObject(levelObjectType);

            if (NULL != pObject)
            {
                pObject->Read(file);
                pObject->SetObserver(pObserver);
            }
        }

        return found_type;
    }

    // if you call this, make sure you also call unload somewheres
    // loads a level file and add its objects to the scene
    void Level::Unload()
    {
        // make sure all those widgets get cleared up
        LevelObjectList::reverse_iterator it;
        for (it = mLevelObjects.rbegin(); it != mLevelObjects.rend(); ++it)
        {
            mLevelObjectsHolder->RemoveWidget(*it);
            mMapHolder->RemoveWidget(*it);
            SuperMariaCosmos::SceneObjects::FreePlanet(*it);
            (*it) = NULL;
        }
        mLevelObjects.clear();

        // and the holder
        REMOVE_WIDGET(mLevelObjectsHolder);
        REMOVE_OBJECT_TOP(mScrollIndicator, LAYER_FOREGROUND);
        REMOVE_OBJECT_TOP(mMapHolder, LAYER_WIDGETS);
    }


    // create an object and add it to the list of created objects but do NOT
    // add it to the scene
    SceneObjects::LevelObject * Level::CreateNewObject(SceneObjects::LevelObjectType type)
    {
        SceneObjects::LevelObject *pLevelObject =
            SuperMariaCosmos::SceneObjects::AllocateNewPlanet(type);

        mLevelObjects.push_back(pLevelObject);
        return pLevelObject;
    }

    // create an object and add it to the scene
    SceneObjects::LevelObject * Level::AddNewObject(SceneObjects::LevelObjectType type)
    {
        SceneObjects::LevelObject *pLevelObject = CreateNewObject(type);
        AddToScene(pLevelObject);

        return pLevelObject;
    }

    // put a created object into the scene (as the last rendered thing)
    void Level::AddToScene(SceneObjects::LevelObject *pLevelObject)
    {
        mMapHolder->AddWidget(pLevelObject);
        mLevelObjectsHolder->AddWidget(pLevelObject);
    }

    // once we have loaded the level and created and initialized the objects
    // (all of which is taken care of by ReadFileAndCreateObjects) we add
    // them to the scene here.
    // really should only be called in LoadLevelIntoHolders
    void Level::AddCreatedObjectsToScene()
    {
        std::stable_sort(mLevelObjects.begin(), mLevelObjects.end(), Level::LevelObjectSort);

        std::for_each(
            mLevelObjects.begin(), mLevelObjects.end(),
            member_fun(this, &Level::AddToScene) );
    }

    void Level::Save(const char* filename)
    {
        FILE* file = fopen(filename, "w");

        // todo: actually handle this case somehow
        assert(NULL!=file);

        // each level object...
        std::vector<SuperMariaCosmos::SceneObjects::LevelObject*>::iterator it;
        for (it = mLevelObjects.begin(); it != mLevelObjects.end(); ++it)
        {
            // ...needs to dump it's info to the file
            (*it)->Write(file);
        }

        // done saving
        fclose(file);
    }

    // remove an object from the scene, free the memory, everything
    void Level::DeleteObject(SuperMariaCosmos::SceneObjects::LevelObject* pObject)
    {
        // remove from our internal list of level objects
        std::vector<SuperMariaCosmos::SceneObjects::LevelObject*>::iterator it =
            std::find( mLevelObjects.begin(), mLevelObjects.end(), pObject );
        if ( it != mLevelObjects.end() )
        {
            mLevelObjects.erase(it);
        }

        // remove from scene
        if (pObject == SuperMariaCosmos::WidgetManager.GetSelected())
        {
            SuperMariaCosmos::WidgetManager.Deselect();
        }
        mLevelObjectsHolder->RemoveWidget(pObject);
        mMapHolder->RemoveWidget(pObject);
    }

    // pan around the level
    void Level::SetCameraPosition(float x, float y)
    {
        // but keep it within the limits
        if (x < CAMERA_MIN_X) {x = CAMERA_MIN_X;}
        if (x > CAMERA_MAX_X) {x = CAMERA_MAX_X;}
        if (y < CAMERA_MIN_Y) {y = CAMERA_MIN_Y;}
        if (y > CAMERA_MAX_Y) {y = CAMERA_MAX_Y;}

        // scroll to center the screen on that point
        float scroll_x =  (ScreenWidth * 0.5f) - x;
        float scroll_y = (ScreenHeight * 0.5f) - y;

        mLevelObjectsHolder->SetTranslation(scroll_x, scroll_y);

        UpdateScrollIndicator();
    }

    // move the scroll indicator to show what chunk of the map ew are zoomed in on
    void Level::UpdateScrollIndicator()
    {
        const ::SceneObjects::Transform& level_transform = mLevelObjectsHolder->GetTransform();
        const ::SceneObjects::Transform& map_transform = mMapHolder->GetTransform();

        float transformed_x;
        float transformed_y;

        // map screen center onto the map
        // taking big advantage of the fact that we know the level only
        // translates and that map does not rotate
        map_transform.Apply(
            (ScreenWidth*0.5f)-level_transform.Translation_x,
            (ScreenHeight*0.5f)-level_transform.Translation_y,
            transformed_x, transformed_y);

        // force it to exactly on a pixel to avoid flickering as it moves
        transformed_x = floorf(transformed_x);
        transformed_y = floorf(transformed_y);

        // center indicator on that point
        mScrollIndicator->SetX( transformed_x - (mScrollIndicator->GetWidth()*0.5f));
        mScrollIndicator->SetY( transformed_y - (mScrollIndicator->GetHeight()*0.5f));
    }

    // map coordinates from the level's space
    void Level::ApplyLevelTransform(float in_x, float in_y, float &out_x, float &out_y)
    {
        mLevelObjectsHolder->GetTransform().Apply(in_x, in_y, out_x, out_y);
    }

    // map coordinates into the level's space
    void Level::ApplyLevelTransformInverse(float in_x, float in_y, float &out_x, float &out_y)
    {
        mLevelObjectsHolder->GetTransform().ApplyInverse(in_x, in_y, out_x, out_y);
    }



    // return true if A should appear before B when sorting.
    // Used to sort the newly created objects before putting them in the scene
    bool Level::LevelObjectSort(SuperMariaCosmos::SceneObjects::LevelObject *pA, SuperMariaCosmos::SceneObjects::LevelObject *pB)
    {
        return ( GetLayer(pA) < GetLayer(pB) );
    }

    // return a number indicating at what layer in the scene the object
    // should appear.  (used by LevelObjectSort)
    int Level::GetLayer(SuperMariaCosmos::SceneObjects::LevelObject *pObject)
    {
        // treating keys doors and Maria special
        SceneObjects::SuperMaria *pMaria;
        SceneObjects::Key *pKey;
        SceneObjects::Goal *pGoal;

        if (pObject->CastMe(&pMaria))
        {
            return 24;
        }
        else if (pObject->CastMe(&pKey))
        {
            return 16;
        }
        else if (pObject->CastMe(&pGoal))
        {
            return 8;
        }
        else
        {
            return 0;
        }
    }
}
