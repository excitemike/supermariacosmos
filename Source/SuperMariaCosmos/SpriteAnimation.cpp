#include "SuperMariaCosmos/SpriteAnimation.h"
#include "Utilities/TextureManager.h"
#include "Framework/math.h"

namespace SuperMariaCosmos
{
    // set up defaults
    SpriteAnimation::SpriteAnimation() : 
        mCurrentTime(0.f),
        mSecondsPerFrame(0.f),
        mNumFrames(0),
        mFrames(NULL),
        mLoop(false)
    {
    }

    // set up an animation
    // texture_files should point to an array of numFrames char* s
    void SpriteAnimation::Init(
        float secondsPerFrame, 
        unsigned numFrames, 
        const char **texture_files, 
        bool loop)
    {
        mCurrentTime = 0.f;
        mSecondsPerFrame = secondsPerFrame;
        mNumFrames = numFrames;
        mLoop = loop;
        mFrames = new const Utilities::Texture*[numFrames];
        for (unsigned i=0; i<mNumFrames; ++i)
        {
            mFrames[i] = Utilities::TextureManager::GetTexture(texture_files[i]);
        }
    }

    // cleanup 
    void SpriteAnimation::Shutdown()
    {
        if (NULL != mFrames)
        {
            delete [] mFrames;
            mFrames = NULL;
        }
    }

    // advance the play head
    void SpriteAnimation::Update(float elapsedTime)
    {
        mCurrentTime += elapsedTime;

        if (mLoop && (mCurrentTime >= (mSecondsPerFrame*mNumFrames)))
        {
            mCurrentTime = 0.f;
        }
    }

    // figure out which texture goes with where we are in the animation
    const Utilities::Texture * SpriteAnimation::GetCurrentFrame()
    {
        if (NULL != mFrames)
        {
            unsigned currentFrame = (unsigned)(mCurrentTime / mSecondsPerFrame);
            currentFrame = Clamp(currentFrame, 0u, mNumFrames-1);

            return mFrames[currentFrame];
        }
        else
        {
            return NULL;
        }
    }

    // restart the animation
    void SpriteAnimation::Restart()
    {
        mCurrentTime = 0.f;
    }
}









