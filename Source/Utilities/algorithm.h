// STL, you are SO CLOSE to being something I could love
#ifndef UTILITIES_ALGORITHM_H
#define UTILITIES_ALGORITHM_H

#include <algorithm>
#include <functional>

namespace Utilities
{
    // forward declarations
    template <typename collection_type, typename function1>
    inline void for_each(collection_type collection, function1 func);

    template <typename collection_type, typename function2, typename argument_type>
    inline void for_each(collection_type collection, function2 func, argument_type arg);

    template<typename _this_type, typename _result_type>
    class delegate0;

    template<typename _this_type, typename _argument_type, typename _result_type>
    class delegate1;

    template<typename _this_type, typename _first_argument_type, typename _second_argument_type, typename _result_type>
    class delegate2;


    // for each item in collection, call func(item)
    template <typename collection_type, typename function1>
    inline void for_each(collection_type collection, function1 func)
    {
        std::for_each(collection.begin(), collection.end(), func);
    }

    // for each item in collection, call func(item, arg)
    template <typename collection_type, typename function2, typename argument_type>
    inline void for_each(collection_type collection, function2 func, argument_type arg)
    {
    	for_each(
    		collection,
    		std::bind2nd(std::ptr_fun(func), arg) );

    	/*
        collection_type :: iterator it = collection.begin();
        for (; it != collection.end(); ++it)
        {
            func(*it, arg);
        }*/
    }

    // make a functor for a member function
    template<typename _this_type, typename _result_type>
    class delegate0
    {
    public:
        typedef _result_type result_type;
        typedef result_type (_this_type::*member_fun_type)();

        delegate0(_this_type *pThis, member_fun_type func)
        {
            mThis = pThis;
            mFunc = func;
        }
        result_type operator()()
        {
            return (mThis->*mFunc)();
        }
    private:
        _this_type* mThis;
        member_fun_type mFunc;
    };

    // make a functor for a member function
    template<typename _this_type, typename _argument_type, typename _result_type>
    class delegate1
    {
    public:
        typedef _argument_type argument_type;
        typedef _result_type result_type;
        typedef result_type (_this_type::*member_fun_type)(argument_type arg);

        delegate1(_this_type *pThis, member_fun_type func)
        {
            mThis = pThis;
            mFunc = func;
        }
        result_type operator()(argument_type arg)
        {
            return (mThis->*mFunc)(arg);
        }
    private:
        _this_type* mThis;
        member_fun_type mFunc;
    };

    // make a functor for a member function
    template<typename _this_type, typename _first_argument_type, typename _second_argument_type, typename _result_type>
    class delegate2
    {
    public:
        typedef _first_argument_type first_argument_type;
        typedef _second_argument_type second_argument_type;
        typedef _result_type result_type;
        typedef result_type (_this_type::*member_fun_type)(first_argument_type first, _second_argument_type second);

        delegate2(_this_type *pThis, member_fun_type func)
        {
            mThis = pThis;
            mFunc = func;
        }
        result_type operator()(first_argument_type first, second_argument_type second)
        {
            return (mThis->*mFunc)(first, second);
        }
    private:
        _this_type* mThis;
        member_fun_type mFunc;
    };

}

#endif // UTILITIES_ALGORITHM_H

