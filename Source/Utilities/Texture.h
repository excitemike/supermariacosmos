#ifndef UTILITIES_TEXTURE_H
#define UTILITIES_TEXTURE_H


namespace Utilities
{
    class Texture
    {
    public:
        Texture(unsigned openGLTextureId, float width, float height, float xMax=1.f, float yMax=1.f);

        unsigned textureId;

        // size in pixels
        float width;
        float height;

        // texture coords
        float xMin;
        float xMax;
        float yMin;
        float yMax;
    };
}

#endif // UTILITIES_TEXTURE_H

