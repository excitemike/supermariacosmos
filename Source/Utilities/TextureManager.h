/*
    utility class for loading and keeping track of textures
*/
#ifndef UTILITIES_TEXTUREMANAGER_H
#define UTILITIES_TEXTUREMANAGER_H

#include "Utilities/Texture.h"
#include <map>
#include <string>

// forward declarations
struct SDL_Surface;
struct SDL_Rect;

namespace Utilities
{
    class TextureManager
    {
    public:
        // get the Texture for the specified texture file, loading the image first if necessary.
        static const Texture* GetTexture(const char* filename);

        // check to see if we have alrady loaded a texture
        static bool TextureAlreadyLoaded(const char* filename);

        // TODO: soon start refcounting textures, and then objects will have to free them
        //static void FreeTexture(Texture* texture);

        static void FreeAllTextures();
    protected:

        // do the pixel swapping to flip the image in an SDL_surface
        // CHANGES THE SURFACE IN PLACE
        static void FlipImageVertically(SDL_Surface *surface);

        // returns the OpenGLTextureId of the loaded texture, or INVALIDTEXTUREID
        // if it is not yet loaded
        static const Texture* GetAlreadyLoadedTexture(const char* filename);

        // load a texture file and create a texture object
        static const Texture* LoadTexture(const char * filename);

        // create an openGl texture from an SDL surface
        static Texture* SdlSurfaceToOpenGLTexture(SDL_Surface *surface);

        // create an openGl texture from an SDL surface
        static int GetOpenGLTextureFormatOfSdlSurface(const SDL_Surface *surface);

        // takes a surface and makes a new one that has power-of-2 dimensions
        // expect YOU to free the new surface.
        // Probably only works with 32-bit color
        static SDL_Surface * ResizeToPowerOf2(SDL_Surface *surface);

        // Resize surface A and put it in image B, resizing UP as necessary
        // if destination is smaller, things are going to suck
        // Probably only works with 32-bit color
        static void ZoomSurface(const SDL_Surface * source, SDL_Surface * destination);

        // do a bilinear interpolation to get the color at a specified point on the surface
        // Probably only works with 32-bit color
        static unsigned GetColorOnSurface(const SDL_Surface *surface, float x, float y);

        // get the color at a specified point on the surface with no interpolation
        // Probably only works with 32-bit color
        static unsigned GetColorOnSurface(const SDL_Surface *surface, int x, int y);

        // set the color of a pixel on the surface
        // Probably only works with 32-bit color
        static void WritePixelToSurface(SDL_Surface *destination, int x, int y, unsigned color);

        // helper function for CopySurfaceData. return 0 if any part of rect overlaps the surface. -1 otherwise
        static int ClipRectToSurface(SDL_Surface *surface, SDL_Rect *rect);

        // copy pixel data out of one surface into another.  Unlike blitting,
        // this will copy in the source's alpha value instead of alpha blending
        static int CopySurfaceData(
            SDL_Surface *src, SDL_Rect *srcrect,
			SDL_Surface *dst, SDL_Rect *dstrect);

        // get a pointer to the specified pixel of a surface
        static unsigned* GetPixel(SDL_Surface *surface, int x, int y);
    private:
        static std::map<std::string, Texture*> mLoadedTextures;
    };
}

#endif // UTILITIES_TEXTUREMANAGER_H
