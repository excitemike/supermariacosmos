#include "Utilities/Texture.h"


namespace Utilities
{
    Texture::Texture(unsigned openGLTextureId, float width, float height, float xMax, float yMax) :
        textureId(openGLTextureId), width(width), height(height), xMin(0.f), xMax(xMax), yMin(0.f), yMax(yMax)
    {
    }
}

