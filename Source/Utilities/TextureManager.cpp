/*
utility class for loading and keeping track of textures
*/
#include "TextureManager.h"
#include "SDL/include/SDL.h"
#include "SDL/include/SDL_opengl.h"
#include "SDL/include/SDL_image.h"
#include "SDL/include/SDL_video.h"
#include "Framework/math.h"
#include <assert.h>

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

// neat trick to do it fastlike
unsigned next_power_of_2(unsigned x)
{
    x -= 1;
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);
    return x+1;
}

namespace Utilities
{
    // static instance of texture collection
    std::map<std::string, Texture*> TextureManager::mLoadedTextures;

    // get the Texture for the specified texture file, loading the image first if necessary.
    const Texture* TextureManager::GetTexture(const char * filename)
    {
        // if we have it already loaded, we can just return that texture id
        const Texture* texture = GetAlreadyLoadedTexture(filename);
        if (NULL != texture)
        {
            return texture;
        }
        else
        {
            return LoadTexture(filename);
        }
    }

    // load a texture file and create a texture object
    const Texture* TextureManager::LoadTexture(const char * filename)
    {
        SDL_Surface *surface = IMG_Load(filename);

        // see if it failed to load
        assert(NULL != surface);
        if (NULL == surface) {return NULL;}

        Texture *texture = SdlSurfaceToOpenGLTexture(surface);
        SDL_FreeSurface(surface);

        // finally done loading, insert into collection
        mLoadedTextures[filename] = texture;

        return texture;
    }

    void TextureManager::FreeAllTextures()
    {
        std::map<std::string, Texture *>::iterator iterator;
        for (iterator = mLoadedTextures.begin(); iterator != mLoadedTextures.end(); ++iterator)
        {
            glDeleteTextures(1, &(iterator->second->textureId));
            delete iterator->second;
        }

        mLoadedTextures.clear();
    }

    // create an openGl texture from an SDL surface
    Texture* TextureManager::SdlSurfaceToOpenGLTexture(SDL_Surface *original_surface)
    {
        // SDL and OpenGl have different ideas of which corner is image
        // coordinate (0,0), so we have to flip the image before sending it to
        // OpenGL
        FlipImageVertically(original_surface);

        // switch to a converted texture if this one ain't powers-of-2 compliant
        SDL_Surface *converted_surface = ResizeToPowerOf2(original_surface);

        // create one texture name
        GLuint textureid;
        glGenTextures(1, &textureid);

        // tell opengl to use the generated texture name
        glBindTexture(GL_TEXTURE_2D, textureid);

        int textureFormat = GetOpenGLTextureFormatOfSdlSurface(original_surface);

        // this reads from the sdl surface and puts it into an opengl texture
        {
            SDL_Surface *surface = (NULL != converted_surface) ? converted_surface : original_surface;
            glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, surface->w, surface->h, 0, textureFormat, GL_UNSIGNED_BYTE, surface->pixels);
        }

        // these affect how this texture is drawn later on...
        // for this particular project, we want nearest neighbor for retro look
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

        // create the texture object
        float xMax = 1.f;
        float yMax = 1.f;
        if (NULL != converted_surface)
        {
            xMax = (float)(original_surface->w - 1) / (float)(converted_surface->w - 1);
            yMax = (float)(original_surface->h - 1) / (float)(converted_surface->h - 1);
        }
        Texture *texture = new Texture(textureid, (float)original_surface->w, (float)original_surface->h, xMax, yMax);

        // clean up
        if (NULL != converted_surface)
        {
            SDL_FreeSurface(converted_surface);
        }

        return texture;
    }


    // get openGL texture format value matching an SDL surface
    int TextureManager::GetOpenGLTextureFormatOfSdlSurface(const SDL_Surface *surface)
    {
        int textureFormat;

        // work out what format to tell glTexImage2D to use...
        if (surface->format->BytesPerPixel == 3) // RGB 24bit
        {
            textureFormat = GL_RGB;
        }
        else if (surface->format->BytesPerPixel == 4) // RGBA 32bit
        {
            textureFormat = GL_RGBA;
        }
        else
        {
            assert(false);
            return 0;
        }

        return textureFormat;
    }

    // do the pixel swapping to flip the image in an SDL_surface
    // CHANGES THE SURFACE IN PLACE
    void TextureManager::FlipImageVertically(SDL_Surface *surface)
    {
        unsigned bytesPerRow = surface->pitch;
        unsigned numRows = surface->h;
        unsigned char *pTempRow = new unsigned char[bytesPerRow];

        // loop through the rows, swapping them to flip the image
        for (unsigned i=0; i<(numRows)/2; ++i)
        {
            unsigned char *pRowFromTopHalf = &((unsigned char*)surface->pixels)[ i *bytesPerRow ];
            unsigned char *pRowFromBottomHalf = &((unsigned char*)surface->pixels)[ ((numRows-1)-i) *bytesPerRow ];

            // store row top half in temp
            memcpy(pTempRow, pRowFromTopHalf, bytesPerRow);
            // copy row from bottom half to ro win top half
            memcpy(pRowFromTopHalf, pRowFromBottomHalf, bytesPerRow);
            // copy temp into row from bottom half, and bam we've swapped them!
            memcpy(pRowFromBottomHalf, pTempRow, bytesPerRow);
        }
        delete [] pTempRow;
    }

    // check to see if we have alrady loaded a texture
    bool TextureManager::TextureAlreadyLoaded(const char* filename)
    {
        return (mLoadedTextures.end() != mLoadedTextures.find(filename));
    }

    // returns the OpenGLTextureId of the loaded texture, or INVALIDTEXTUREID
    // if it is not yet loaded
    const Texture* TextureManager::GetAlreadyLoadedTexture(const char* filename)
    {
        std::map<std::string, Texture*>::iterator iterator = mLoadedTextures.find(filename);

        // if it was not found return invalid
        if (mLoadedTextures.end() == iterator)
        {
            return NULL;
        }
        else
        {
            // it was found, return the id.
            // (for stl map iterators, the key is iterator->first,
            // and the value is iterator->second)
            return iterator->second;
        }
    }

    // takes a surface and makes a new one that has power-of-2 dimensions
    // expect YOU to free the new surface.
    // returns NULL if the image is already power of 2
    // Probably only works with 32-bit color
    SDL_Surface * TextureManager::ResizeToPowerOf2(SDL_Surface *original_surface)
    {
        // find new dimensions
        int converted_width  = (int)next_power_of_2((unsigned)original_surface->w);
        int converted_height = (int)next_power_of_2((unsigned)original_surface->h);

        SDL_Surface *converted_surface = NULL;

        // early out if it is already pow2
        if ( (converted_width == original_surface->w) && (converted_height == original_surface->h) )
        {
        }
        else
        {
            // make new surface
            converted_surface = SDL_CreateRGBSurface(
                SDL_SWSURFACE,
                converted_width, converted_height,
                original_surface->format->BitsPerPixel,
                original_surface->format->Rmask,
                original_surface->format->Gmask,
                original_surface->format->Bmask,
                original_surface->format->Amask);

            // Put empty space around the original to get a power of 2 texture
            SDL_FillRect(converted_surface, NULL, 0x00000000);
            CopySurfaceData(original_surface, NULL, converted_surface, NULL);

            // copy over an additional row and column so that bilinear filerig doesn't screw up the edges
            // copy that last column
                SDL_Rect sourceRect;
                sourceRect.x = (Sint16) original_surface->w - 1;
                sourceRect.y = 0;
                sourceRect.w = 1;
                sourceRect.h = (Uint16) original_surface->h;
                SDL_Rect targetRect;
                targetRect.x = (Sint16) original_surface->w;
                targetRect.y = 0;
                targetRect.w = 1;
                targetRect.h = (Uint16) original_surface->h;
                CopySurfaceData(original_surface, &sourceRect, converted_surface, &targetRect);
            // dupe that last row
                sourceRect.x = 0;
                sourceRect.y = (Sint16) original_surface->h - 1;
                sourceRect.w = (Uint16) original_surface->w;
                sourceRect.h = 1;
                targetRect.x = 0;
                targetRect.y = (Sint16) original_surface->h;
                targetRect.w = (Uint16) original_surface->w;
                targetRect.h = 1;
                CopySurfaceData(original_surface, &sourceRect, converted_surface, &targetRect);
            // and the corner
                sourceRect.x = (Sint16) original_surface->w - 1;
                sourceRect.y = (Sint16) original_surface->h - 1;
                sourceRect.w = 1;
                sourceRect.h = 1;
                targetRect.x = (Sint16) original_surface->w;
                targetRect.y = (Sint16) original_surface->h;
                targetRect.w = 1;
                targetRect.h = 1;
                CopySurfaceData(original_surface, &sourceRect, converted_surface, &targetRect);
        }

        return converted_surface;
    }

    // Resize surface A and put it in image B, resizing UP as necessary
    // if destination is smaller, things are going to suck
    // Probably only works with 32-bit color
    void TextureManager::ZoomSurface(const SDL_Surface * source, SDL_Surface * destination)
    {
        assert(destination->w >= source->w);
        assert(destination->h >= source->h);
        assert(destination->format->BitsPerPixel == source->format->BitsPerPixel );

        for (int y=0; y<destination->h; ++y)
        {
            for (int x=0; x<destination->w; ++x)
            {
                float source_x = ReMap((float)x, 0.f, (float)destination->w-1.f, 0.f, (float)source->w-1.f);
                float source_y = ReMap((float)y, 0.f, (float)destination->h-1.f, 0.f, (float)source->h-1.f);
                WritePixelToSurface(destination, x, y, GetColorOnSurface(source, source_x, source_y));
            }
        }
    }

    // do a bilinear interpolation to get the color at a specified point on the surface
    // Probably only works with 32-bit color
    unsigned TextureManager::GetColorOnSurface(const SDL_Surface *surface, float x, float y)
    {
        x = Clamp(x, 0.f, (float)surface->w);
        y = Clamp(y, 0.f, (float)surface->h);

        int source_x_whole = (int)x;
        int source_y_whole = (int)y;
        float source_x_fractional = x - floorf(x);
        float source_y_fractional = y - floorf(y);
        float left_weight  = 1.f - source_x_fractional;
        float right_weight = source_x_fractional;
        float bottom_weight = 1.f - source_y_fractional;
        float top_weight    = source_y_fractional;

        SDL_PixelFormat *format = surface->format;

        // starting in bottom left, going counter clockwise
        float weights[4] = {
            left_weight*bottom_weight,
            right_weight*bottom_weight,
            right_weight*top_weight,
            left_weight*top_weight};
        int source_pixel_x[4] = {
            source_x_whole,
            source_x_whole+1,
            source_x_whole+1,
            source_x_whole};
        int source_pixel_y[4] = {
            source_y_whole,
            source_y_whole,
            source_y_whole+1,
            source_y_whole+1};

        float fR = 0.f;
        float fG = 0.f;
        float fB = 0.f;
        float fA = 0.f;
        for (int i=0; i<4; ++i)
        {
            Uint32 color = GetColorOnSurface(surface, source_pixel_x[i], source_pixel_y[i]);
            fR += (float)((color & format->Rmask) >> format->Rshift) * weights[i];
            fG += (float)((color & format->Gmask) >> format->Gshift) * weights[i];
            fB += (float)((color & format->Bmask) >> format->Bshift) * weights[i];
            fA += (float)((color & format->Amask) >> format->Ashift) * weights[i];
        }

        Uint32 finalColor =
              ( ((Uint8)fR & 0xFF) << format->Rshift)
            + ( ((Uint8)fG & 0xFF) << format->Gshift)
            + ( ((Uint8)fB & 0xFF) << format->Bshift)
            + ( ((Uint8)fA & 0xFF) << format->Ashift);
        return finalColor;
    }

    // get the color at a specified point on the surface with no interpolation
    // Probably only works with 32-bit color
    unsigned TextureManager::GetColorOnSurface(const SDL_Surface *surface, int x, int y)
    {
        x = Clamp(x, 0, surface->w);
        y = Clamp(y, 0, surface->h);

        Uint8* pSource = (Uint8*)surface->pixels;

        pSource += y * surface->pitch;
        pSource += x * surface->format->BytesPerPixel;

        unsigned color = 0;

        switch(surface->format->BytesPerPixel)
        {
        case 4:
            {
                Uint32 *pData = (Uint32*)pSource;
                color = *pData;
            }
            break;
        default:
            assert(false && "unsupported surface format!");
            break;
        }

        return color;
    }

    // get a pointer to the specified pixel of a surface
    unsigned* TextureManager::GetPixel(SDL_Surface *surface, int x, int y)
    {
        x = Clamp(x, 0, surface->w);
        y = Clamp(y, 0, surface->h);

        Uint8* pData = (Uint8*)surface->pixels;

        pData += y * surface->pitch;
        pData += x * surface->format->BytesPerPixel;

        Uint32 *pPixels = (Uint32*)pData;
        return pPixels;
    }

    // set the color of a pixel on the surface
    // Probably only works with 32-bit color
    void TextureManager::WritePixelToSurface(SDL_Surface *destination, int x, int y, unsigned color)
    {
        *GetPixel(destination,x,y) = color;
    }

    // helper function for CopySurfaceData. return 0 if any part of rect overlaps the surface. -1 otherwise
    int TextureManager::ClipRectToSurface(SDL_Surface *surface, SDL_Rect *rect)
    {
        // check for some early outs
        if (surface->w <= rect->x) {return -1;}
        if (surface->h <= rect->y) {return -1;}
        if ((rect->x + rect->w) <= 0) {return -1;}
        if ((rect->y + rect->h) <= 0) {return -1;}

        Sint16 xmax = rect->x + rect->w;
        Sint16 ymax = rect->y + rect->h;
        rect->x = max(rect->x, (Sint16)0);
        rect->y = max(rect->y, (Sint16)0);
        rect->w = (Uint16) min(xmax, surface->w - rect->x);
        rect->h = (Uint16) min(ymax, surface->h - rect->y);

        return 0;
    }

    // copy pixel data out of one surface into another.  Unlike blitting,
    // this will copy in the source's alpha value instead of alpha blending
    // but otherwise it mimics SDL_BlitSurface
    int TextureManager::CopySurfaceData(
        SDL_Surface *src, SDL_Rect *srcrect,
		SDL_Surface *dst, SDL_Rect *dstrect)
    {
        // might get shrunk further below based on the rects
        int src_start_x = 0;
        int src_start_y = 0;
        int dst_start_x = 0;
        int dst_start_y = 0;
        int copy_width  = min(src->w, dst->w);
        int copy_height = min(src->h, dst->h);

        if (NULL != srcrect)
        {
            int errorcode = 0;
            errorcode = ClipRectToSurface(src, srcrect);
            if (0 != errorcode) {return errorcode;}

            copy_width  = min(copy_width, srcrect->w);
            copy_height = min(copy_height, srcrect->h);

            src_start_x = srcrect->x;
            src_start_y = srcrect->y;
        }

        if (NULL != dstrect)
        {
            int errorcode = ClipRectToSurface(dst, dstrect);
            if (0 != errorcode) {return errorcode;}

            copy_width  = min(copy_width, dstrect->w);
            copy_height = min(copy_height, dstrect->h);

            dst_start_x = dstrect->x;
            dst_start_y = dstrect->y;
        }

        // copy as much of each row as will go
        for (int y=0; y<copy_height; ++y)
        {
            memcpy(
                GetPixel(dst, dst_start_x, dst_start_y+y),
                GetPixel(src, src_start_x, src_start_y+y),
                copy_width * sizeof(Uint32) );
        }

        return 0;
    }

    /*TODO:
    // free the texture
    void Sprite::FreeTexture(unsigned textureID)
    {
    glDeleteTextures(1, &mTextureId );
    mTextureId = 0;
    }
    */
}
