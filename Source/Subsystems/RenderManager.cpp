// Subsystem to handle rendering using OpenGL
#include "RenderManager.h"
#include "SDL/include/SDL.h"
#include "SDL/include/SDL_opengl.h"
#include "Framework/math.h"

// fallback texture
static const Utilities::Texture DEFAULT_TEXTURE(0, 0.f, 0.f);

// tells how many slices to break arcs into when drawing them
static const unsigned ARC_SEGMENTS = 32;

namespace Subsystems
{
    RenderManager::Color RenderManager::WHITE = {1.f,1.f,1.f,1.f};
    RenderManager::Color RenderManager::INVISIBLE = {1.f,1.f,1.f,0.f};

    RenderManager::RenderManager()
        : mSpriteDisplayList(0)
    {
    }

    void RenderManager::Init()
    {
        // setup open gl for how we want to render
        SetupViewportAndProjection();
        SetOpenGLOptions();
    }

    // set all the options for the way we want to render
    void RenderManager::SetOpenGLOptions()
    {
        // turn on some OpenGL options
        //glEnable(GL_DEPTH_TEST); // TODO: turn on when we go 3d again
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        // Enable lighting
        // TODO: turn on lighting when we go 3d again
        //glEnable(GL_LIGHTING);

        // Set up light 0
        // TODO: turn on lighting when we go 3d again
        //glEnable(GL_LIGHT0);

        // enable texturing with alpha blending
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // temporary lighting
        // TODO: for-real lights
        GLfloat lightpos[] = { 0.f, 100.f, 0.f, 1.f};
        GLfloat lightcolor[] = { 0.f, 1.f, 0.f, 1.f};
        glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightcolor);
        GLfloat ambient[] = {0.5f, 0.5f, 0.5f, 1.0f };
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient); // set ambient light for the scene
        glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // for proper specular calculations
        //glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR); // apply specular AFTER the texture

        // set up a material for objects to use
        GLfloat materialspecular[]  = {1.f, 1.f, 1.f, 1.f };
        GLfloat materialshininess[] = { 128.0 };
        GLfloat materialambient[] = {1.f, 1.f, 1.f, 1.f };
        glMaterialfv( GL_FRONT, GL_SPECULAR,  materialspecular);
        glMaterialfv( GL_FRONT, GL_SHININESS, materialshininess);
        glMaterialfv( GL_FRONT, GL_AMBIENT, materialambient);
    }

    void RenderManager::Shutdown()
    {
    }

    void RenderManager::BeginScene()
    {
        /* Clear the color plane and the z-buffer */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    // slap a texture onscreen
    void RenderManager::RenderSprite(
        const Utilities::Texture *texture,
        float x,
        float y,
        float width,
        float height,
        bool flip_horizontal,
        bool flip_vertical,
        float rotation_in_radians,
        float offset_to_center_x,
        float offset_to_center_y)
    {
        if (NULL == texture)
        {
            texture = &DEFAULT_TEXTURE;
        }

        glBindTexture(GL_TEXTURE_2D, texture->textureId);

        glPushMatrix();

        // move to correct location
        glTranslatef(x, y, 0.f);

        // rorate around the center
        glTranslatef(offset_to_center_x, offset_to_center_y, 0.f);
        glRotatef(rotation_in_radians * DEGREES_PER_RADIAN, 0.f, 0.f, 1.f);
        glTranslatef(-offset_to_center_x, -offset_to_center_y, 0.f);

        // figuring out texture coords in case of flipping
        float xMin = texture->xMin;
        float xMax = texture->xMax;
        float yMin = texture->yMin;
        float yMax = texture->yMax;
        float tex_left  = flip_horizontal ? xMax : xMin;
        float tex_right = flip_horizontal ? xMin : xMax;
        float tex_bottom = flip_vertical ? yMax : yMin;
        float tex_top    = flip_vertical ? yMin : yMax;

        // set to correct size
        glScalef(width, height, 1.f);

        // tell OpenGL we are staring to render
        glBegin(GL_QUADS);

        // bottom left
        glTexCoord2f(tex_left, tex_bottom);
        glVertex2f(0.f, 0.f);

        // bottom right
        glTexCoord2f(tex_right, tex_bottom);
        glVertex2f(1.f, 0.f);

        // top right
        glTexCoord2f(tex_right, tex_top);
        glVertex2f(1.f, 1.f);

        // top left
        glTexCoord2f(tex_left, tex_top);
        glVertex2f(0.f, 1.f);

        glEnd();

        glPopMatrix();
    }

    // draw an arc.  COUNTER-CLOCKWISE from start angle to end angle.
    // angles in radians, measuring counter-clockwise from straight right.
    void RenderManager::RenderArc(
        float center_x,
        float center_y,
        float angle_start,
        float angle_end,
        float outer_radius,
        float inner_radius,
        Color outer_color,
        Color inner_color)
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glPushMatrix();

        // move to correct location
        float start_angle_in_degrees = angle_start * DEGREES_PER_RADIAN;
        glTranslatef(center_x, center_y, 0.f);
        glRotatef(start_angle_in_degrees, 0.f, 0.f, 1.f);

        // tell OpenGL we are staring to render
        glBegin(GL_QUADS);

        // make sure we are going counter clockwise, and not going around more than once
        float anglediff = (angle_end - angle_start);
        if (anglediff < 0.f) { anglediff += (2.f*PI); }
        anglediff = fmodf(anglediff, 2.f*PI);

        float angle_step = anglediff / ARC_SEGMENTS;

        float inner_x = inner_radius;
        float inner_y = 0.f;
        float outer_x = outer_radius;
        float outer_y = 0.f;
        float angle = 0.f;

        for (unsigned i=0; i<ARC_SEGMENTS; ++i)
        {
            // bottom left
            glColor4f(inner_color.r, inner_color.g, inner_color.b, inner_color.a);
            glVertex2f(inner_x, inner_y);

            // bottom right
            glColor4f(outer_color.r, outer_color.g, outer_color.b, outer_color.a);
            glVertex2f(outer_x, outer_y);

            // next step
            angle += angle_step;
            inner_x = inner_radius * cosf(angle);
            inner_y = inner_radius * sinf(angle);
            outer_x = outer_radius * cosf(angle);
            outer_y = outer_radius * sinf(angle);

            // top right
            // (still using outer color)
            glVertex2f(outer_x, outer_y);

            // top left
            glColor4f(inner_color.r, inner_color.g, inner_color.b, inner_color.a);
            glVertex2f(inner_x, inner_y);
        }

        glEnd();

        glPopMatrix();

        // reset the color
        glColor4fv(WHITE.array);
    }

    void RenderManager::RenderCircle(
            float center_x,
            float center_y,
            float radius,
            Color outer_color,
            Color inner_color)
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glPushMatrix();

        // move to starting location
        glTranslatef(center_x, center_y, 0.f);

        // tell OpenGL we are staring to render
        glBegin(GL_TRIANGLE_FAN);

        float angle_step = 2.f*PI / ARC_SEGMENTS;

        // center (this is a triangle fan, so we only have to make the one center dot use this color)
        glColor4f(inner_color.r, inner_color.g, inner_color.b, inner_color.a);
        glVertex2f(0.f, 0.f);

        // set outside color
        glColor4f(outer_color.r, outer_color.g, outer_color.b, outer_color.a);

        float angle = 0.f;
        float outer_x = radius;
        float outer_y = 0.f;
        glVertex2f(outer_x, outer_y);

        for (unsigned i=0; i<ARC_SEGMENTS; ++i)
        {
            // next step
            angle += angle_step;
            outer_x = radius * cosf(angle);
            outer_y = radius * sinf(angle);

            // (still using outer color)
            glVertex2f(outer_x, outer_y);
        }

        glEnd();

        glPopMatrix();

        // reset the color
        glColor4fv(WHITE.array);
    }
    void RenderManager::RenderRect(
        float left, float right,
        float bottom, float top,
        Color outer_color,
        Color inner_color
        )
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glPushMatrix();

        // move to starting location
        glTranslatef(left, bottom, 0.f);

        float width = (right-left);
        float height = (top-bottom);
        float center_x = width * 0.5f;
        float center_y = height * 0.5f;

        // tell OpenGL we are staring to render
        glBegin(GL_TRIANGLE_FAN);

        // center (this is a triangle fan, so we only have to make the one center dot use this color)
        glColor4f(inner_color.r, inner_color.g, inner_color.b, inner_color.a);
        glVertex2f(center_x, center_y);

        // outside edge
        glColor4f(outer_color.r, outer_color.g, outer_color.b, outer_color.a);
        glVertex2f(width, 0.f);
        glVertex2f(width, height);
        glVertex2f(0.f, height);
        glVertex2f(0.f, 0.f);
        glVertex2f(width, 0.f);

        glEnd();

        glPopMatrix();

        // reset the color
        glColor4fv(WHITE.array);
    }

    // draw a rectangle
    void RenderManager::RenderQuad(
        float left, float right,
        float bottom, float top,
        Color color
        )
    {
        glBindTexture(GL_TEXTURE_2D, 0);

        // tell OpenGL we are staring to render
        glBegin(GL_QUADS);

        glColor4fv(color.array);

        glVertex2f( left, bottom);
        glVertex2f(right, bottom);
        glVertex2f(right, top);
        glVertex2f( left, top);

        glEnd();

        // reset the color
        glColor4fv(WHITE.array);
    }


    void RenderManager::EndScene()
    {
        // flip the buffers so that it shows up
        glFlush();
        SDL_GL_SwapBuffers();
    }

    void RenderManager::SetupViewportAndProjection()
    {
        //todo: hardcoded viewport size! bad!
        UpdateViewport(0, 0, 1024, 512);
    }

    void RenderManager::UpdateViewport(int x, int y, int width, int height)
    {
        // Make the viewport cover the whole window
        glViewport(x, y, width, height);

        // Set the camera projection matrix:
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        // TODO: switch to gluPerspective when we go back to 3d
        //gluPerspective(fieldOfView_Y, aspect, nearClip, farClip);
        glOrtho(
            0, /*left*/
            width, /*right*/
            0, /*bottom*/
            height, /*top*/
            -1.0, /*near*/
            1.0 /*far*/ );

        // We're done with the camera, now matrix operations
        // will affect the modelview matrix
        glMatrixMode(GL_MODELVIEW);
    }

    // only works after Init()
    void RenderManager::SetBackgroundColor(float r, float g, float b)
    {
        glClearColor(r,g,b,1.f);
    }

    void RenderManager::PushRotation(float angle_in_radians)
    {
        glPushMatrix();
        glRotatef(angle_in_radians * DEGREES_PER_RADIAN, 0.f, 0.f, 1.f);
    }

    void RenderManager::PushScale(float scale_x, float scale_y)
    {
        glPushMatrix();
        glScalef(scale_x, scale_y, 1.f);
    }

    void RenderManager::PushTranslation(float x, float y)
    {
        glPushMatrix();
        glTranslatef(x, y, 0.f);
    }

    // rotate, then translate
    void RenderManager::PushRotateAndTranslation(float angle_in_radians, float x, float y)
    {
        glPushMatrix();
        glTranslatef(x, y, 0.f);
        glRotatef(angle_in_radians * DEGREES_PER_RADIAN, 0.f, 0.f, 1.f);
    }

    // scale, then rotate, then translate
    void RenderManager::PushTransformation(float scale, float angle_in_radians, float x, float y)
    {
        glPushMatrix();
        glTranslatef(x, y, 0.f);
        glRotatef(angle_in_radians * DEGREES_PER_RADIAN, 0.f, 0.f, 1.f);
        glScalef(scale, scale, scale);
    }

    void RenderManager::PopTransformation()
    {
        glPopMatrix();
    }
}
