/*
    keeps track of input for the game
*/
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include "Framework/BaseSubsystem.h"
#include "Framework/ButtonState.h"
#include "SDL/include/SDL.h"

#include <assert.h>

namespace Subsystems
{
    class InputManager : public Framework::BaseSubsystem
    {
    public:
        InputManager();
        void Init();
        void Update(float secondsSinceLastFrame);

        const Framework::ButtonState& GetLeftMouseButton() const {return mLeftMouseButton;}
        float GetMouseX() const {return mMouseX;}
        float GetMouseY() const {return mMouseY;}
        float GetMouseMoveX() const {return mMouseMoveX;}
        float GetMouseMoveY() const {return mMouseMoveY;}

        const Framework::ButtonState& GetKeyState(SDLKey keycode) const {assert(keycode < SDLK_LAST); return mKeyStates[keycode];}
    private:
        void UpdateMouse();
        void UpdateKeyboard();

        // set the mouse position, clear mouse move
        void ResetMouse(float x, float y);

        Framework::ButtonState mKeyStates[SDLK_LAST];

        Framework::ButtonState mLeftMouseButton;
        float mMouseX;
        float mMouseY;
        float mMouseMoveX;
        float mMouseMoveY;
    };
}

#endif // INPUTMANAGER_H

