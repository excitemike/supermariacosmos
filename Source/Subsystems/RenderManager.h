/*
    Subsystem to handle rendering using OpenGL
*/

#ifndef RENDERMANAGER_H
#define RENDERMANAGER_H

#include "Framework/BaseSubsystem.h"
#include "Utilities/Texture.h"

namespace Subsystems
{
    class RenderManager : public Framework::BaseSubsystem
    {
    public:
        union Color
        {
            struct
            {
                float r;
                float g;
                float b;
                float a;
            };
            float array[4];
        };
        static Color WHITE;
        static Color INVISIBLE;

        RenderManager();
        virtual void Init();
        virtual void Shutdown();

        void BeginScene();
        void EndScene();
        void RenderSprite(
            const Utilities::Texture *texture,
            float x, float y, float width, float height,
            bool flip_horizontal=false, bool flip_vertical=false,
            float rotation_in_radians = 0.f,
            float offset_to_center_x = 0.f,
            float offset_to_center_y = 0.f);
        void RenderArc(
            float center_x,
            float center_y,
            float angle_start,
            float angle_end,
            float outer_radius,
            float inner_radius = 0.f,
            Color outer_color = WHITE,
            Color inner_color = WHITE);
        void RenderCircle(
            float center_x,
            float center_y,
            float radius,
            Color outer_color = WHITE,
            Color inner_color = WHITE);
        void RenderQuad(
            float left, float right,
            float bottom, float top,
            Color color = WHITE
            );
        void RenderRect(
            float left, float right,
            float bottom, float top,
            Color outer_color = WHITE,
            Color inner_color = WHITE
            );

        void PushRotation(float angle_in_radians);
        void PushScale(float scale_x, float scale_y);
        void PushTranslation(float x, float y);
        void PopTransformation();

        // rotate, then translate
        void PushRotateAndTranslation(float angle_in_radians, float x, float y);

        // scale, then rotate, then translate
        void PushTransformation(float scale, float angle_in_radians, float x, float y);

        void UpdateViewport(int x, int y, int width, int height);

        // only works after Init()
        void SetBackgroundColor(float r, float g, float b);
    private:
        void SetupViewportAndProjection();
        void SetOpenGLOptions();

        unsigned mSpriteDisplayList;
    };
}

#endif // RENDERMANAGER_H
