/*
    interface for the GameWindow subsystem - handles creating and altering the
    window the game runs in.
*/
#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "Framework/BaseSubsystem.h"
#include <string>

namespace Subsystems
{
    class GameWindow : public Framework::BaseSubsystem
    {
    public:
        GameWindow();
        virtual void Init();
        virtual void Shutdown();

        void SetWindowSize(unsigned width, unsigned height);
        void SetCaption(const char* caption);
    private:
        std::string mCaption;
        unsigned mWidth;
        unsigned mHeight;
        bool mInitialized;
    };
}

#endif // GAMEWINDOW_H
