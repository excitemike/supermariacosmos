/*
    Particle Blow's Manager for UI widgets
*/
#ifndef WIDGETMANAGER_H
#define WIDGETMANAGER_H

#include "Framework/BaseSubsystem.h"
#include "SceneObjects/BaseWidget.h"
#include "SceneObjects/WidgetHolder.h"
#include "SceneManager.h"
#include "InputManager.h"

namespace Subsystems
{
    class WidgetManager : public Framework::BaseSubsystem, public SceneObjects::WidgetHolder
    {
    public:
        WidgetManager();

        virtual void SetSceneManager(SceneManager *pSceneManager){mpSceneManager = pSceneManager;}

        virtual void Update(float secondsSinceLastFrame);
        virtual void Shutdown();

        // overridden to add it to the scene manager while we are in here
        void AddWidget(SceneObjects::BaseWidget*);

        // overridden to remove it from the scene manager while we are in here
        void RemoveWidget(SceneObjects::BaseWidget*);

        // get the currently selected widget (or null if none selected)
        SceneObjects::BaseWidget* GetSelected();

        // set a widget as selected
        void Select(SceneObjects::BaseWidget* pWidget);

        // deselect the currently selected widget
        void Deselect();

    private:
        SceneManager * mpSceneManager;

        SceneObjects::BaseWidget* mSelectedWidget;

        unsigned mWidgetLayer;
    };
}

#endif // WIDGETMANAGER_H
