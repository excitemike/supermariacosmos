#include "Subsystems/Sound.h"
#include <assert.h>

namespace Subsystems
{
    bool Sound::sInstantiated = false;
    Sound::SoundData Sound::sSoundData[NUM_SOUNDS];
    SDL_AudioSpec Sound::sGameAudioFormat;

    Sound::Sound()
    {
        // having more than one instance of this isn't really going to work
        assert(!sInstantiated);
        if (!sInstantiated)
        {
            for (unsigned i=0; i<NUM_SOUNDS; ++i)
            {
                sSoundData[i].sound_loaded = false;
            }

            // set the game's audio format
            sGameAudioFormat.freq = 22050;
            sGameAudioFormat.format = AUDIO_S16;
            sGameAudioFormat.channels = 1;
            sGameAudioFormat.samples = 512;
            sGameAudioFormat.callback = Sound::MixAudio;
            sGameAudioFormat.userdata = NULL;

            sInstantiated = true;
        }
    }

    void Sound::Init()
    {
        // start up the SDL sound stuff
        int successcode = SDL_OpenAudio(&sGameAudioFormat, NULL);
        if (0 != successcode) {assert(false);}
        SDL_PauseAudio(0);
    }

    bool Sound::LoadSound(const char* filename, SoundSlot slot)
    {
        // make sure stuff gets freed if we already had a sound in that slot
        UnloadSound(slot);

        // load and get info on the sound
        SDL_AudioSpec loaded_audio_format;
        Uint8 *data;
        Uint32 data_length;
        SDL_AudioSpec *loadwavsuccess = SDL_LoadWAV(filename, &loaded_audio_format, &data, &data_length);
        if (NULL == loadwavsuccess) {assert(false); return false;}

        // get conversion info
        SDL_AudioCVT conversion_data;
        int successcode = SDL_BuildAudioCVT(
            &conversion_data,
            loaded_audio_format.format, loaded_audio_format.channels, loaded_audio_format.freq,
            sGameAudioFormat.format, sGameAudioFormat.channels, sGameAudioFormat.freq);
        if (-1 == successcode) {assert(false); return false;}

        // copy audio data over to conversion location
        conversion_data.buf = new Uint8[data_length * conversion_data.len_mult ];
        conversion_data.len = data_length;
        memcpy(conversion_data.buf, data, data_length);

        // we won't actually be using the memory SDL allocated
        SDL_FreeWAV(data);

        // do the conversion
        successcode = SDL_ConvertAudio(&conversion_data);
        if (-1 == successcode) {assert(false); return false;}

        // put the data where it goes
        SDL_LockAudio();
        sSoundData[slot].data = conversion_data.buf;
        sSoundData[slot].length = conversion_data.len_cvt;
        sSoundData[slot].position = 0;
        sSoundData[slot].playing = false;
        sSoundData[slot].sound_loaded = true;
        SDL_UnlockAudio();

        // invalid assumption that I am too tired to fix for reals
        return true;
    }

    void Sound::PlaySound(SoundSlot slot)
    {
        assert(sSoundData[slot].sound_loaded);
        if (sSoundData[slot].sound_loaded)
        {
            sSoundData[slot].position = 0;
            sSoundData[slot].playing = true;
        }
    }

    void Sound::Shutdown()
    {
        // get rid of all loaded sounds
        for (unsigned i=0; i<NUM_SOUNDS; ++i)
        {
            UnloadSound(SoundSlot(i));
        }

        SDL_CloseAudio();
    }

    void Sound::UnloadSound(SoundSlot slot)
    {
        if (sSoundData[slot].sound_loaded)
        {
            delete [] sSoundData[slot].data;
            sSoundData[slot].sound_loaded = false;
        }
    }

    //
    void Sound::MixAudio(void *userdata, Uint8 *stream, int len)
    {
	    // Clear the buffer so we aren't mixing with noise
	    memset(stream, 0, len);

        for (unsigned soundIndex = 0; soundIndex < NUM_SOUNDS; ++soundIndex)
        {
            SoundData* pSoundData = &(sSoundData[soundIndex]);

            if (pSoundData->sound_loaded && pSoundData->playing)
            {
                int amount = (pSoundData->length - pSoundData->position);
                if ( amount > len )
                {
                    amount = len;
                }

                SDL_MixAudio(stream, &(pSoundData->data[pSoundData->position]), amount, SDL_MIX_MAXVOLUME);
                pSoundData->position += amount;
            }
        }
    }
}
