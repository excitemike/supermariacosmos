/*
    Subsystem for handling the particles in ParticleBlow
*/

#ifndef PARTICLEMANAGER_H
#define PARTICLEMANAGER_H

#include "Framework/BaseSubsystem.h"
#include "SceneObjects/Particle.h"
#include "SceneManager.h"

namespace Subsystems
{
    class ParticleManager : public Framework::BaseSubsystem
    {
    public:
        ParticleManager();

        // CALL THIS BEFORE WE REACH INIT()
        void Config(
            SceneManager *pSceneManager,
            unsigned maxParticles,
            unsigned particleLayer,
            float maxParticleLifetimeSeconds,
            float minX,
            float maxX,
            float minY,
            float maxY);

        virtual void Init();
        virtual void Update(float secondsSinceLastFrame);
        virtual void Shutdown();
        virtual void SpawnParticle(float startingX, float startingY, float velocityX, float velocityY);

        // retrieve how many particles there are.  (highest_index_Getparticle_can_take = GetNumParticles() - 1)
        unsigned GetNumParticles() {return mMaxParticles;}
        SceneObjects::Particle* GetParticle(unsigned index) {return &(mParticles[index]);}

    private:
        void AgeParticle(unsigned particleIndex, float secondsSinceLastFrame);
        bool ParticleIsOutOfBounds(unsigned particleIndex);
        SceneObjects::Particle* GetFirstDeadParticle();
        SceneObjects::Particle* GetOldestParticle();

        SceneManager* mpSceneManager;
        unsigned mMaxParticles;
        float mMaxParticleLifetimeInSeconds;
        SceneObjects::Particle* mParticles;

        unsigned mParticleLayer;

        float mMinX;
        float mMaxX;
        float mMinY;
        float mMaxY;
    };
}


#endif // PARTICLEMANAGER_H
