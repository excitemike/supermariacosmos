/*
    keeps track of input for the game
*/
#include "InputManager.h"
#include "SDL/include/SDL_video.h"

unsigned _crash()
{
    unsigned a = *(unsigned*)0x12345678;
    return a;
}

namespace Subsystems
{
    InputManager::InputManager() :
        mLeftMouseButton(),
        mMouseX(0),
        mMouseY(0),
        mMouseMoveX(0),
        mMouseMoveY(0)
    {
    }

    void InputManager::Init()
    {
        // get mouse position
        UpdateMouse();

        UpdateKeyboard();

        // correct initial mousemove values
        mMouseMoveX = 0.f;
        mMouseMoveY = 0.f;
    }

    void InputManager::Update(float secondsSinceLastFrame)
    {
        UpdateMouse();
        UpdateKeyboard();
    }

    void InputManager::UpdateMouse()
    {
        int x;
        int y;
        unsigned sdlMouseButtonState = SDL_GetMouseState(&x, &y);

        float oldMouseX = mMouseX;
        float oldMouseY = mMouseY;

        mMouseX = (float)x;
        // in our system, the y axis is zero at the BOTTOM of the screen, but
        // in SDL it is at the top, so we need to flip it here
        int screenHeight = SDL_GetVideoInfo()->current_h;
        mMouseY = ((float)screenHeight - (float)y);

        mMouseMoveX = mMouseX - oldMouseX;
        mMouseMoveY = mMouseY - oldMouseY;

        mLeftMouseButton.Update( 0!=(sdlMouseButtonState & SDL_BUTTON(1)) );
    }

    void InputManager::UpdateKeyboard()
    {
        int numkeys;
        Uint8 *pData = SDL_GetKeyState(&numkeys);

        for (int keycode = 0; (keycode < numkeys)&&(keycode < SDLK_LAST); ++keycode)
        {
            mKeyStates[keycode].Update( 0!= pData[keycode] );
        }
    }

    // set the mouse position, clear mouse move
    void InputManager::ResetMouse(float x, float y)
    {
        mMouseX = x;
        mMouseY = y;
        mMouseMoveX = 0.f;
        mMouseMoveY = 0.f;
    }
}
