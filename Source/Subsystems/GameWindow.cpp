/*
    GameWindow subsystem
    handles creating and altering the window the game runs in.
*/

#include "GameWindow.h"
#include "SDL/include/SDL.h"
#include "SDL/include/SDL_image.h"

namespace Subsystems
{
    GameWindow::GameWindow()
        : mCaption("<caption here>"), mWidth(1024), mHeight(512), mInitialized(false)
    {
    }

    void GameWindow::Init()
    {
        // Setup OpenGL stuff
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );

        SDL_SetVideoMode( mWidth, mHeight, 0, SDL_OPENGL) ;
        SDL_WM_SetCaption( mCaption.c_str(), 0);
        SDL_WM_SetIcon(IMG_Load("Icon.bmp"), NULL);

        mInitialized = true;
    }

    void GameWindow::Shutdown()
    {
        mInitialized = false;
    }

    void GameWindow::SetWindowSize(unsigned width, unsigned height)
    {
        mWidth = width;
        mHeight = height;

        if (mInitialized)
        {
            SDL_SetVideoMode( mWidth, mHeight, 0, SDL_OPENGL) ;
        }
    }

    void GameWindow::SetCaption(const char* caption)
    {
        mCaption = caption;
        SDL_WM_SetCaption( mCaption.c_str(), 0);
    }
}
