#ifndef SUBSYSTEMS_SOUND_H
#define SUBSYSTEMS_SOUND_H

#include "Framework/BaseSubsystem.h"
#include "SDL/include/SDL_audio.h"

namespace Subsystems
{
    // subsystem for playing sounds.
    // because of the statics we need to play nice with SDL, 
    // you can't really use multiple instances of this class
    class Sound : public Framework::BaseSubsystem
    {
    public:
        // TODO:  get some kind of system (shared with TextureManager) for 
        // managing loaded assets of various kinds
        // because this is painful
        enum SoundSlot
        {
            SOUND_A = 0,
            SOUND_B,
            SOUND_C,
            SOUND_D,
            NUM_SOUNDS
        };

        Sound();

        virtual void Init();
        virtual void Shutdown();

        virtual bool LoadSound(const char* filename, SoundSlot slot);
        virtual void UnloadSound(SoundSlot slot);
        virtual void PlaySound(SoundSlot slot);
    protected:
        struct SoundData
        {
            Uint8 *data;
            Uint32 position;
            Uint32 length;
            bool sound_loaded;
            bool playing;
        };

        static void MixAudio(void *userdata, uint8_t *stream, int len);
    private:
        // shared across all instances of Sound
        static SoundData sSoundData[NUM_SOUNDS];
        
        static SDL_AudioSpec sGameAudioFormat;

        // so we can assert that there is only one instance ever
        static bool sInstantiated;
    };
}

#endif // SUBSYSTEMS_SOUND_H

