/*
    Subsystem to handle rendering using OpenGL
*/

#include "SceneManager.h"
#include <algorithm>
#include <assert.h>

namespace Subsystems
{
    SceneManager::SceneManager() : mpBackground(0), mActiveIterators(0)
    {
    }

    // don't leak memory
    SceneManager::~SceneManager()
    {
        if (0 != mpBackground)
        {
            delete mpBackground;
            mpBackground = 0;
        }
    }

    // render all the objects in the scene manager
    void SceneManager::RenderScene(RenderManager *pRenderer)
    {
        // render background first
        if (0 != mpBackground)
        {
            mpBackground->Render(pRenderer);
        }

        for (unsigned layer = 0; layer < MAX_LAYER_COUNT; ++layer)
        {
            RenderSceneLayer(pRenderer, layer);
        }
    }

    void SceneManager::RenderSceneLayer(RenderManager *pRenderer, unsigned layer)
    {
        AddActiveIterator();

        // then all the foreground sprites
        std::vector<SceneObjects::BaseSceneItem*>::iterator it;
        for (it = mSceneItems[layer].begin(); it != mSceneItems[layer].end(); ++it)
        {
            (*it)->Render(pRenderer);
        }

        RemoveActiveIterator();
    }


    // set an image to stretch across the whole screen for the background
    void SceneManager::SetBackgroundImage(const char* imageFile)
    {
        if (0 != mpBackground)
        {
            delete mpBackground;
            mpBackground = 0;
        }

        mpBackground = new SceneObjects::Sprite(imageFile, 1024, 512);
    }

    void SceneManager::AddObject(SceneObjects::BaseSceneItem *pBaseSceneItem, unsigned layer)
    {
        if (0 == mActiveIterators)
        {
            mSceneItems[layer].push_back(pBaseSceneItem);
        }
        else
        {
            DelayedOperation delayedOp;
            delayedOp.type = ADD;
            delayedOp.ptr = pBaseSceneItem;
            delayedOp.layer = layer;
            mDelayedOperations.push(delayedOp);
        }
    }

    void SceneManager::RemoveObject(SceneObjects::BaseSceneItem *pBaseSceneItem, unsigned layer)
    {
        if (0 == mActiveIterators)
        {
            // TODO: Assert that the scenelayer is valid
            std::vector<SceneObjects::BaseSceneItem*>::iterator it = std::find( mSceneItems[layer].begin(), mSceneItems[layer].end(), pBaseSceneItem );
            if (it != mSceneItems[layer].end())
            {
                mSceneItems[layer].erase(it);
            }
        }
        else
        {
            DelayedOperation delayedOp;
            delayedOp.type = REMOVE;
            delayedOp.ptr = pBaseSceneItem;
            delayedOp.layer = layer;
            mDelayedOperations.push(delayedOp);
        }
    }

    // remove everything from the scene
    void SceneManager::ClearAllObjects()
    {
        // we're about to invalidate any iterators
        assert(0==mActiveIterators);
        for (unsigned i=0; i<MAX_LAYER_COUNT; ++i)
        {
            mSceneItems[i].clear();
        }
    }

    // update all scene objects
    void SceneManager::Update(float secondsSinceLastFrame)
    {
        AddActiveIterator();

        for (unsigned layer=0; layer<MAX_LAYER_COUNT; ++layer)
        {
            std::vector<SceneObjects::BaseSceneItem*>::iterator it;
            for (it = mSceneItems[layer].begin(); it != mSceneItems[layer].end(); ++it)
            {
                (*it)->Update(secondsSinceLastFrame);
            }
        }

        RemoveActiveIterator();
    }


    void SceneManager::Init()
    {
        Framework::BaseSubsystem::Init();
    }

    void SceneManager::Shutdown()
    {
        ClearAllObjects();

        Framework::BaseSubsystem::Shutdown();
    }

    // should only be called from within RemoveActiveIterator
    void SceneManager::PerformDelayedOperations()
    {
        assert(0==mActiveIterators);

        while (!mDelayedOperations.empty())
        {
            DelayedOperation op = mDelayedOperations.front();
            mDelayedOperations.pop();

            switch (op.type)
            {
            case ADD:
                AddObject(op.ptr, op.layer);
                break;
            case REMOVE:
                RemoveObject(op.ptr, op.layer);
                break;
            }
        }
    }

    void SceneManager::AddActiveIterator()
    {
        ++mActiveIterators;
    }
    void SceneManager::RemoveActiveIterator()
    {
        --mActiveIterators;
        assert(0<=mActiveIterators);
        if (0 == mActiveIterators)
        {
            PerformDelayedOperations();
        }
    }
}
