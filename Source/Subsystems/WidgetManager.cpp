/*
    Particle Blow's Manager for UI widgets
*/

#include "WidgetManager.h"
#include <algorithm>


namespace Subsystems
{
    WidgetManager::WidgetManager() :
        SceneObjects::WidgetHolder(),
        mSelectedWidget(NULL),
        mWidgetLayer(SceneManager::DEFAULT_WIDGET_LAYER)
    {
    }

    // handle user input
    void WidgetManager::Update(float secondsSinceLastFrame)
    {
        // scene manager will update the widgets for us
    }

    // clean up!
    void WidgetManager::Shutdown()
    {
        Deselect();
        RemoveAll();
    }

    // add a widget to the manager (adds it to the scene manager for you)
    void WidgetManager::AddWidget(SceneObjects::BaseWidget* pWidget)
    {
        if (!ShouldDelayOperations()) // if not delaying the add
        {
            // we will need it to be in the scene manager if it is going to be rendered
            mpSceneManager->AddObject(pWidget, mWidgetLayer);
        }

        SceneObjects::WidgetHolder::AddWidget(pWidget);
    }

    // remove a widget from the manager (removes it from the scene manager for you)
    void WidgetManager::RemoveWidget(SceneObjects::BaseWidget* pWidget)
    {
        if (!ShouldDelayOperations()) // if not delaying the remove
        {
            mpSceneManager->RemoveObject(pWidget, mWidgetLayer);
        }

        SceneObjects::WidgetHolder::RemoveWidget(pWidget);
    }

    // set a widget as selected
    void WidgetManager::Select(SceneObjects::BaseWidget* pWidget)
    {
        Deselect();

        if (NULL != pWidget)
        {
            mSelectedWidget = pWidget;
            mSelectedWidget->Select();
        }
    }

    // deselect the currently selected widget
    void WidgetManager::Deselect()
    {
        if (NULL != mSelectedWidget)
        {
            mSelectedWidget->Deselect();
            mSelectedWidget = NULL;
        }
    }

    // get the currently selected widget (or null if none selected)
    SceneObjects::BaseWidget* WidgetManager::GetSelected()
    {
        return mSelectedWidget;
    }
}



