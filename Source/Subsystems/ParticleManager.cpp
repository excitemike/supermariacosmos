/*
    Subsystem for handling the particles in ParticleBlow
*/

#include "ParticleManager.h"
#include <stdlib.h>

namespace Subsystems
{
    ParticleManager::ParticleManager()
        : mpSceneManager(0),
        mMaxParticles(100),
        mMaxParticleLifetimeInSeconds(1.f),
        mParticles(0),
        mParticleLayer(SceneManager::DEFAULT_PARTICLE_LAYER),
        mMinX(0.f),
        mMaxX(100.f),
        mMinY(0.f),
        mMaxY(100.f)
    {
    }

    // configure the particle manager
    // TODO: this should try to get the scenemanger itself in Init
    void ParticleManager::Config(
        SceneManager *pSceneManager,
        unsigned maxParticles,
        unsigned particleLayer,
        float maxParticleLifetimeInSeconds,
        float minX,
        float maxX,
        float minY,
        float maxY)
    {
        mpSceneManager = pSceneManager;
        mMaxParticles = maxParticles;
        mParticleLayer = particleLayer;
        mMaxParticleLifetimeInSeconds = maxParticleLifetimeInSeconds;
        mMinX = minX;
        mMaxX = maxX;
        mMinY = minY;
        mMaxY = maxY;
    }

    // initialize the particle manager
    // - create the array of particles and add the to the scene manager
    void ParticleManager::Init()
    {
        // create the particles
        mParticles = new SceneObjects::Particle[mMaxParticles];

        // initialize them
        for (unsigned i=0; i<mMaxParticles; ++i)
        {
            mpSceneManager->AddObject( &(mParticles[i]), mParticleLayer );
            mParticles[i].Hide();
        }
    }

    // clean up to avoid leaking memory
    void ParticleManager::Shutdown()
    {
        if (0 != mParticles)
        {
            delete [] mParticles;
            mParticles = 0;
        }
    }

    // TODO: make the particles fly around here
    void ParticleManager::Update(float secondsSinceLastFrame)
    {
        // look at each particle
        for (unsigned i=0; i<mMaxParticles; ++i)
        {
            // only living ones need to be updated
            if (mParticles[i].IsVisible())
            {
                // age the particles, mark them as dead if they get too old
                AgeParticle(i, secondsSinceLastFrame);

                // kill particles that go out of bounds
                if ( ParticleIsOutOfBounds(i) )
                {
                    mParticles[i].Hide();
                }

                // have the particles do their thing
                // TODO: Update should be part of a sceneitem interface and
                // the scene manager should be the one to call this stuff
                mParticles[i].Update(secondsSinceLastFrame);
            }
        }
    }

    // see if a particle has gone too far in any direction
    bool ParticleManager::ParticleIsOutOfBounds(unsigned particleIndex)
    {
        if ( (mParticles[particleIndex].GetX() < mMinX - mParticles[particleIndex].GetWidth())
           ||(mParticles[particleIndex].GetX() > mMaxX)
           ||(mParticles[particleIndex].GetY() < mMinY - mParticles[particleIndex].GetWidth())
           ||(mParticles[particleIndex].GetY() > mMaxY) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // age the particles, mark them as dead if they get too old
    void ParticleManager::AgeParticle(unsigned particleIndex, float secondsSinceLastFrame)
    {
        mParticles[particleIndex].IncreaseAge(secondsSinceLastFrame);

        if (mParticles[particleIndex].GetAge() > mMaxParticleLifetimeInSeconds)
        {
            mParticles[particleIndex].Hide();
        }
    }

    // Start a particle going
    void ParticleManager::SpawnParticle(float startingX, float startingY, float velocityX, float velocityY)
    {
        // instead of looping through and searching for a good particle to use, grabbing one at random is
        // fast and looks about the same.
        SceneObjects::Particle *pParticle = &(mParticles[rand() % mMaxParticles]);

        // init the particle
        pParticle->SetAge(0.f);
        pParticle->SetX(startingX);
        pParticle->SetY(startingY);
        pParticle->SetVelocity(velocityX, velocityY);
        pParticle->Show();
    }


    // return a pointer to the first dead particle in the list, or null if
    // there are no dead particles
    SceneObjects::Particle* ParticleManager::GetFirstDeadParticle()
    {
        // start looking through the list
        for (unsigned i=0; i<mMaxParticles; ++i)
        {
            // if it's dead, then return that one and stop looking
            if (!mParticles[i].IsVisible())
            {
                return &(mParticles[i]);
            }
        }

        // we failed to find any dead particles, return null
        return 0;
    }

    // return a pointer to the oldest (living) particle in the list
    SceneObjects::Particle* ParticleManager::GetOldestParticle()
    {
        float highestAgeSoFar = -1.f;
        SceneObjects::Particle *pOldestParticleSoFar = 0;

        // start looking through the list
        for (unsigned i=0; i<mMaxParticles; ++i)
        {
            // we only count living particles
            if (mParticles[i].IsVisible())
            {
                if (highestAgeSoFar < mParticles[i].GetAge())
                {
                    highestAgeSoFar = mParticles[i].GetAge();
                    pOldestParticleSoFar = &(mParticles[i]);
                }
            }
        }

        if (0 == pOldestParticleSoFar)
        {
            // crash so I find out about it immediately
            // HOW DID THEY ALL GET AN AGE < -1????
            SceneObjects::Particle* a = *(SceneObjects::Particle**)0x12345678;
            return a;
        }

        // we finished going through the list so return whatever particle was the oldest
        return pOldestParticleSoFar;
    }
}
