/*
    Subsystem to handle organizing the scenegraph for ParticleBlow
*/

#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "Framework/BaseSubsystem.h"
#include "Subsystems/RenderManager.h"
#include "SceneObjects/BaseSceneItem.h"
#include "SceneObjects/Sprite.h"
#include <vector>
#include <queue>

namespace Subsystems
{
    class SceneManager : public Framework::BaseSubsystem
    {
    public:
        static const unsigned DEFAULT_BACKGROUND_LAYER = 0;
        static const unsigned DEFAULT_WIDGET_LAYER = 1;
        static const unsigned DEFAULT_PARTICLE_LAYER = 2;
        static const unsigned MAX_LAYER_COUNT = 8;
    public:
        SceneManager();
        virtual ~SceneManager();

        virtual void Update(float secondsSinceLastFrame);

        void RenderScene(RenderManager*);

        // set an image to stretch across the whole screen for the background
        void SetBackgroundImage(const char * imageFile);

        void AddObject(SceneObjects::BaseSceneItem *pBaseSceneItem, unsigned layer);
        void RemoveObject(SceneObjects::BaseSceneItem *pBaseSceneItem, unsigned layer);

        void Init();
        void Shutdown();

        // remove everything from the scene
        void ClearAllObjects();
    private:
        void RenderSceneLayer(RenderManager *pRenderer, unsigned layer);

        SceneObjects::Sprite *mpBackground;
        std::vector<SceneObjects::BaseSceneItem *> mSceneItems[MAX_LAYER_COUNT];

        //
        // crap to deal with iterator invalidation
        // (we COULD give each layer it's own count here)
            int mActiveIterators;

            enum DelayedOperationType
            {
                ADD,
                REMOVE
            };

            struct DelayedOperation
            {
                DelayedOperationType type;
                SceneObjects::BaseSceneItem *ptr;
                unsigned layer;
            };

            std::queue<DelayedOperation> mDelayedOperations;
            void AddActiveIterator();
            void RemoveActiveIterator();
            void PerformDelayedOperations();
    };
}

#endif // SCENEMANAGER_H
