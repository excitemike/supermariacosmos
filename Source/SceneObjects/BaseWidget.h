/*
    Base class for UI widgets
*/

#ifndef BASEWIDGET_H
#define BASEWIDGET_H

#include "BaseSceneItem.h"
#include "WidgetHolder.h"

namespace SceneObjects
{
    class BaseWidget : public BaseSceneItem, public WidgetHolder
    {
    public:
        BaseWidget();

        // return true if this widget handled the event, false if it should be
        // passed on to something else to handle it
        virtual bool OnMouseDown(float x, float y){return WidgetHolder::OnMouseDown(x, y);}

        // called every frame regardles of whether it actually moved
        virtual void OnMouseMove(float x, float y){WidgetHolder::OnMouseMove(x, y);}

        // react to mouse release
        virtual void OnMouseUp(float x, float y){WidgetHolder::OnMouseUp(x, y);}

        virtual void Render(Subsystems::RenderManager* pRenderer){WidgetHolder::Render(pRenderer);}
        virtual void Update(float secondsSinceLastFrame){WidgetHolder::Update(secondsSinceLastFrame);}

        virtual bool IsSelected() {return mIsSelected;}

        virtual void Select() {mIsSelected = true;}
        virtual void Deselect() {mIsSelected = false;}
    private:
        bool mIsSelected;
    };
}

#endif // BASEWIDGET_H
