#ifndef SCENEOBJECTS_WIDGETHOLDER_H
#define SCENEOBJECTS_WIDGETHOLDER_H

#include "Subsystems/RenderManager.h"
#include "Transform.h"
#include <vector>
#include <queue>


namespace SceneObjects
{
    class BaseWidget;

    class WidgetHolder
    {
    public:
        WidgetHolder() : mUpdateChildWidgets(true), mActiveIterators(0) {}
        virtual ~WidgetHolder(){}

        virtual void Update(float secondsSinceLastFrame);

        // expects some kind owner class to call it, so that it can propagate to all the widgets
        virtual void OnMouseMove(float mousex, float mousey);
        virtual void OnMouseUp(float mousex, float mousey);

        // returns true if a widget "caught" the event and doesn't want it leaking down any further
        virtual bool OnMouseDown(float mousex, float mousey);

        virtual unsigned GetNumWidgets() {return (unsigned)mWidgets.size();}
        virtual SceneObjects::BaseWidget* GetWidget(unsigned index) {return mWidgets[index];}

        // control whether widgets this thing is holding should have Update()
        // called on them.  In case you have them appearing more than once in
        // the scene.
        virtual void SetUpdateChildWidgets(bool bDoUpdate){mUpdateChildWidgets = bDoUpdate;}

        // draw it's widgets
        virtual void Render(Subsystems::RenderManager* pRenderer);

        virtual void AddWidget(SceneObjects::BaseWidget*);

        virtual void RemoveWidget(SceneObjects::BaseWidget*);

        virtual void RemoveAll();

        virtual void SetScale(float scale)       {mTransform.Scale = scale;}
        virtual void SetRotation(float rotation) {mTransform.Rotation = rotation;}
        virtual void SetTranslation(float translation_x, float translation_y)
        {
            mTransform.Translation_x = translation_x;
            mTransform.Translation_y = translation_y;
        }

        virtual const Transform& GetTransform(){return mTransform;}

    protected:
        virtual bool ShouldDelayOperations(){return 0!=mActiveIterators;}
    private:

        std::vector<SceneObjects::BaseWidget*> mWidgets;

        Transform mTransform;

        bool mUpdateChildWidgets;

        //
        // crap to deal with iterator invalidation
        //
            int mActiveIterators;

            enum DelayedOperationType
            {
                ADD,
                REMOVE
            };

            struct DelayedOperation
            {
                DelayedOperationType type;
                SceneObjects::BaseWidget* ptr;
            };

            std::queue<DelayedOperation> mDelayedOperations;
            virtual void AddActiveIterator();
            virtual void RemoveActiveIterator();
            virtual void PerformDelayedOperations();
    };
}

#endif // SCENEOBJECTS_WIDGETHOLDER_H


