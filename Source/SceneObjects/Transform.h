#ifndef SCENEOBJECTS_TRANSFORM_H
#define SCENEOBJECTS_TRANSFORM_H

#include "Framework/math.h"

namespace SceneObjects
{
    class Transform
    {
    public:
        Transform() :
          Scale(1.f),
          Rotation(0.f),
          Translation_x(0.f),
          Translation_y(0.f)
        {
        }

        void Apply(float in_x, float in_y, float &out_x, float &out_y) const
        {
            // scale
            out_x = Scale * in_x;
            out_y = Scale * in_y;

            // rotate
            RotatePoint(sinf(Rotation), cosf(Rotation), out_x, out_y, out_x, out_y);

            // translate
            out_x += Translation_x;
            out_y += Translation_y;
        }

        void ApplyInverse(float in_x, float in_y, float &out_x, float &out_y) const
        {
            // un-translate
            out_x = in_x - Translation_x;
            out_y = in_y - Translation_y;

            // un-rotate
            RotatePoint(sinf(-Rotation), cosf(-Rotation), out_x, out_y, out_x, out_y);

            // un-scale
            float dividablescale = (fabsf(Scale) > LENGTH_EPSILON) ? Scale : LENGTH_EPSILON;
            out_x /= dividablescale;
            out_y /= dividablescale;
        }

        float Scale;
        float Rotation;
        float Translation_x;
        float Translation_y;
    };
}

#endif // SCENEOBJECTS_TRANSFORM_H
