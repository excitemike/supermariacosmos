/*
    Base class for UI widgets
*/

#include "BaseWidget.h"

namespace SceneObjects
{
    BaseWidget::BaseWidget()
        : BaseSceneItem(), WidgetHolder(), mIsSelected(false)
    {
    }
}
