/*
    Base type for items that go in the scene graph
*/
#ifndef BASESCENEITEM_H
#define BASESCENEITEM_H

#include "Subsystems/RenderManager.h"

namespace SceneObjects
{
    class BaseSceneItem
    {
    public:
        BaseSceneItem();
        virtual ~BaseSceneItem();

        virtual void SetPosition(float x, float y) {mXPos = x;mYPos = y;}
        virtual void SetX(float x) {mXPos = x;}
        virtual void SetY(float y) {mYPos = y;}
        virtual float GetX() {return mXPos;}
        virtual float GetY() {return mYPos;}
        virtual bool IsVisible() {return mIsVisible;}
        virtual void Show(){mIsVisible = true;}
        virtual void Hide(){mIsVisible = false;}

        virtual void Render(Subsystems::RenderManager*){}
        virtual void Update(float secondsSinceLastFrame){}
    protected:
        float mXPos;
        float mYPos;
        bool mIsVisible;
    };
}

#endif // BASESCENEITEM_H
