/*
    Class to represent a simple 2d sprite for ParticleBlow
*/
#ifndef SPRITE_H
#define SPRITE_H

#include "BaseSceneItem.h"
#include "Utilities/TextureManager.h"

namespace SceneObjects
{
    class Sprite : public BaseSceneItem
    {
    public:
        Sprite(const char* image, float width, float height);

        void Render(Subsystems::RenderManager*);

        float GetWidth() {return mWidth;}
        float GetHeight() {return mHeight;}
        float GetRotation() {return mRotation;}

        // rotate the sprite around center
        void SetRotation(float angle) {mRotation = angle;}

        void SetSize(float width, float height){mWidth = width; mHeight = height;}
    protected:
        float mWidth;
        float mHeight;
        float mOffset_x;
        float mOffset_y;
        float mRotation;
        const Utilities::Texture *mTexture;
    };
}

#endif
