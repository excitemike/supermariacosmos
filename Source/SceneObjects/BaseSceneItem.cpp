/*
    Base type for items that go in the scene graph
*/
#include "BaseSceneItem.h"

namespace SceneObjects
{
    BaseSceneItem::BaseSceneItem()
        : mXPos(0.f), mYPos(0.f), mIsVisible(true)
    {
    }

    BaseSceneItem::~BaseSceneItem()
    {
        //TODO: Utilities::TextureManager::FreeTexture(mTextureId);
    }
}
