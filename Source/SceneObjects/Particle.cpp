/*
class for each of the jillions of particles to use in Particle Blow
*/
#include "Particle.h"

namespace SceneObjects
{
    Particle::Particle()
        : Sprite("Content/Textures/Particle.png", 16.f, 16.f),
        mAge(0)
    {
    }

    // move to where that velocity would take it in the specified time
    void Particle::Update(float secondsSinceLastFrame)
    {
        SetX(GetX() + (mVelocityX*secondsSinceLastFrame));
        SetY(GetY() + (mVelocityY*secondsSinceLastFrame));
    }
}
