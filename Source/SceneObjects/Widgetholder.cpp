#include "SceneObjects/WidgetHolder.h"
#include "SceneObjects/BaseWidget.h"
#include <assert.h>
#include <algorithm>

// call a function on all widgets, going through them in reverse order
#define CALL_ON_ALL( function_and_args ) \
    std::vector<SceneObjects::BaseWidget*>::iterator it = mWidgets.begin(); \
    AddActiveIterator(); \
    \
    for (; it != mWidgets.end(); ++it) \
    { \
        (*it)-> function_and_args ; \
    } \
    \
    RemoveActiveIterator();

// call a function on all widgets, going through them in reverse order
#define CALL_ON_ALL_REVERSE( function_and_args ) \
    std::vector<SceneObjects::BaseWidget*>::reverse_iterator it = mWidgets.rbegin(); \
    AddActiveIterator(); \
    \
    for (; it != mWidgets.rend(); ++it) \
    { \
        (*it)-> function_and_args ; \
    } \
    \
    RemoveActiveIterator();

namespace SceneObjects
{
    void WidgetHolder::Update(float secondsSinceLastFrame)
    {
        if (mUpdateChildWidgets)
        {
            CALL_ON_ALL_REVERSE( Update(secondsSinceLastFrame) );
        }
    }

    void WidgetHolder::Render(Subsystems::RenderManager* pRenderer)
    {
        pRenderer->PushTransformation(
            mTransform.Scale,
            mTransform.Rotation,
            mTransform.Translation_x,
            mTransform.Translation_y );

        CALL_ON_ALL( Render(pRenderer) );

        pRenderer->PopTransformation();
    }

    // have all the widgets handle the event
    void WidgetHolder::OnMouseMove(float x, float y)
    {
        mTransform.ApplyInverse(x, y, x, y);
        CALL_ON_ALL_REVERSE( OnMouseMove(x, y) );
    }

    // have all the widgets handle the event
    void WidgetHolder::OnMouseUp(float x, float y)
    {
        mTransform.ApplyInverse(x, y, x, y);
        CALL_ON_ALL_REVERSE( OnMouseUp(x, y) );
    }

    bool WidgetHolder::OnMouseDown(float x, float y)
    {
        mTransform.ApplyInverse(x, y, x, y);

        bool inputHandled = false;

        AddActiveIterator();

        // from highest to lowest in draw order, let the widgets see input and
        // stop once one handles it
        std::vector<SceneObjects::BaseWidget*>::reverse_iterator it = mWidgets.rbegin();

        for (; it != mWidgets.rend(); ++it)
        {
            if ( (*it)->OnMouseDown(x, y) )
            {
                inputHandled = true;
                break;
            }
        }

        RemoveActiveIterator();
        return inputHandled;
    }

    // clean up!
    void WidgetHolder::RemoveAll()
    {
        // we're about to invalidate any iterators
        assert(0==mActiveIterators);
        mWidgets.clear();
    }

    // add a widget to the manager (adds it to the scene manager for you)
    void WidgetHolder::AddWidget(SceneObjects::BaseWidget* pWidget)
    {
        if (!ShouldDelayOperations())
        {
            mWidgets.push_back(pWidget);
        }
        else
        {
            DelayedOperation delayedOp;
            delayedOp.type = ADD;
            delayedOp.ptr = pWidget;
            mDelayedOperations.push(delayedOp);
        }
    }

    // remove a widget from the manager (removes it from the scene manager for you)
    void WidgetHolder::RemoveWidget(SceneObjects::BaseWidget* pWidget)
    {
        if (!ShouldDelayOperations())
        {
            std::vector<SceneObjects::BaseWidget*>::iterator it = std::find( mWidgets.begin(), mWidgets.end(), pWidget );
            if ( it != mWidgets.end() )
            {
                mWidgets.erase(it);
            }
        }
        else
        {
            DelayedOperation delayedOp;
            delayedOp.type = REMOVE;
            delayedOp.ptr = pWidget;
            mDelayedOperations.push(delayedOp);
        }
    }


    // should only be called from within RemoveActiveIterator
    void WidgetHolder::PerformDelayedOperations()
    {
        assert(0==mActiveIterators);

        while (!mDelayedOperations.empty())
        {
            DelayedOperation op = mDelayedOperations.front();
            mDelayedOperations.pop();

            switch (op.type)
            {
            case ADD:
                AddWidget(op.ptr);
                break;
            case REMOVE:
                RemoveWidget(op.ptr);
                break;
            }
        }
    }

    void WidgetHolder::AddActiveIterator()
    {
        ++mActiveIterators;
    }

    void WidgetHolder::RemoveActiveIterator()
    {
        --mActiveIterators;
        assert(0<=mActiveIterators);
        if (0 == mActiveIterators)
        {
            PerformDelayedOperations();
        }
    }
}
