/*
    Class to represent a simple 2d sprite for ParticleBlow
*/
#include "Sprite.h"

namespace SceneObjects
{
    Sprite::Sprite(const char* image, float width, float height)
        : BaseSceneItem(), mWidth(width), mHeight(height),
        mOffset_x(0.f), mOffset_y(0.f), mRotation(0.f), mTexture(NULL)
    {
        mTexture = Utilities::TextureManager::GetTexture(image);
    }

    // actually draw the sprite onscreen
    void Sprite::Render(Subsystems::RenderManager* renderer)
    {
        if (mIsVisible)
        {
            renderer->RenderSprite(mTexture, mXPos, mYPos, mWidth, mHeight, false, false, mRotation, mWidth/2.f, mHeight/2.f);
        }
    }
}

