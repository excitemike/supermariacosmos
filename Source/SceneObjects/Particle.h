/*
    class for each of the jillions of particles to use in Particle Blow
*/
#ifndef PARTICLE_H
#define PARTICLE_H

#include "Sprite.h"

namespace SceneObjects
{
    class Particle : public Sprite
    {
    public:
        Particle();

        void SetAge(float ageInSeconds) {mAge = ageInSeconds;}
        float GetAge() {return mAge;}
        void IncreaseAge(float seconds) {mAge += seconds;}

        void SetVelocity(float velocityX, float velocityY) {mVelocityX = velocityX; mVelocityY = velocityY;}
        float GetVelocityX() const {return mVelocityX;}
        float GetVelocityY() const {return mVelocityY;}
        void AddToVelocity(float velocityX, float velocityY) {mVelocityX += velocityX; mVelocityY += velocityY;}

        void Update(float secondsSinceLastFrame);
    private:
        float mAge;
        float mVelocityX;
        float mVelocityY;
    };
}

#endif // PARTICLE_H
